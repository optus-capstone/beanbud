//Connects to accounts database and manages create/delete/edit of user accounts

import mysql from "mysql"
import express, { response } from 'express';
import cors from 'cors'
import bcrypt from 'bcrypt'

//Returns a created mysql driver object connecting to the BeanBud database 
const driver = () => {
    let con = mysql.createConnection(
        {
            //Change to ENV variable later on. This is for test purposes only
            host    : "beanbud-database.cmnuelfqlcfy.us-west-1.rds.amazonaws.com",
            user    : "admin",
            password: "D8zGIn9x2B3yGEYPaIgq",
            port    : 3306,
            database: "BeanBud"
        }
    );
    return con
}

let connector = driver()

const app = express()
const port = 3001


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.get('/', (req, res) => {
    res.send('Beanbud Server')
})


app.listen(port, () => {
    console.log(`Server app listening on port ${port}`)
})

app.get('/users/all', async (req,res) => {
    const userData = await showUsers().then((data) => {
        return data
    })
    let users = []
    for(let i in userData) {
        users.push(i.first_name)
    }
    res.send(userData)
})

app.get('/customer/all', async (req,res) => {
    res.send(showCustomers())
})

app.get('/tables', async (req,res) => {
    res.send(getTables())
})

app.get('/account/all', async (req,res) => {
    res.send(getAccounts())
})


app.get('/items/all', async (req,res) => {
    res.send(showItems())
})

app.get('/account/verify/:username/:password', async (req, res, next) => {
    const username = req.params.username
    const password = req.params.password
    const hashPassword = await getPassword(username).then((data) => {
        return data[0].password
    })
    const match = await bcrypt.compare(password,hashPassword)
    if(match) {
        res.send(true)
    }
    else { 
        res.send(false)
    }
    next()
})

app.get('/create/account/:username/:password', async (req,res, next) => {
    const username = req.params.username
    const salt = await bcrypt.genSalt(10)
    const password = await bcrypt.hash(req.params.password, salt);
    const checkUsername = await getAccount(username).then((data) => {
        return data
    })
    if(checkUsername.length != 0) {
        res.send(false)
    }
    else {
        const confirm = await createAccount(username,password).then((data) => {
            return data
        }) 
        res.send(true)
    }
    next()
})

app.get('/account/check/:username', async (req,res,next) => {
    const checkUsername = await getAccount(req.params.username).then((data)=> {
        return data
    })
    if(checkUsername.length == 0) {
        res.send(false)
    }
    else {
        res.send(checkUsername[0].username)
    }

    next()
})

app.get('/get/items/', async (req,res, next ) => {
    const status = req.params.status
    const result = await getItems().then((data) => {
        return data
    })
    res.send(result)
    next()
})

app.get('/get/orders', async (req,res) => {
    const data = await getOrders().then((data) => {
        return data
    })
    res.send(data)
})

app.get('/orders/all', async (req,res) => {
    const data = await showOrders().then((data) => {
        return data
    })
    res.send(data)
})

app.get('/order/:id/:status', async (req, res, next) => {
    console.log('id: ', req.params.id)
    const response = await setOrderReady(req.params.id, req.params.status)
    res.send(response)
    next()

})




async function showUsers() {
    const statement = "SELECT customer_id, first_name FROM Customer";
    return new Promise((resolve, reject) => {
        connector.query(statement, (err, result) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(result);
            }
        })
    })

}




function getDatabaseDriver() {
    return connector
}


function getTables() {
    connector.query("SHOW TABLES", function(err, result, fields) {
        if (err) throw err;
        console.log(result)
    })
}



function getCustomerById(customerid) {
    const statement = `SELECT `
}


function showCustomers() {
    connector.query('SELECT customer_id, first_name FROM Customer', function(err, result, fields) {
        if (err) throw err;
        console.log(result)
    })
}




function showItems() {
    connector.query('SELECT * FROM Item', function(err, result, fields) {
        if (err) throw err;
        console.log(result)
    })
}


async function getItems() {
    const statement = `SELECT item_id, category ,item_description as item_attributes, item_price FROM Item`
    return new Promise((resolve, reject) => {
        connector.query(statement, (err, result) => {
            if(err) {
                reject(err);
            }
            else {
                  resolve(result);
            }
        })
    })
}



function addItem(item) {
    const data = [item.name, item.id, item.price, item.desc, item.image]
    const statement = `INSERT INTO Item(item_name, item_id, item_price, item_desc, item_image) VALUES ?`
    connector.query(statement, [data], function (err, result) {
        if(err) throw err;
        console.log(`Added ${item.name}: ${item.id} to the database`)
    })
    return true
}


function removeItem(item) {
    const itemExist = checkItem(item.id)
    if (itemExist) {
        const statemenet = `DELETE FROM Item WHERE item_id = ${item.id}`
        new Promise((reject, resolve) => {
            connector.query(statement, (err, result) => {
                if(err) {
                    console.log(result)
                    reject(result)
                }
                else {
                    console.log(result)
                    resolve(result)
                }
            })
        })
    } 
}




async function getItemByCategory(category) {
    const statement = `SELECT * FROM Item WHERE category = ${category}`
    const result = connector.query(statement, (err,res) => {
        if (err) throw err;
        if (result.length != 0) {
            return true
        }
        return false
    })
}


function showDatabase() {
    connector.query("SELECT * FROM Item", function(err,result,fields) {
        if (err) throw err;
        console.log(result)
    }) 
}


function showRatings() {
    const statement = 'SELECT * FROM Ratings'
    connector.query(statement, function(err, result, fields) {
        if (err) throw err;
        console.log(result)
    })
}


function showExtras() {
    const statement = 'SELECT * FROM Extras'
    connector.query(statement, function(err, result, fields) {
        if (err) throw err;
        console.log(result)
    })
}





//accounts 


function getAccounts() {
    const statement =  'SELECT * FROM Account'
    return new Promise((resolve, reject) => {
      connector.query(statement, (err, result) => {
          if(err) {
              reject(err);
          }
          else {
                console.log(result)
                resolve(result);
          }
      })
  })
}


async function getAccount(username) {
    const statement = `SELECT username FROM Account WHERE username = '${username}'`
    return new Promise((resolve, reject) => {
        connector.query(statement, (err, result) => {
            if(err) {
                reject(err);
            }
            else {
                  resolve(result);
            }
        })
    })
}


async function getPassword(username) {
    const statement = `SELECT password FROM Account WHERE username = '${username}'`
    return new Promise((resolve, reject) => {
        connector.query(statement, (err, result) => {
            if(err) {
                reject(err);
            }
            else {
                  resolve(result);
            }
        })
    })
}

async function createAccount(username, password) {
    const statement = `INSERT INTO Account (username, password) VALUES('${username}', '${password}')`
    return new Promise((resolve, reject) => {
        connector.query(statement, (err, result) => {
            if(err) {
                reject(err);
            }
            else {
                  console.log(result)
                  resolve(true)
            }
        })
    })
}

// Orders

async function getOrders() {
    const statement = `SELECT * FROM Orders WHERE status = 'Pending'`
    return new Promise((resolve, reject) => {
        connector.query(statement, (err, result) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(result);
            }
        })
    })
}

async function showOrders() {
    const statement = `SELECT * FROM Orders`
    return new Promise((resolve, reject) => {
        connector.query(statement, (err, result) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(result);
            }
        })
    })
}


async function setOrderReady(orderid,status) {
      const statement =  `UPDATE Orders SET status="${status}" WHERE order_id=${orderid}`
      return new Promise((resolve, reject) => {
        connector.query(statement, (err, result) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(result);
            }
        })
    })
}


