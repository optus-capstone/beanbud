##Server for handling user accounts
- Requires NodeJS to be installed
- Uses ES6 Syntax

###dependencies: 
1. ejs
2. aws-sdk
3. express
4. mocha
5. body-parser
6. mysql



###How to run:
- `npm run`
  - Check if node_modules are update using `npm update`
- Test
  - `npm test` 

**Tables in BeanBud Database:**
![Imgur](https://imgur.com/WfYU8aG.png)

**Assumption of Account database Information:**
- Username
- Email
- Password salt
- Password (Store as hash in db)
- Name
- Phone Number (optional)
- UUID

**Up to date:**
- Server Running
- Connects to AWS database



**Function:**
- AccountServer.js handles requests from client
- AccountManager.js communicates with the Accounts database for user creation/deletion/alteration


**To do:**
- Add function to create users
- Add function to delete user
- Update user details



**Added Test Cases:**
- Database Connection
- Create/Delete/Edit User Accounts

**Change later:**
- Remove hardcoded database login details and set env variables for database connection




Links:
- https://www.donedone.com/blog/building-the-optimal-user-database-model-for-your-application
- https://mochajs.org/
- https://expressjs.com/en/starter/hello-world.html
- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-nodejs.rds.html