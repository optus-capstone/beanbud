import { CircularProgress, Typography } from "@mui/material"




const LoadingScreen = () => {
    return (<div className="Home-page">
        <CircularProgress/>
        <Typography>Loading...</Typography>
    </div>) 
}

export default LoadingScreen