import React from 'react';
import SignUpPagePanel from '../components/SignUpPagePanel';


export class SignUpPage extends React.Component  {
    render() {
        return (
            <div className='App-header'>
                <SignUpPagePanel></SignUpPagePanel>
            </div>
        )
    }
}

export default SignUpPage;