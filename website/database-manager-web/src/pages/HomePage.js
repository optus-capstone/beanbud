import Calendar from "../components/HomePage/Calendar";
import InfoBar from "../components/HomePage/InfoBar";
import { Sidebar } from "../components/HomePage/Sidebar";
import Stack from '@mui/material/Stack'
import HomePageLogo from "../components/HomePage/HomePageLogo";
import OrderedMenu from "../components/HomePage/OrderedMenu";
import StatBar from "../components/HomePage/StatBar";
import DashboardPage from "./DashboardPage";
import { Typography } from "@mui/material";
import React from "react";
import axios from 'axios'


class HomePage extends React.Component {
  constructor(props) {
    super(props)
    this.state = { data: []}
  }

  componentDidMount() {
    const addr = 'http://localhost:3001/users/all'
    axios.get(addr).then(res => res.data).then(dataJson => {
      let dataArr = []
      for(let i in dataJson) {
        dataArr.push(dataJson[i])
      }
      this.setState({data: dataArr})
    })
  }
  
  render() {
    try {
      console.log(this.state.data)
    }
    catch(err) {
    }
    return (  
      <DashboardPage/>
    );
  }
}


export default HomePage;
