import React, { useState, useEffect } from 'react';
import { Box, Stack, Typography, Grid, Button, Modal } from '@mui/material/'
import Sidebar from '../components/HomePage/Sidebar';
import Calendar from '../components/HomePage/Calendar'
import { createTheme } from '@mui/system';
import axios from 'axios'
import LoadingScreen from './LoadingScreen';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import FastfoodIcon from '@mui/icons-material/Fastfood';

let ORDERS = []
for (let i = 0; i < 7; i++)
{
    ORDERS[i] = {
        item: 'Chicken Burger',
        custom: ['No lettuce', 'Extra cheese'],
        quantity: 1,
        ppu: 8.5,
        time: "17:24"
    }
}

const style = {
    borderRadius:'16px',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
  };

const OrdersPage = () => {
    const [orders ,setOrder] = React.useState()
    const [items, setItems] = React.useState()
    const [itemsOnOrder, setItemsOnOrder] = React.useState()
    const [customers, setCustomers] = React.useState()
    const [fetchingOrders, setFetchOrdersDone] = React.useState(false)
    const [fetchingItems, setFetchItemsDone] = React.useState(false)
    const [orderMap, setOrderMap] = React.useState() 
    const [currentOrder, setCurrentOrder] = React.useState()


    useEffect(() => {
        loadData()
    },[])
    const getItemById = (item_id) => {
        for(let i in items) {
            if(items[i].item_id == item_id) {
                return items[i]
            } 
        }
    }
    async function loadData() {
        const addr = 'http://localhost:3001/get/orders'
        const itemAdrr  = 'http://localhost:3001/get/items'
        await axios.get(addr).then(res => res.data).then(dataJson => {
        let dataArr = []
        for(let i in dataJson) {
            dataArr.push(dataJson[i])
        }
        setOrder(dataArr)
        })
        await axios.get(itemAdrr).then(res => res.data).then(dataJson => {
            let dataArr = []
            for(let i in dataJson) {
                dataArr.push(dataJson[i])
            }
            setItems(dataArr)
        
        })
        setFetchOrdersDone(true)
        setFetchItemsDone(true)
        setCurrentOrder(orders[0])
    }


    while((fetchingItems || fetchingOrders) == false) {
        return (<LoadingScreen/>)
    }



    const OrdersList = () => {


        return (
            <Stack direction="column" spacing={5}>
                {orders.map((order, key) => (
                    <Button className="Order-box" sx={{
                        color:"#FFFFFF",
                        backgroundColor:'#003049',
                        "&:hover":{
                            color: "#003049",
                            backgroundColor: '#EEEEEE'
                        }
                    }}   >
                        <Grid>
                            <Typography textAlign="center" textTransform="none" fontWeight={600}>{'Order: ' + order.order_id}</Typography>
                            <Typography  textAlign="center" textTransform="none" fontWeight={600}>{order.order_type}</Typography>
                        </Grid>
                    </Button>
                ))}
                
            </Stack>
        )
    }


    const OrderedItems = () =>  {
        let ORDERS = []

        let data = []
        for(let i in orders) { 
            let order = orders[i]
            console.log(order)
            let items = order.item_id.trim().slice(1,order.item_id.length-1).split(',')
            let quantity = order.item_quantity.trim().slice(1,order.item_quantity.length-1).split(',')
            let itemListOnOrder = []
            for(let j in items) {
                const itemName = getItemById(items[j]) 
                itemListOnOrder.push( {item: itemName, quantity: quantity[j]})
            }
            let orderJson = {
                customer_id: order.customer_id,
                order_id: order.order_id,
                items_list: itemListOnOrder

            }
            data.push(orderJson)
        }
        ORDERS = data
        console.log(ORDERS[0])
        return (
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                <TableRow>
                    <TableCell>Item</TableCell>
                    <TableCell>Quantity</TableCell>
                    <TableCell>PPU</TableCell>
                    <TableCell>ID</TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {ORDERS[0].items_list.map((row, index) => (
                    <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                        <TableCell>
                            <Grid container>
                                <Grid item lg={2}>
                                    <FastfoodIcon/>
                                </Grid>
                                <Grid item lg={10}>
                                    <Typography fontWeight={600}>{row.item.category}</Typography>
                                    <Typography>{row.item.item_attributes}</Typography>
                                </Grid>
                            </Grid>
                        </TableCell>
                        <TableCell>{row.quantity}</TableCell>
                        <TableCell>{'$'+row.item.item_price}</TableCell>
                        <TableCell>{row.item.item_id}</TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
            </TableContainer>
        )
    }

    return (
        <Box color={"text.primary"}>
            <Stack direction="row" spacing={5} justifyContent="center" className="Dashboard-page">
                <Sidebar></Sidebar>
                <Box flex={0.75}>
                    <Stack direction="column" spacing={1}>
                        <Calendar></Calendar>
                        <Stack direction="column" spacing={3}>
                            <CurrentOrderPanel {...currentOrder}></CurrentOrderPanel>
                            <Typography align="left" fontWeight={600} fontSize={18}>Items Ordered</Typography>
                            <OrderedItems></OrderedItems>
                        </Stack>
                    </Stack>
                </Box>
                <Stack direction='column' flex={0.2} spacing={2}>
                    <Typography variant="h6" component="h2" fontWeight={600} fontSize={22} mt={2}>
                    Orders
                    </Typography>
                    <OrdersList></OrdersList>
                </Stack>
            </Stack>
        </Box>
    )
}



class OrderedItems extends React.Component {
    constructor(props) {
        super(props)
        
    }
    render() {
        return (
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                <TableRow>
                    <TableCell>Item</TableCell>
                    <TableCell>Quantity</TableCell>
                    <TableCell>PPU</TableCell>
                    <TableCell>ID</TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {ORDERS.map((row) => (
                    <TableRow key={row.category} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                        <TableCell>
                            <Grid container>
                                <Grid item lg={2}>
                                    <FastfoodIcon/>
                                </Grid>
                                <Grid item lg={10}>
                                    <Typography fontWeight={600}>{row.item}</Typography>
                                    <Typography>{row.custom}</Typography>
                                </Grid>
                            </Grid>
                        </TableCell>
                        <TableCell>{row.quantity}</TableCell>
                        <TableCell>{row.ppu}</TableCell>
                        <TableCell>{row.time}</TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
            </TableContainer>
        )
    }
}

/*class OrdersPage extends React.Component {
    render () {
        return (
            <Box color={"text.primary"}>
                <Stack direction="row" spacing={5} justifyContent="center" className="Dashboard-page">
                    <Sidebar></Sidebar>
                    <Box flex={0.75}>
                        <Stack direction="column" spacing={1}>
                            <Calendar></Calendar>
                            <Stack direction="column" spacing={3}>
                                <CurrentOrderPanel></CurrentOrderPanel>
                                <Typography align="left" fontWeight={600} fontSize={18}>Items Ordered</Typography>
                                <OrderedItems></OrderedItems>
                            </Stack>
                        </Stack>
                    </Box>
                    <Stack direction='column' flex={0.2} spacing={2}>
                        <Typography variant="h6" component="h2" fontWeight={600} fontSize={22} mt={2}>
                        Orders
                        </Typography>
                        <OrdersList></OrdersList>
                    </Stack>
                </Stack>
            </Box>
        )
    }
}*/

async function onClickComplete(event,orderId) {
    const address = `http://localhost:3001/${orderId}/Pending`
    const response = await axios.get(address).then(res => res.data).then(dataJson => {
        return dataJson
    })
    if(response) {
        console.log('Successfully completed order')
    }
    else {
        console.log('Failed to complete order: Server response = ' + response)
    }
}

const ManageOrderModal = ({currentOrder}) => {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => {
      setOpen(true);
    };
    const handleClose = () => {
      setOpen(false);
    };

    return (
      <div>
        <Button  variant="contained" sx={{backgroundColor:'#979797', "&:hover":{backgroundColor: '#EE817E'}}} onClick={handleOpen}><Typography textTransform="none" fontSize={20} fontWeight={600}>Manage Order</Typography></Button >
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="parent-modal-title"
          aria-describedby="parent-modal-description"
        >
          <Box sx={{ ...style, width: 400 }}>

            <p id="parent-modal-description">
              Complete Order? for id: {currentOrder}
            </p>
            <Stack direction="row" spacing={2}>
                <Button onClick={ (event) => onClickComplete(event,currentOrder)} sx={{
                            color:"#FFFFFF",
                            backgroundColor:'#003049',
                            "&:hover":{
                                color: "#003049",
                                backgroundColor: '#EEEEEE'
                            }
                        }}><Typography>Complete</Typography></Button>
                        
                <Button onClick={handleClose} sx={{
                            color:"#FFFFFF",
                            backgroundColor:'#003049',
                            "&:hover":{
                                color: "#003049",
                                backgroundColor: '#EEEEEE'
                            }
                        }}><Typography>Cancel</Typography></Button>
            </Stack>
          </Box>
        </Modal>
      </div>
    );
}


const CurrentOrderPanel = ({currentOrder}) => {

    const orderId = 32
    if(!orderId) {
        return (
            <LoadingScreen/>
        )
    }
    return (
        <Box spacing={1} p={3} sx={{
            backgroundColor: '#003049',
            borderRadius: '4px',
            maxWidth:'60000px',
            maxHeight:'100px'
        }}>
            <Stack direction="row" justifyContent="space-between">
                <Grid>
                    <Typography align="left" fontWeight={600} sx={{color:'white'}}>Current Order</Typography>
                    <Typography align="left" fontWeight={600} sx={{color:'white'}}>{orderId}</Typography>
                </Grid>
                <Stack direction="row" spacing={1}>
                    <Stack direction="column" justifyContent="center">
                        <Typography fontSize={20} fontWeight={600} sx={{color:'white'}}>
                            Ordered...
                        </Typography>
                    </Stack>
                    <Box sx={{
                        "min-width": "50px",
                        backgroundColor: '#979797'
                    }}>
                        <Typography sx={{color:'white', fontSize: '16px'}}>
                            <Typography fontWeight={600} fontSize={20}>12</Typography>
                            <Typography fontWeight={600}>min</Typography>
                        </Typography>
                    </Box>
                </Stack>

                <Stack direction="row" spacing={1}>
                    <Stack direction="column" justifyContent="center">
                        <Typography fontSize={20} fontWeight={600} sx={{color:'white'}}>
                            Ready in...
                        </Typography>
                    </Stack>
                    <Box sx={{
                        "min-width": "50px",
                        backgroundColor: '#979797'
                    }}>
                        <Typography sx={{color:'white', fontSize: '16px'}}>
                            <Typography fontWeight={600} fontSize={20}>3</Typography>
                            <Typography fontWeight={600}>min</Typography>
                        </Typography>
                    </Box>
                    <Stack direction="column" justifyContent="space-between">
                        <Button variant="contained" sx={{
                            "min-width": "0.5px",
                            "min-height": "0.5px",
                            backgroundColor:'#979797',
                            "&:hover":{
                                color: "#003049",
                                backgroundColor: '#EEEEEE'
                            }
                        }}>
                            <Typography fontSize={8} fontWeight={900}>+</Typography>
                        </Button>
                        <Button variant="contained" sx={{
                            "min-width": "1px",
                            "min-height": "1px",
                            backgroundColor:'#979797',
                            "&:hover":{
                                color: "#003049",
                                backgroundColor: '#EEEEEE'
                            }
                        }}>
                            <Typography fontSize={8} fontWeight={900}>-</Typography>
                        </Button>
                    </Stack>
                </Stack>

                <Stack direction="column" justifyContent="center">
                    <ManageOrderModal {...currentOrder}/>
                </Stack>
            </Stack>
        </Box>
    )
}


export default OrdersPage;