import { Grid, Stack, Box, Typography } from '@mui/material'
import React from 'react'
import Calendar from '../components/HomePage/Calendar'
import HomePageLogo from '../components/HomePage/HomePageLogo'
import InfoBar from '../components/HomePage/InfoBar'
import OrderedMenu from '../components/HomePage/OrderedMenu'
import Sidebar from '../components/HomePage/Sidebar'
import StatBar from '../components/HomePage/StatBar'



class DashboardPage extends React.Component {
    
    render() {
        return (
            <Stack direction="row" spacing={5} justifyContent="center" className="Dashboard-page">
                <Sidebar></Sidebar>
                <Box flex={0.75}>
                    <Stack direction="column" spacing={1}>
                        <Calendar></Calendar>
                        <Stack direction="column" spacing={3}>
                            <InfoBar></InfoBar>
                            <Typography align="left" fontWeight={600} fontSize={18}>Items Ordered</Typography>
                            <OrderedMenu></OrderedMenu>
                        </Stack>
                    </Stack>
                </Box>
                <Stack direction='column' flex={0.2} spacing={2}>
                    <Typography textAlign="left" variant="h6" component="h2" fontWeight={600} fontSize={22} mt={2}>
                        Overall Statistics
                    </Typography>
                    <StatBar></StatBar>
                    <StatBar></StatBar>
                    <StatBar></StatBar>
                    <StatBar></StatBar>
                </Stack>
            </Stack>

        )
    }
}

export default DashboardPage