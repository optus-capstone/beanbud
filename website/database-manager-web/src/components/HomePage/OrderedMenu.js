import FastfoodOutlinedIcon from '@mui/icons-material/FastfoodOutlined';
import React from 'react';
import { Typography, Grid, Stack, Box, makeStyles, Button } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


class ItemRow extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            disable: props.disable,
            name: props.name,
            orders: props.orders,
            ppu: props.ppu,
            revenue: props.revenue
        }
    }

    render() {
        return(
            <Stack direction="row" spacing={20}>
                <Stack direction='row' spacing={0.5}>
                    <FastfoodOutlinedIcon id="icon-enable"></FastfoodOutlinedIcon>
                    <Typography>{this.state.name}</Typography>
                </Stack>
                <Typography>{this.state.orders}</Typography>
                <Typography>${this.state.ppu}</Typography>
                <Typography>${this.state.revenue}</Typography>
            </Stack>

        )
    }
}

let ORDERS = []
for (let i = 0; i < 3; i++)
{
    ORDERS[i] = {
        item: 'Chicken Burger',
        orders: 23,
        ppu: 8.5,
        revenue: 195.50
    }
}


class OrderedMenu extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                <TableRow>
                    <TableCell>Order Items</TableCell>
                    <TableCell>Orders</TableCell>
                    <TableCell>PPU</TableCell>
                    <TableCell>Revenue</TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {ORDERS.map((row) => (
                    <TableRow key={row.name} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                        <TableCell>
                            <Grid container>
                                <Grid item lg={2}>
                                    <FastfoodOutlinedIcon id="icon-enable"/>
                                </Grid>
                                <Grid item lg={10}>
                                    <Typography fontWeight={600}>{row.item}</Typography>
                                </Grid>
                            </Grid>
                        </TableCell>
                        <TableCell>{row.orders}</TableCell>
                        <TableCell>{row.ppu}</TableCell>
                        <TableCell>{row.revenue}</TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
            </TableContainer>
        )
    }
}


export default OrderedMenu