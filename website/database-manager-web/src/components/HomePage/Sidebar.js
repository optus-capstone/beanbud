import React from "react";
import {Box, Button, Grid, Paper, Stack, Typography} from '@mui/material'
import CoffeeSharpIcon from '@mui/icons-material/CoffeeSharp';
import InsertChartSharpIcon from '@mui/icons-material/InsertChartSharp';
import LocalMallSharpIcon from '@mui/icons-material/LocalMallSharp';
import ManageAccountsSharpIcon from '@mui/icons-material/ManageAccountsSharp';
import LogoutSharpIcon from '@mui/icons-material/LogoutSharp';
import HomePageLogo from './HomePageLogo.js';

import { Link } from "react-router-dom";
export class Sidebar extends React.Component {
    render() {
        return (
            <Stack direction='column' spacing={3}>
                <Stack direction='row' justifyContent='center'>
                    <HomePageLogo>
                    </HomePageLogo>
                </Stack>
                <Link to='/products' style={{
                    textDecoration: 'none'
                }}>
                    <Button
                        id='products' color="success" variant="contained" className="Sidebar-box" sx={{
                            "min-height": "100px",
                            "min-width": "110px",
                            color:"#FFFFFF",
                            backgroundColor:'#003049',
                            "&:hover":{
                                color: "#003049",
                                backgroundColor: '#EEEEEE'
                            }    
                        }}>
                            <Grid>
                                <CoffeeSharpIcon/>
                                <Typography textAlign="center" textTransform="none" fontWeight={600}>Products</Typography>
                            </Grid>
                    </Button>
                </Link>
            
                <Link to='/home' style={{
                    textDecoration: 'none'
                }}>
                    <Button
                        id='dashboard' className="Sidebar-box" variant="contained" sx={{
                            "min-height": "100px",
                            "min-width": "100px",
                            color:"#FFFFFF",
                            backgroundColor:'#003049',
                            "&:hover":{
                                color: "#003049",
                                backgroundColor: '#EEEEEE'
                            }    
                        }}>
                            <Grid>
                                <InsertChartSharpIcon/>
                                <Typography textAlign="center" textTransform="none" fontWeight={600}>Dashboard</Typography>
                            </Grid>
                    </Button>
                </Link>


                <Link to='/orders' style={{
                    textDecoration: 'none'
                }}>
                    <Button
                        id='orders' className="Sidebar-box" variant="contained" sx={{
                            "min-height": "100px",
                            "min-width": "100px",
                            color:"#FFFFFF",
                            backgroundColor:'#003049',
                            "&:hover":{
                                color: "#003049",
                                backgroundColor: '#EEEEEE'
                            }    
                        }}>
                            <Grid>
                                <LocalMallSharpIcon/>
                                <Typography textAlign="center" textTransform="none" fontSize={13} fontWeight={600}>Online Orders</Typography>
                            </Grid>

                    </Button>
                </Link>

                <Button
                    id='setting' className="Sidebar-box" variant="contained" sx={{
                        "min-height": "100px",
                        "min-width": "100px",
                        color:"#FFFFFF",
                        backgroundColor:'#003049',
                        "&:hover":{
                            color: "#003049",
                            backgroundColor: '#EEEEEE'
                        }    
                    }}>
                    <Grid>
                        <ManageAccountsSharpIcon/>
                        <Typography textAlign="center" textTransform="none" fontWeight={600}>Settings</Typography>
                    </Grid>
                </Button>

                <Link to='/' style={{
                    textDecoration: 'none'
                }}>
                    <Button
                        id='logout' className="Sidebar-box" variant="contained" sx={{
                            "min-height": "100px",
                            "min-width": "120px",
                            color:"#FFFFFF",
                            backgroundColor:'#003049',
                            "&:hover":{
                                color: "#003049",
                                backgroundColor: '#EEEEEE'
                            }    
                        }}>
                        <Grid>
                            <LogoutSharpIcon/>
                            <Typography textAlign="center" textTransform="none" fontWeight={600}>Logout</Typography>
                        </Grid>
                    </Button>
                </Link>
            </Stack>
        )
    }
}



export default Sidebar;