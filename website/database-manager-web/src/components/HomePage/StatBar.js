import React, {PureComponent} from 'react'
import Box from '@mui/material/Box'
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const data = [
    {
        week: 'Week 1',
        ChickenBurger: 1022,
        BigMac: 908,
        DoubleQuarterPounder:682
    },
    {
        week: 'Week 2',
        ChickenBurger: 2212,
        BigMac: 1022,
        DoubleQuarterPounder: 986
    },
    {
        week: 'Week 3',
        ChickenBurger: 680,
        BigMac: 1896,
        DoubleQuarterPounder: 1201
    }
]

class BarGraph extends React.Component {
    constructor(props) { super(props)}

    render() {
        return (        
        <BarChart
            width={250}
            height={200}
            data={data}
        >
            <XAxis dataKey='week' />
            <Legend iconSize={18}verticalAlign='bottom' layout='vertical' iconType='plainline'/>
            <Bar sx={{ borderRadius: '16px'}} dataKey="ChickenBurger" fill="#003049" />
            <Bar sx={{ borderRadius: '16px'}} dataKey="BigMac" fill="#00669b" />
            <Bar sx={{ borderRadius: '16px'}} dataKey="DoubleQuarterPounder" fill="#008ed8" />
        </BarChart>)
    }
}

class StatBar extends PureComponent {
    render() {
        return (
            <Box>
                <BarGraph/>
            </Box>
        )
    }


    
}

export default StatBar