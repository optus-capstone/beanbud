import React from 'react';
import { Box, Button, Grid, Stack } from '@mui/material/'
import { Typography } from '@mui/material';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import ReceiptIcon from '@mui/icons-material/Receipt';
import PeopleIcon from '@mui/icons-material/People';
import PhoneIphoneSharpIcon from '@mui/icons-material/PhoneIphoneSharp';

export class InfoBar extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Stack direction='row' justifyContent="space-between">
                <Box className='Info-box'>
                    <Grid>
                        <AttachMoneyIcon/>
                        <Typography>$12,456.04</Typography>
                        <Typography>Total Revenue</Typography>
                    </Grid>
                </Box>
            
                <Box className='Info-box'>
                    <ReceiptIcon/>
                    <Typography>1039</Typography>
                    <Typography>Total Orders</Typography>
                </Box>

                <Box className='Info-box'>
                    <PeopleIcon/>
                    <Typography>30</Typography>
                    <Typography>Orders Completed</Typography>
                </Box>

                <Box className='Info-box'>
                    <PhoneIphoneSharpIcon/>
                    <Typography>199</Typography>
                    <Typography>In App Orders</Typography>
                </Box>
            </Stack>
        )
    }
}

export default InfoBar