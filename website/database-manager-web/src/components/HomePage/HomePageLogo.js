import { Avatar } from '@mui/material'
import React from 'react'



export class HomePageLogo extends React.Component {
    constructor(props) {
        super(props)
    }


    render() {
        return (
            <Avatar sx={{ width: '100px', height: '100px'}} alt="Beanbud Logo" src="../beanbudlogo.png" ></Avatar>
        )
    }
}

export default HomePageLogo