import React, {useState, useEffect} from 'react';
import Box from '@mui/material/Box'
import { Typography } from '@mui/material';



export function Calendar() {
    const [dateString, setSeconds] = useState(0)
    useEffect(() => {
        const interval = setInterval(() => {
            const date = new Date()
            const userLocale = navigator.languages && navigator.languages.length ? navigator.languages[0] : navigator.language;
            var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
            setSeconds(dateString => date.toLocaleTimeString(userLocale, options));
        }, 1000);
        return () => clearInterval(interval);
        }, []);
    return (
        <Box align="left" mb={4} fontWeight={600} sx={{fontSize: 17}}>
            {dateString}
        </Box>
    )
}


export default Calendar;