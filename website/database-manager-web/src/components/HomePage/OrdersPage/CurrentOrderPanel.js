import { Grid, Typography,Button, Box, Stack, ButtonBase, ButtonGroup, Modal} from "@mui/material"
import { borderRadius, maxHeight, maxWidth } from "@mui/system"
import React from "react"
import LoadingScreen from "../../../pages/LoadingScreen";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
  };

const ManageOrderModal = () => {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => {
      setOpen(true);
    };
    const handleClose = () => {
      setOpen(false);
    };
  
    return (
      <div>
        <Button  variant="contained" sx={{backgroundColor:'#979797', "&:hover":{backgroundColor: '#EE817E'}}} onClick={handleOpen}><Typography textTransform="none" fontSize={20} fontWeight={600}>Manage Order</Typography></Button >
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="parent-modal-title"
          aria-describedby="parent-modal-description"
        >
          <Box sx={{ ...style, width: 400 }}>

            <p id="parent-modal-description">
              Complete Order?
            </p>
            <Stack direction="row" spacing={2}>
                <Button  sx={{
                            color:"#FFFFFF",
                            backgroundColor:'#003049',
                            "&:hover":{
                                color: "#003049",
                                backgroundColor: '#EEEEEE'
                            }
                        }}><Typography>Complete</Typography></Button>
                        
                <Button sx={{
                            color:"#FFFFFF",
                            backgroundColor:'#003049',
                            "&:hover":{
                                color: "#003049",
                                backgroundColor: '#EEEEEE'
                            }
                        }}><Typography>Cancel</Typography></Button>
            </Stack>
          </Box>
        </Modal>
      </div>
    );
}


class CurrentOrderPanel extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            customerName: props.name,
            orderId: props.order_id,
            timeOrdered: props.time,
            estimateTimeCompletiob: props.timer
        }
        console.log('Props: ' +props.order_id)
    }


    render() {

        const orderId = this.state.orderId
        if(!orderId) {
            return (
                <LoadingScreen/>
            )
        }
        return (
            <Box spacing={1} p={3} sx={{
                backgroundColor: '#003049',
                borderRadius: '4px',
                maxWidth:'60000px',
                maxHeight:'100px'
            }}>
                <Stack direction="row" justifyContent="space-between">
                    <Grid>
                        <Typography align="left" fontWeight={600} sx={{color:'white'}}>Current Order</Typography>
                        <Typography align="left" fontWeight={600} sx={{color:'white'}}>{orderId}</Typography>
                    </Grid>
                    <Stack direction="row" spacing={1}>
                        <Stack direction="column" justifyContent="center">
                            <Typography fontSize={20} fontWeight={600} sx={{color:'white'}}>
                                Ordered...
                            </Typography>
                        </Stack>
                        <Box sx={{
                            "min-width": "50px",
                            backgroundColor: '#979797'
                        }}>
                            <Typography sx={{color:'white', fontSize: '16px'}}>
                                <Typography fontWeight={600} fontSize={20}>12</Typography>
                                <Typography fontWeight={600}>min</Typography>
                            </Typography>
                        </Box>
                    </Stack>

                    <Stack direction="row" spacing={1}>
                        <Stack direction="column" justifyContent="center">
                            <Typography fontSize={20} fontWeight={600} sx={{color:'white'}}>
                                Ready in...
                            </Typography>
                        </Stack>
                        <Box sx={{
                            "min-width": "50px",
                            backgroundColor: '#979797'
                        }}>
                            <Typography sx={{color:'white', fontSize: '16px'}}>
                                <Typography fontWeight={600} fontSize={20}>3</Typography>
                                <Typography fontWeight={600}>min</Typography>
                            </Typography>
                        </Box>
                        <Stack direction="column" justifyContent="space-between">
                            <Button variant="contained" sx={{
                                "min-width": "0.5px",
                                "min-height": "0.5px",
                                backgroundColor:'#979797',
                                "&:hover":{
                                    color: "#003049",
                                    backgroundColor: '#EEEEEE'
                                }
                            }}>
                                <Typography fontSize={8} fontWeight={900}>+</Typography>
                            </Button>
                            <Button variant="contained" sx={{
                                "min-width": "1px",
                                "min-height": "1px",
                                backgroundColor:'#979797',
                                "&:hover":{
                                    color: "#003049",
                                    backgroundColor: '#EEEEEE'
                                }
                            }}>
                                <Typography fontSize={8} fontWeight={900}>-</Typography>
                            </Button>
                        </Stack>
                    </Stack>

                    <Stack direction="column" justifyContent="center">
                        <ManageOrderModal/>
                    </Stack>
                </Stack>
            </Box>
        )
    }
}

export default CurrentOrderPanel