import React from 'react';
import Grid from '@mui/material/Grid'
import { Box, Button, Stack, Typography } from '@mui/material';



class OrdersList extends React.Component {


    render () {
        return (
            <Stack direction="column" spacing={5}>
                <Button className="Order-box" sx={{
                    color:"#FFFFFF",
                    backgroundColor:'#003049',
                    "&:hover":{
                        color: "#003049",
                        backgroundColor: '#EEEEEE'
                    }
                }}>
                    <Grid>
                        <Typography textAlign="center" textTransform="none" fontWeight={600}>Test</Typography>
                        <Typography textAlign="center" textTransform="none" fontWeight={600}>Id: DHASB12</Typography>
                    </Grid>
                </Button>
                <Button className="Order-box" sx={{
                    color:"#FFFFFF",
                    backgroundColor:'#003049',
                    "&:hover":{
                        color: "#003049",
                        backgroundColor: '#EEEEEE'
                    }
                }}>
                    <Grid>
                        <Typography textAlign="center" textTransform="none" fontWeight={600}>Test</Typography>
                        <Typography textAlign="center" textTransform="none" fontWeight={600}>Id: DHASB12</Typography>
                    </Grid>
                </Button>
                <Button className="Order-box" sx={{
                    color:"#FFFFFF",
                    backgroundColor:'#003049',
                    "&:hover":{
                        color: "#003049",
                        backgroundColor: '#EEEEEE'
                    }
                }}>
                    <Grid>
                        <Typography textAlign="center" textTransform="none" fontWeight={600}>Test</Typography>
                        <Typography textAlign="center" textTransform="none" fontWeight={600}>Id: DHASB12</Typography>
                    </Grid>
                </Button>
                <Button className="Order-box" sx={{
                    color:"#FFFFFF",
                    backgroundColor:'#003049',
                    "&:hover":{
                        color: "#003049",
                        backgroundColor: '#EEEEEE'
                    }
                }}>
                    <Grid>
                        <Typography textAlign="center" textTransform="none" fontWeight={600}>Test</Typography>
                        <Typography textAlign="center" textTransform="none" fontWeight={600}>Id: DHASB12</Typography>
                    </Grid>
                </Button>
                <Button className="Order-box" sx={{
                    color:"#FFFFFF",
                    backgroundColor:'#003049',
                    "&:hover":{
                        color: "#003049",
                        backgroundColor: '#EEEEEE'
                    }
                }}>
                    <Grid>
                        <Typography textAlign="center" textTransform="none" fontWeight={600}>Test</Typography>
                        <Typography textAlign="center" textTransform="none" fontWeight={600}>Id: DHASB12</Typography>
                    </Grid>
                </Button>
                

            </Stack>
        )
    }
}

export default OrdersList;
