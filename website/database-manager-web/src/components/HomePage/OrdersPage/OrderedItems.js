import React from 'react';
import { Typography, Grid, Stack, Box, makeStyles, Button } from '@mui/material';
import FastfoodIcon from '@mui/icons-material/Fastfood';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

let ORDERS = []
for (let i = 0; i < 4; i++)
{
    ORDERS[i] = {
        item: 'Chicken Burger',
        custom: ['No lettuce', 'Extra cheese'],
        quantity: 1,
        ppu: 8.5,
        time: "17:24"
    }
}

class OrderedItems extends React.Component {
    render() {
        return (
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                <TableRow>
                    <TableCell>Item</TableCell>
                    <TableCell>Quantity</TableCell>
                    <TableCell>PPU</TableCell>
                    <TableCell>Time</TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {ORDERS.map((row) => (
                    <TableRow key={row.name} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                        <TableCell>
                            <Grid container>
                                <Grid item lg={2}>
                                    <FastfoodIcon/>
                                </Grid>
                                <Grid item lg={10}>
                                    <Typography fontWeight={600}>{row.item}</Typography>
                                    <Typography>{row.custom[0]}</Typography>
                                    <Typography>{row.custom[1]}</Typography>
                                </Grid>
                            </Grid>
                        </TableCell>
                        <TableCell>{row.quantity}</TableCell>
                        <TableCell>{row.ppu}</TableCell>
                        <TableCell>{row.time}</TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
            </TableContainer>
        )
    }
}

export default OrderedItems