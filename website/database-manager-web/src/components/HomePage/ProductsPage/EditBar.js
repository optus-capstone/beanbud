import React from 'react'
import { Box, Button, Stack, TextField, Typography } from '@mui/material'

const EditBar = () => {
  return (
    <Stack direction="column" spacing={2}>
        <Box spacing={1} p={3} sx={{
            backgroundColor: '#003049',
            "min-height":'100px'
        }}>
            <Stack direction="column" justifyContent="center">
            <Typography color="white" fontSize={20} fontWeight={600} mb={2}>Edit Product</Typography>
            </Stack>
        </Box>
        <Typography align="left" fontWeight={600} fontSize={20}>Product Details</Typography>
        <Typography align="left">Item Name</Typography>
        <TextField id="filled-basic" label="Big Breakfast" variant="filled" size="small" sx={{
            backgroundColor: 'white',
            borderRadius: "8px",
            "min-width": "50px"
        }}/>
        <Typography align="left">Selling Price</Typography>
        <TextField id="filled-basic" label="$22.99" variant="filled" size="small" sx={{
            backgroundColor: 'white',
            borderRadius: "8px",
            "min-width": "50px"
        }}/>
        <Typography align="left">Category</Typography>
        <TextField id="filled-basic" label="Breakfast" variant="filled" size="small" sx={{
            backgroundColor: 'white',
            borderRadius: "8px",
            "min-width": "50px"
        }}/>
        <Typography align="left">Additional Details (Optional)</Typography>
        <Button variant="contained" sx={{
            backgroundColor: "#003049",
            "&:hover": {
                backgroundColor: '#003049',
            }
        }}>Choose Photo</Button>

        <Button variant="contained">Change Product</Button>

        <Button variant="contained" sx={{
            backgroundColor: "#F4261A",
            "&:hover": {
                backgroundColor: '#F4261A',
            }
        }}>Remove Product</Button>
    </Stack>
  )
}

export default EditBar