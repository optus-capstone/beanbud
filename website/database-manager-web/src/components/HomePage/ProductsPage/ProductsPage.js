import { Box, Stack, Typography } from '@mui/material';
import React from 'react'
import Calendar from '../components/HomePage/Calendar';
import AddProductPanel from '../components/HomePage/ProductsPage/AddProductPanel';
import Item from '../components/HomePage/ProductsPage/Item';
import ListProductsPanel from '../components/HomePage/ProductsPage/ListProductsPanel';
import Sidebar from '../components/HomePage/Sidebar';
import AddBar from '../components/HomePage/ProductsPage/AddBar';
import EditBar from '../components/HomePage/ProductsPage/EditBar';

class ProductsPage extends React.Component {
    render() {
        return (
            <Box>
                <Stack direction="row" spacing={5} justifyContent="center" className="Dashboard-page">
                    <Sidebar></Sidebar>
                    <Box flex={0.75}>
                        <Stack direction="column" spacing={1}>
                            <Calendar></Calendar>

                            <Stack direction="column" spacing={3}>
                                <ListProductsPanel></ListProductsPanel>
                            </Stack>
                        </Stack>
                    </Box>

                    <Stack direction='column' flex={0.2} spacing={2}>
                        <Typography variant="h6" component="h2" fontWeight={600} fontSize={22} mt={2}>
                            {/* <AddBar></AddBar> OR <EditBar></EditBar>*/}

                        </Typography>
                        <EditBar></EditBar>
                    </Stack>
                </Stack>
            </Box>
        )
    }
}

export default ProductsPage;