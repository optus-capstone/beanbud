import { Typography, Box } from '@mui/material';
import React from 'react';





const CategorySelector = (props) => {
    const list = props.list;
    return (
        <Box>
            <Typography>Choose Category</Typography>
            <Typography>All Category</Typography>
        </Box>
    )
}



const ProductBox = (props) => {
    return (
        <Box>
            <Typography>{props.name}</Typography>
            <Typography>{props.category}</Typography>
            <Typography>${props.price}</Typography>
        </Box>
    )
}

const AddProductButton = () => {
    <Box sx={{borderRadius:'32px'}}>
        <Typography>Add New Product</Typography>
    </Box>
}

export {
    CategorySelector,
    ProductBox,
    AddProductButton,
}
