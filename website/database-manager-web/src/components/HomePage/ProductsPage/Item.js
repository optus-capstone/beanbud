import { Avatar, Box, Grid, Stack, Typography } from '@mui/material'
import React from 'react'

const Item = () => {
  return (
    <Box spacing={1} p={2} sx={{
        backgroundColor: '#EEEEEE',
        borderRadius: "25px",
        "min-height":'25px',
        "&:hover": {
            backgroundColor: '#CCCCCC'
        }
    }}>
        <Stack direction="row" justifyContent="space-around">
            <Stack direction="row" spacing={2}>
                <Avatar></Avatar>
                <Grid>
                    <Typography align="left" color="#003049" fontSize={16} fontWeight={600}>Big Breakfast</Typography>
                    <Typography align="left" color="#003049" fontSize={13} fontWeight={400}>Breakfast</Typography>
                </Grid>
            </Stack>
            <Typography align="right" color="#003049" fontSize={16} fontWeight={600}>$22.99</Typography>
        </Stack>
        
    </Box>
  )
}

export default Item