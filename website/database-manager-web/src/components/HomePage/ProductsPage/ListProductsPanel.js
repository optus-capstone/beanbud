import React from 'react'
import  { Avatar, Box, Button, Grid, Input, InputBase, List, ListItem, ListItemAvatar, ListItemText, TextField, Typography } from '@mui/material'
import SearchIcon from '@mui/icons-material/Search';
import FolderIcon from '@mui/icons-material/Folder';
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';
import { Stack } from '@mui/system';
import Item from './Item.js';

class ListProductsPanel extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <Stack direction="column" spacing={2}>
                <Box spacing={1} p={3} sx={{
                    backgroundColor: '#003049',
                    "min-height":'100px'
            }}>
                    <Stack direction="column" justifyContent="center">
                    <Typography color="white" fontSize={20} fontWeight={600} mb={2}>List Products</Typography>
                    <TextField id="filled-basic" label="Search name product" variant="filled" size="small" sx={{
                        backgroundColor: 'white',
                        borderRadius: "8px",
                        "min-width": "50px"
                    }}/>
                    </Stack>
                </Box>

                <Box spacing={1} p={2} sx={{
                    backgroundColor: '#003049',
                    borderRadius: "25px",
                    "min-width": '50%'
                }}>
                    <Typography align="left" color="white" fontSize={12} fontWeight={300}>Choose Category</Typography>
                    <Typography align="left" color="white" fontWeight={500}>All Categories</Typography>
                </Box>
                <Item></Item>
                <Item></Item>
                <Item></Item>

                <Button spacing={1} p={2} sx={{
                    backgroundColor: '#003049',
                    color: '#FFFFFF',
                    borderRadius: "25px",
                    "min-width": '50%',
                    "min-height": '50px',
                    "&:hover": {
                        color: "#FFFFFF",
                        backgroundColor: "#003049"
                    }
                }}>
                    <Typography align="left" textTransform="none" fontSize={16} fontWeight={600}>Add Product</Typography>
                </Button>
            </Stack>
            
            
        )
    }
}

export default ListProductsPanel;