import { Grid, Typography, Box, Stack, TextField, MenuItem, Button} from '@mui/material'
import React from 'react'
import PhotoOutlinedIcon from '@mui/icons-material/PhotoOutlined';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

const categories = [
    {
        name: 'Burgers'
    },
    {
        name: 'Meals'
    },
    {
        name: 'Cold Drinks'
    },
    {
        name: 'Warm Drinks'
    },
]

class AddProductPanel extends React.Component{
    constructor(props) {
        super(props)
        
    }


    render() {
        return (
            <Stack className="Home-page" direction="column" spacing={2}>
                <Box className="Box-color" sx={{width:300,minHeight:150}}><Typography sx={{fontSize:24}}>Add Product</Typography></Box>
                <Typography>Product Details</Typography>
                <Box>
                    <Typography>Item name</Typography>
                    <TextField required sx={{ borderRadius:64, width: '105%'}} id="outlined-email-input" label="Product Name" type="text"/>
                    <Typography>Selling Price</Typography>
                    <TextField required sx={{ borderRadius:64, width: '105%'}} id="outlined-email-input" label="Price" type="decimal"/>
                    <Typography>Category</Typography>
                    <TextField required sx={{ borderRadius:64, width: '105%'}} id="select-category" select label="Select" value={categories} helperText="Select your product Category">
                        {categories.map((option) => (
                            <MenuItem key={option.name} value={option.name}>
                            {option.name}
                            </MenuItem>
                        ))}
                    </TextField>
                    
                  
                    <Typography>Additional Details (Optional)</Typography>
                    
                    <Stack direction='row' spacing={5}>
                    <PhotoOutlinedIcon/>
                    <Button
                        variant="contained"
                        component="label"
                        >
                        Upload File
                        <input
                            type="file"
                            hidden
                        />
                    </Button>
                    </Stack>

                    <TextField
                        sx={{ borderRadius:64, width: '105%'}}
                        id="outlined-multiline-flexible"
                        label="Description"
                        multiline
                        minRows={3}
                        maxRows={3}
                        />

                    <Grid container spacing={2}>
                            <Grid item xs={8}>
                                <Typography>Make Live Now</Typography>
                            </Grid>
                            <Grid item xs={1}>
                                <Button variant="contained">
                                    Test
                                </Button>
                            </Grid>
                    </Grid>
                    <Button variant="contained">
                        Add new Product
                    </Button>
                    
                    <Button variant="text" color="error">
                        <DeleteForeverIcon/>
                        Delete Product
                    </Button>
                </Box>
            </Stack>
        )
    }
}

export default AddProductPanel