import React from 'react';
import Box from '@mui/material/Box'
import { TextField, Link, Avatar,Stack, Button, Typography } from '@mui/material';
import axios from 'axios';

export class LoginPagePanel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
    }
    render() {
        const onChangeUsername = (e) => {
            this.state.username = e.target.value
        }

        const onChangePassword = (e) => {
            this.state.password = e.target.value
        }
        const verifyLogin = () => {
            const addr = `http://localhost:3001/account/verify/${this.state.username}/${this.state.password}`
            const verified = axios.get(addr).then(res => res.data).then(dataJson => {
                return dataJson
            })
            if(verified) {
                document.getElementById("loginButton").href = '/home';
            }
            else {
                document.getElementById("loginButton").href = '/';
            }
           
        }

        return (
                <Box  className='Login-panel' sx={{
                    width: '500px',
                    height: '600px', 
                    borderRadius: '8px'}}>
                        <h1></h1>
                        <Stack className='Login-panel' spacing={2}>
                        <Avatar sx={{ width: '100px', height: '100px'}} alt="Beanbud Logo" src="./beanbudlogo.png" />
                        <Box color="black">Beanbud</Box>
                        <TextField required sx={{ width: '50%'}} id="username-input" label="Username" type="email" onChangeUsername={onChangeUsername}/>
                        <TextField required sx={{ width: '50%'}} id="password-input" label="Password" type="password" autoComplete="current-password" onChange={onChangePassword}/>
                        <Button onClick={() => {verifyLogin()}} id="loginButton" sx={{ borderRadius: '32px'}} variant="contained" href='/homepage'
                >Login</Button>
                        </Stack>
                </Box>
        )
    }

}



export default LoginPagePanel;