import React from 'react';
import Box from '@mui/material/Box'
import { TextField, Link, Avatar,Stack, Button } from '@mui/material';


export class SignUpPagePanel extends React.Component {
    render() {
        return (
            <div className="Login-panel">
                                <Box  className='Login-panel' sx={{
                    width: '500px',
                    height: '600px', 
                    borderRadius: '8px'}}>
                        <h1></h1>
                        <Stack className='Login-panel' spacing={2}>
                        <Avatar sx={{ width: '100px', height: '100px'}} alt="Beanbud Logo" src="./beanbudlogo.png" />
                        <Box color="black">Beanbud</Box>
                        <TextField sx={{ width: '50%'}} id="outlined-email-input" label="Username" type="email"/>
                        <TextField sx={{ width: '50%'}} id="outlined-password-input" label="Password" type="password" autoComplete="current-password"/>
                        <Button sx={{ borderRadius: '32px'}} variant="contained">Sign up</Button>
                        </Stack>
                </Box>
            </div>
        )
    }


    onClickSignUp() {
        
    }
}


export default SignUpPagePanel;