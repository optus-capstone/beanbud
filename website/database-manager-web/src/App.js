import logo from './logo.svg';
import './App.css';
import LoginPage from './pages/LoginPage';
import { Routes, Route, Link } from "react-router-dom";
import SignUpPage from './pages/SignUpPage';
import TestPage from './pages/TestPage'
import HomePage from './pages/HomePage';
import ProductsPage from './pages/ProductsPage';
import OrdersPage from './pages/OrdersPage';

function App() {
  return (
    <div className="App">
      <header>
        <Routes>
          <Route path="/" element={<LoginPage/>} />
          <Route path="/signup" element={<SignUpPage/>} />
          <Route path="/home" element={<HomePage/>} />
          <Route path="/products" element={<ProductsPage/>}/>
          <Route path="/orders" element={<OrdersPage/>}/>
          <Route path="/test" element={<TestPage/>}/>
      </Routes>
      </header>
    </div>
  );
}

export default App;