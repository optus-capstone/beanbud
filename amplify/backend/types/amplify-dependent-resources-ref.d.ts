export type AmplifyDependentResourcesAttributes = {
    "auth": {
        "beanbudauth0332cf1f": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string",
            "UserPoolId": "string",
            "UserPoolArn": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string"
        }
    },
    "analytics": {
        "beanbudauth": {
            "Region": "string",
            "Id": "string",
            "appName": "string"
        }
    },
    "api": {
        "BeanBudAuth": {
            "GraphQLAPIKeyOutput": "string",
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    }
}