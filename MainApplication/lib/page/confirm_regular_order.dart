import 'package:BeanBud/classes/cart_item.dart';
import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/classes/regular_order_instance.dart';
import 'package:BeanBud/models/order_model.dart';
import 'package:BeanBud/mysql.dart';
import 'package:flutter/material.dart';

import '../classes/cart.dart';

class ConfirmRegularOrder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    List<CartItem> items = [];
    for(int i = 0; i < Cart.instance.getLength(); i++) {
      items.add(Cart.instance.findItemAtIndex(i));
    }

    return Scaffold(
      //Main top headbar
      appBar: AppBar(
        toolbarHeight: width / 3,
        backgroundColor: Colors.white,
        //Bar
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50.0)),
        ),
        shadowColor: Colors.grey.withOpacity(0.5),
        title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          //Icon
          Padding(
            padding: const EdgeInsets.only(top: 45),
            child: Image.asset(
              "assets/images/icon.png",
              fit: BoxFit.contain,
              height: width / 5,
              width: width / 5,
            ),
          ),
          //Sub-heading
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(top: width / 10, left: 10),
                child: Text(
                  "Bean Bud",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: width / 10,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: width / 7),
                child: Text(
                  "Coffee For You",
                  style: TextStyle(
                    fontSize: width / 25,
                  ),
                ),
              )
            ],
          ),
        ]),
      ),
      //Login Widget
      body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return _buildLoginContainer(context, width, height, items);
          }),
    );
  }
}

Widget _buildLoginContainer(context, width, height, items) {
  return Stack(
    children: [
      //Background circle
      Padding(
        padding: EdgeInsets.only(bottom: height / 10),
        child: Center(child: CustomPaint(painter: CirclePainter(width))),
      ),
      Padding(
        padding: EdgeInsets.only(bottom: height / 10),
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: width / 4,
              ),
              Text(
                "Hey $name!",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: width / 10, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: width / 8,
              ),
              SizedBox(
                width: width / 1.2,
                child: Text(
                  "Would you like what you currently have in your cart to be your regular order?",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: width / 20,
                  ),
                ),
              ),
              SizedBox(
                height: width / 20,
              ),
              SizedBox(
                width: width / 1.3,
                child: Text(
                  "Your regular order can be ordered from the home screen with one tap of a button!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: width / 20,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      //Log In Button
      Padding(
        padding: EdgeInsets.only(top: height / 4),
        child: Center(
          child: SizedBox(
            width: width / 2,
            height: width / 10,
            //Proceed to capture picture upon accepting
            child: ElevatedButton(
              onPressed: () {
                  Mysql().addRegular(OrderModel(
                      orderType: Cart.instance.orderType,
                      orderDate: DateTime.now(),
                      status: "",
                      items: items
                  ));
                  Regular.instance.reset;
                  Mysql().getRegularOrder();
                Navigator.pop(context);
              },
              child: Text(
                'Yes Please!',
                style: TextStyle(
                    fontSize: width / 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
        ),
      ),
      //Skips Facial setup button
      Padding(
        padding: EdgeInsets.only(top: height / 2.5),
        child: Center(
          child: SizedBox(
            child: InkWell(
              //If facial capture is skipped, move to ratings page
              onTap: () {
                Navigator.pop(context);
              },
              child: Text(
                'No Thanks',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: width / 30),
              ),
            ),
          ),
        ),
      ),
      //Bottom bar
      Container(
        margin: EdgeInsets.only(top: height / 1.4),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius:
            const BorderRadius.only(topLeft: Radius.circular(50.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 10,
                blurRadius: 7,
                offset: const Offset(0, 3),
              ),
            ]),
      ),
    ],
  );
}

//Draws Circle
class CirclePainter extends CustomPainter {
  double screenWidth = 0;
  CirclePainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 2 + screenWidth / 5;
    Offset center = const Offset(0, 0);
    Path oval = Path();
    oval.addOval(Rect.fromCircle(center: center, radius: radius));

    Paint shadowPaint = Paint()
      ..color = Colors.black.withOpacity(0.25)
      ..maskFilter = const MaskFilter.blur(BlurStyle.normal, 50);
    canvas.drawPath(oval, shadowPaint);
    // draw circle
    Paint thumbPaint = Paint()
      ..color = const Color(0xFFFFD591)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}