import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:BeanBud/mysql.dart';

import '../kiosk/start_kiosk.dart';
import 'account_page.dart';
import 'cart_page.dart';
import 'menu_page.dart';
import 'home_page.dart';
import 'rewards_page.dart';

enum MenuItem { logout, deleteAccount }

//Homepage of application contains the bottom navigation bar and drop down menu on the appbar

//Initial Starting Widget
class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

@override
class _MainScreenState extends State<MainScreen> {
  //Width variable to return width of current device

  int _selectedIndex = 0;
  final screens = [
    HomePage(),
    RewardsPage(),
    MenuPage(),
    CartPage(),
    const AccountPage(),
  ];

  //State change for bottom bar clickable features
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  //Building the main widget
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    //Scaffold used to hold all child widget of the screen
    return Scaffold(
        //Appbar implementation
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          toolbarHeight: width / 5.5,
          backgroundColor: Colors.white,
          //Bar
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30.0)),
          ),
          shadowColor: Colors.grey.withOpacity(0.5),
          title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            //Icon
            Image.asset(
              "assets/images/icon.png",
              fit: BoxFit.contain,
              height: width / 9,
              width: width / 9,
            ),
            //Sub-heading
            Padding(
              padding: EdgeInsets.only(left: width / 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Bean Bud",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: width / 13,
                    ),
                  ),
                  Text(
                    "Coffee For You",
                    style: TextStyle(
                      fontSize: width / 35,
                    ),
                  )
                ],
              ),
            ),
          ]),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 15.0),
              child: PopupMenuButton<MenuItem>(
                  iconSize: width / 15,
                  icon: const Icon(Icons.menu_sharp, color: Colors.black),
                  onSelected: (value) {
                    if (value == MenuItem.logout) {
                      Amplify.Auth.signOut().then((_) {
                        Navigator.pushReplacementNamed(context, '/');
                      });
                    }
                    if (value == MenuItem.deleteAccount) {
                      Mysql().deleteUser();
                      Amplify.Auth.deleteUser().then((_) {
                        Navigator.pushReplacementNamed(context, '/');
                      });
                    }
                  },
                  padding: EdgeInsets.all(width / 40),
                  itemBuilder: (context) => [
                        PopupMenuItem(
                          value: MenuItem.logout,
                          child: Text('Sign Out',
                              style: TextStyle(fontSize: width / 35)),
                        ),
                        PopupMenuItem(
                          value: MenuItem.deleteAccount,
                          child: Text('Delete your account',
                              style: TextStyle(fontSize: width / 35)),
                        ),
                      ]),
            ),
          ],
        ),
        body: screens[_selectedIndex],
        //Bottom navigation bar
        bottomNavigationBar: Container(
          height: width / 7,
          decoration: const BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.grey,
                blurRadius: 10,
              ),
            ],
          ),
          child: BottomNavigationBar(
            iconSize: width / 20,
            selectedFontSize: width / 40,
            unselectedFontSize: width / 40,
            items: const <BottomNavigationBarItem>[
              //Home Screen (current screen)
              BottomNavigationBarItem(
                icon: Icon(Icons.home_filled),
                label: 'Home',
              ),
              //Rewards Screen
              BottomNavigationBarItem(icon: Icon(Icons.star), label: 'Rewards'),
              //Cafe menu screen (All items)
              BottomNavigationBarItem(
                  icon: Icon(Icons.restaurant_menu), label: 'Menu'),
              //Cart Screen
              BottomNavigationBarItem(
                  icon: Icon(Icons.shopping_cart), label: 'Cart'),
              //Account management screen
              BottomNavigationBarItem(
                icon: Icon(Icons.account_box),
                label: 'Account',
              ),
            ],
            currentIndex: _selectedIndex,
            selectedItemColor: Colors.cyan,
            unselectedItemColor: Colors.black54,
            onTap: _onItemTapped,
            type: BottomNavigationBarType.fixed,
          ),
        ));
  }
}
