import 'package:flutter/material.dart';
import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/customWidgets/AccountSetting.dart';

//Author: Darren Ou Yang
//Last Updated" 23/09/2022
//Page for users to manage their own account

//Your account header
const List<Tab> accountTabs = <Tab>[
  Tab(
      child: SizedBox(
    width: 400,
    child: Center(
      child: Text(
        'Your Account',
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
    ),
  )),
];

class AccountPage extends StatelessWidget {
  const AccountPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    //Header
    final double width = MediaQuery.of(context).size.width;
    return DefaultTabController(
      length: 1,
      child: Builder(builder: (BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            flexibleSpace: Align(
              alignment: Alignment.bottomCenter,
              child: Material(
                color: const Color(0xFFFFD591),
                child: TabBar(
                  isScrollable: true,
                  unselectedLabelColor:
                      const Color(0xFF000000).withOpacity(0.4),
                  tabs: accountTabs,
                ),
              ),
            ),
            backgroundColor: const Color(0xFFFFD591),
          ),
          //Tab Bar
          body: TabBarView(
            children: accountTabs.map((Tab tab) {
              return Column(children: [
                SizedBox(
                  height: width / 3,
                  child: Row(
                    children: [
                      //User Image
                      Padding(
                        padding: const EdgeInsets.only(top: 15, left: 25),
                        child: Card(
                          child: Center(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: const Image(
                                image: AssetImage('assets/images/logo.png'),
                              ),
                            ),
                          ),
                        ),
                      ),
                      //User Name
                      Padding(
                          padding: EdgeInsets.only(left: width / 20),
                          child: Text(
                            name,
                            style: TextStyle(
                                fontSize: width / 15,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Segoe-UI'),
                          )),
                    ],
                  ),
                ),
                //Account Setting options for the user
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.only(top: 15),
                  //Clickable account settings widgets
                  child: ListView(
                    children: const [
                      AccountSetting(title: "Update Profile"),
                      AccountSetting(title: "Your Regular Order"),
                      AccountSetting(title: "Your Facial Recognition"),
                      AccountSetting(title: "Default Payment Methods"),
                      AccountSetting(title: "Past Orders"),
                    ],
                  ),
                )),
              ]);
            }).toList(),
          ),
        );
      }),
    );
  }
}
