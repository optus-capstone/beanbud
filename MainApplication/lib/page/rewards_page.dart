import 'package:BeanBud/classes/global_variables.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

//Rewards page that the user is entitled to, shows the logged in users' coffee loyalty card
//which tracks the amount of coffee's purchased and if they're entitled to a free coffee

//Rewards Tabs
const List<Tab> rewardTabs = <Tab>[
  Tab(
      child: SizedBox(
        width: 400,
        child: Center(
          child: Text(
            'Your Rewards',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
      )),
];

bool _visible = true;
int _counter = 0;

class RewardsPage extends StatefulWidget {
  RewardsPage({super.key});

  @override
    State<RewardsPage> createState() => RewardsPageState();
  }

class RewardsPageState extends State<RewardsPage> {
  @override
  initState() {
    super.initState();
    _counter = 0;
    _visible = true;
    _start();
  }

  _start() async {
    await getCoffeeCount();
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    return DefaultTabController(
      length: 1,
      child: Builder(builder: (BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            flexibleSpace: Align(
              alignment: Alignment.bottomCenter,
              child: Material(
                color: const Color(0xFFFFD591),
                child: TabBar(
                  isScrollable: true,
                  unselectedLabelColor:
                      const Color(0xFF000000).withOpacity(0.4),
                  tabs: rewardTabs,
                ),
              ),
            ),
            backgroundColor: const Color(0xFFFFD591),
          ),
          //Tab Bar
          body: TabBarView(
            children: rewardTabs.map((Tab tab) {
              return Stack(children: [
                //Promotional Images
                _carouselSlider(height, width),

                //Circle
                _rewards(width, height),
              ]);
            }).toList(),
          ),
        );
      }),
    );
  }
}

//todo Carousel slider to show active cafe promotions for future development
_carouselSlider(double height, double width) {
  return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.only(top: width / 20),
        child: CarouselSlider(
            items: [
              Container(
                  margin: const EdgeInsets.all(8.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.asset(
                        "assets/images/promo.png",
                        fit: BoxFit.fill,
                        height: width / 2,
                        width: width / 1.2,
                      )))
            ],
            options: CarouselOptions(
              height: width / 2,
              autoPlay: true,
              autoPlayCurve: Curves.fastOutSlowIn,
              enableInfiniteScroll: true,
              autoPlayAnimationDuration: const Duration(milliseconds: 900),
              viewportFraction: 0.8,
            )),
      ));
}

//Coffee loyalty card
_rewards(double width, double height) {
  return Stack(children: [
    Align(
        alignment: Alignment.bottomLeft,
        child: CustomPaint(painter: CirclePainter(width, height))),
    //9th Drink Free
    Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: EdgeInsets.only(bottom: width / 2.3),
        child: Text(
          "Every 9th Drink Free!",
          style: TextStyle(fontSize: width / 15, fontWeight: FontWeight.bold),
        ),
      ),
    ),
    //Rewards Icons
    Padding(
      padding: EdgeInsets.only(bottom: width / 20, left: width / 30),
      child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
        Row(children: [
          _buildCoffeeStamp(width),
          _buildCoffeeStamp(width),
          _buildCoffeeStamp(width),
          _buildCoffeeStamp(width),
        ]),
        Row(children: [
          _buildCoffeeStamp(width),
          _buildCoffeeStamp(width),
          _buildCoffeeStamp(width),
          _buildCoffeeStamp(width),
        ]),
      ]),
    ),
    //Free Drink Icon
    _buildFreeDrinkIcon(width),
  ]);
}

_buildFreeDrinkIcon(width) {
  if (coffeeCount == 8) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: EdgeInsets.only(bottom: width / 8, left: width / 1.35),
        child: Image.asset(
          "assets/images/freeIcon.png",
          color: Colors.green.withOpacity(1),
          colorBlendMode: BlendMode.modulate,
          fit: BoxFit.contain,
          height: width / 4.5,
          width: width / 4.5,
        ),
      ),
    );
  }
  return Align(
    alignment: Alignment.bottomCenter,
    child: Padding(
      padding: EdgeInsets.only(bottom: width / 8, left: width / 1.35),
      child: Image.asset(
        "assets/images/freeIcon.png",
        color: Colors.redAccent.withOpacity(1),
        colorBlendMode: BlendMode.modulate,
        fit: BoxFit.contain,
        height: width / 4.5,
        width: width / 4.5,
      ),
    ),
  );
}

//Build Coffee Stamps
_buildCoffeeStamp(double width) {
  double iconDimensions = width / 7;
  double spaceBetween = width / 45;
  _checkVisible();
  _counter++;

  if(_visible == true) {
    return Padding(
      padding: EdgeInsets.all(spaceBetween),
      child: Image.asset(
        "assets/images/icon.png",
        fit: BoxFit.contain,
        height: iconDimensions,
        width: iconDimensions,
      ),
    );
  }
  return Padding(
      padding: EdgeInsets.all(spaceBetween),
      child: Image.asset(
        "assets/images/icon.png",
        color: Colors.grey.withOpacity(0.2),
        colorBlendMode: BlendMode.modulate,
        fit: BoxFit.contain,
        height: iconDimensions,
        width: iconDimensions,
      ),
    );
}

_checkVisible() {
  if(_counter == coffeeCount) {
    _visible = false;
  }
}

//Circle Drawer
class CirclePainter extends CustomPainter {
  double screenWidth = 0.0;
  double screenHeight = 0.0;

  CirclePainter(double width, double height) {
    screenWidth = width;
    screenHeight = height;
  }

  @override
  void paint(Canvas canvas, Size size) {
    //shadow
    double radius = (screenWidth + screenWidth / 8) / 2.0;
    Offset center = Offset(screenWidth / 2.0, screenWidth / -13);
    // draw circle
    Paint thumbPaint = Paint()
      ..color = const Color(0xFFFFD591)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
