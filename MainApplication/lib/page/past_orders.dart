import 'package:BeanBud/classes/orders.dart';
import 'package:BeanBud/customWidgets/pastOrderList.dart';
import 'package:BeanBud/models/order_model.dart';
import 'package:flutter/material.dart';

//Past orders made by the currently logged in user

class PastOrders extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;

    //Converts the CartItems into CartProduct widget to display
    List<PastOrder> pastOrders = List.empty(growable: true);
    for (int i = 0; i < Orders.instance.getLength(); i++) {
      OrderModel order = Orders.instance.findItemAtIndex(i);
      pastOrders.add(PastOrder(order: order));
    }

    //sorting in descending order
    pastOrders.sort((a, b){
      return (b.order.orderDate).compareTo((a.order.orderDate));
    });

    //Scaffold used to hold all child widget of the screen
    return Scaffold(
      //Appbar implementation
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        toolbarHeight: width / 5.5,
        backgroundColor: Colors.white,
        //Bar
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30.0)),
        ),
        shadowColor: Colors.grey.withOpacity(0.5),
        title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          //Icon
          Image.asset(
            "assets/images/icon.png",
            fit: BoxFit.contain,
            height: width / 9,
            width: width / 9,
          ),
          //Sub-heading
          Padding(
            padding: EdgeInsets.only(left: width / 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Bean Bud",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: width / 13,
                  ),
                ),
                Text(
                  "Coffee For You",
                  style: TextStyle(
                    fontSize: width / 35,
                  ),
                )
              ],
            ),
          ),
        ]),
      ),
        body: ListView(
            children: [
              Column(
                children: [
                  Column(
                    children: pastOrders,
                  ),
                ]
            )
            ])
    );
  }
}