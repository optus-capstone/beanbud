import 'package:flutter/material.dart';
import 'dart:typed_data';

import 'package:BeanBud/classes/cart.dart';
import 'package:BeanBud/classes/cart_item.dart';

//Author: Darren Ou Yang
//Last Updated" 23/09/2022

//Header
const List<Tab> productTabs = <Tab>[
  Tab(
      child: SizedBox(
        width: 400,
        child: Center(
          child: Text(
            'Customise',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
      )),
];

int itemQuantity = 1;

//NEED TO CHANGE TO STATEFUL WIDGET TO CHANGE COLOR OF BUTTON
class ProductPage extends StatefulWidget {
  int id;
  String itemName;
  String itemDescription;
  double itemPrice;
  Uint8List itemImage;
  String itemCategory;

  ProductPage(
      {Key? key,
        required this.id,
        required this.itemName,
        required this.itemDescription,
        required this.itemImage,
        required this.itemPrice,
        required this.itemCategory})
      : super(key: key);

  @override
  State<ProductPage> createState() => ProductPageState(id: id,itemName: itemName, itemImage: itemImage, itemPrice: itemPrice, itemDescription: itemDescription, itemCategory: itemCategory);
}
class ProductPageState extends State<ProductPage> {
  int id;
  String itemName;
  String itemDescription;
  double itemPrice;
  Uint8List itemImage;
  String itemCategory;

  ProductPageState({Key? key,
    required this.id,
    required this.itemName,
    required this.itemDescription,
    required this.itemImage,
    required this.itemPrice,
    required this.itemCategory});

  String? sizeVal = 'medium';
  String? milk = 'full cream';

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery
        .of(context)
        .size
        .width;
    if (itemCategory != "Hot Drink" && itemCategory != "Cold Drink") {
      return Scaffold(
        //Appbar implementation
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          titleSpacing: 15.0,
          backgroundColor: Colors.white,
          toolbarHeight: width / 6.5,
          centerTitle: false,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40.0)),
          ),
          //BeanBud title
          title: Column(
              crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              'BeanBud',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: width / 16,
                  fontFamily: 'Segoe-UI'),
            ),
            Text(
              ' Coffee for you',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w900,
                  fontSize: width / 40,
                  fontFamily: 'Segoe-UI'),
            ),
          ]),
          leading: Padding(
            padding: EdgeInsets.only(left: width / 40),
            child: const Image(
              //AppBar BeanBud Logo
              image: AssetImage('assets/images/icon.png'),
            ),
          ),
        ),
        body: DefaultTabController(
          length: 1,
          child: Builder(builder: (BuildContext context) {
            return Scaffold(
              appBar: AppBar(
                flexibleSpace: Align(
                  alignment: Alignment.bottomCenter,
                  child: Material(
                    color: const Color(0xFFFFD591),
                    child: TabBar(
                      isScrollable: true,
                      unselectedLabelColor:
                      const Color(0xFF000000).withOpacity(0.4),
                      tabs: productTabs,
                    ),
                  ),
                ),
                backgroundColor: const Color(0xFFFFD591),
              ),
              //Tab Bar
              body: TabBarView(
                children: productTabs.map((Tab tab) {
                  return Column(children: [
                    //Item Box
                    Container(
                        width: width,
                        height: width / 1.7,
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              spreadRadius: 5.0,
                              blurRadius: 7,
                              offset: Offset(
                                  0, 0), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            //Item Image
                            ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: Image.memory(itemImage,
                                  fit: BoxFit.fill,
                                  height: width / 3,
                                  width: width / 3,
                                  alignment: Alignment.center),
                            ),
                            //Item Name
                            Text(
                              itemName,
                              style: TextStyle(
                                  fontSize: width / 15,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        )),
                    //Item Options
                    Expanded(
                        child: ListView(
                            children: [
                              //Quantity of the Item
                              const Counter(),
                              //Adds the item to the cart button
                              Padding(
                                padding: EdgeInsets.only(
                                    bottom: 50.0,
                                    top: 20.0,
                                    left: width / 4,
                                    right: width / 4),
                                child: SizedBox(
                                  width: width / 2.5,
                                  height: width / 8,
                                  child: ElevatedButton(
                                    onPressed: () {
                                      CartItem item = _getCartItem();
                                      itemQuantity = 1;
                                      Cart.instance.addToCart(item);
                                      //CHANGE TO IF ALL SELECTIONS SELECTED
                                      Navigator.pop(context);
                                    },
                                    style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                              borderRadius: BorderRadius
                                                  .circular(18.0),
                                            ))),
                                    child: Center(
                                      child: Text(
                                        'Add to Cart',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'AdobeCleanUXBold',
                                            fontSize: width / 20),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ])
                    )
                  ]
                  );
                }).toList(),
              ),
            );
          }),
        ),
      );
    }
    //Scaffold used to hold all child widget of the screen
    return Scaffold(
      //Appbar implementation
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        titleSpacing: 15.0,
        backgroundColor: Colors.white,
        toolbarHeight: width / 6.5,
        centerTitle: false,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40.0)),
        ),
        //BeanBud title
        title: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'BeanBud',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: width / 16,
                fontFamily: 'Segoe-UI'),
          ),
          Text(
            ' Coffee for you',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w900,
                fontSize: width / 40,
                fontFamily: 'Segoe-UI'),
          ),
        ]),
        leading: Padding(
          padding: EdgeInsets.only(left: width / 40),
          child: const Image(
            //AppBar BeanBud Logo
            image: AssetImage('assets/images/icon.png'),
          ),
        ),
      ),
      body: DefaultTabController(
        length: 1,
        child: Builder(builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(
              flexibleSpace: Align(
                alignment: Alignment.bottomCenter,
                child: Material(
                  color: const Color(0xFFFFD591),
                  child: TabBar(
                    isScrollable: true,
                    unselectedLabelColor:
                    const Color(0xFF000000).withOpacity(0.4),
                    tabs: productTabs,
                  ),
                ),
              ),
              backgroundColor: const Color(0xFFFFD591),
            ),
            //Tab Bar
            body: TabBarView(
              children: productTabs.map((Tab tab) {
                return Column(children: [
                  //Item Box
                  Container(
                      width: width,
                      height: width / 1.7,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            spreadRadius: 5.0,
                            blurRadius: 7,
                            offset: Offset(
                                0, 0), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //Item Image
                          ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Image.memory(itemImage,
                                fit: BoxFit.fill,
                                height: width / 3,
                                width: width / 3,
                                alignment: Alignment.center),
                          ),
                          //Item Name
                          Text(
                            itemName,
                            style: TextStyle(
                                fontSize: width / 15,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      )),
                  //Item Options
                  Expanded(
                      child: ListView(
                        children: [
                          //Quantity of the Item
                          const Counter(),
                          //Item Options
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: width / 15,
                                  top: width / 25,
                                  bottom: width / 25),
                              child: Text(
                                "Size",
                                style: TextStyle(fontSize: width / 20,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          DropdownButton<String>(
                            onChanged: (value) =>
                                setState(() =>
                                this.sizeVal = value!),
                            items: [
                              DropdownMenuItem<String>(child: Row(children: [
                                Padding(padding: EdgeInsets.only(left: 25),
                                    child: Text("Small (220ml)",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'AdobeCleanUXBold',
                                            fontSize: width / 30)
                                    )
                                ),
                                SizedBox(width: width / 2.5),
                                Text("- \$1.00",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'AdobeCleanUXBold',
                                      fontSize: width / 30),
                                ),
                              ]), value: "small"),
                              DropdownMenuItem<String>(child: Row(children: [
                                Padding(padding: EdgeInsets.only(left: 25),
                                    child: Text("Medium (320ml)",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'AdobeCleanUXBold',
                                            fontSize: width / 30)
                                    )
                                )
                              ]), value: "medium"),
                              DropdownMenuItem<String>(child: Row(children: [
                                Padding(padding: EdgeInsets.only(left: 25),
                                    child: Text("Large (420ml)",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'AdobeCleanUXBold',
                                            fontSize: width / 30)
                                    )
                                ),
                                SizedBox(width: width / 2.5),
                                Text("+ \$1.00",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'AdobeCleanUXBold',
                                      fontSize: width / 30),
                                ),
                              ]), value: "large"),
                            ],
                            value: sizeVal,
                            isExpanded: true,
                            iconSize: 40,
                            icon: Icon(Icons.keyboard_arrow_down),
                            iconEnabledColor: Colors.black,
                            iconDisabledColor: Colors.grey,
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: width / 15,
                                  top: width / 25,
                                  bottom: width / 25),
                              child: Text(
                                "Milk",
                                style: TextStyle(fontSize: width / 20,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          DropdownButton<String>(
                            onChanged: (value) =>
                                setState(() =>
                                this.milk = value!),
                            items: [
                              DropdownMenuItem<String>(child: Row(children: [
                                Padding(padding: EdgeInsets.only(left: 25),
                                    child: Text("Oat Milk",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'AdobeCleanUXBold',
                                            fontSize: width / 30)
                                    )
                                ),
                                SizedBox(width: width / 2.50 - 2),
                                Text("+ \$0.50",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'AdobeCleanUXBold',
                                      fontSize: width / 30),
                                ),
                              ]), value: "oat"),
                              DropdownMenuItem<String>(child: Row(children: [
                                Padding(padding: EdgeInsets.only(left: 25),
                                    child: Text("Full Cream Milk",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'AdobeCleanUXBold',
                                            fontSize: width / 30)
                                    )
                                ),
                              ]), value: "full cream"),
                              DropdownMenuItem<String>(child: Row(children: [
                                Padding(padding: EdgeInsets.only(left: 25),
                                    child: Text("Almond Milk",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'AdobeCleanUXBold',
                                            fontSize: width / 30)
                                    )
                                ),
                                SizedBox(
                                  width: width / 2.5,
                                ),
                                Text("+ \$0.50",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'AdobeCleanUXBold',
                                      fontSize: width / 30),
                                ),
                              ]), value: "almond"),
                            ],
                            value: milk,
                            isExpanded: true,
                            iconSize: 40,
                            icon: Icon(Icons.keyboard_arrow_down),
                            iconEnabledColor: Colors.black,
                            iconDisabledColor: Colors.grey,
                          ),
                        ],
                      )),
                  //Adds the item to the cart button
                  Padding(
                    padding: EdgeInsets.only(
                        bottom: 50.0,
                        top: 20.0,
                        left: width / 4,
                        right: width / 4),
                    child: SizedBox(
                      width: width / 2.5,
                      height: width / 8,
                      child: ElevatedButton(
                        onPressed: () {
                          CartItem item = _getCartItem();
                          Cart.instance.addToCart(item);
                          itemQuantity = 1;
                          //CHANGE TO IF ALL SELECTIONS SELECTED
                          Navigator.pop(context);
                        },
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                ))),
                        child: Center(
                          child: Text(
                            'Add to Cart',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'AdobeCleanUXBold',
                                fontSize: width / 20),
                          ),
                        ),
                      ),
                    ),
                  ),
                ]);
              }).toList(),
            ),
          );
        }),
      ),
    );
  }

  //Extras added to beverage items
  _getCartItem() {
    if(itemCategory == "Snack" || itemCategory == "Dessert") {
      return CartItem(
        itemId: id,
        itemName: itemName,
        itemDescription: itemDescription,
        category: itemCategory,
        itemImage: itemImage,
        itemPrice: itemPrice,
        itemQuantity: itemQuantity,
        attributes: [],
      );
    }
    //Almond Milk Surcharge (+ $0.5)
    if (milk == 'almond') {
      //Large almond drink
      if (sizeVal == 'large') {
        return CartItem(
            itemId: id,
            itemName: itemName,
            itemDescription: itemDescription,
            category: itemCategory,
            itemImage: itemImage,
            itemPrice: itemPrice + 1.50,
            itemQuantity: itemQuantity,
            attributes: ['Large', 'Almond Milk']
        );
      }
      //Small almond drink
      else if (sizeVal == 'small') {
        return CartItem(
            itemId: id,
            itemName: itemName,
            itemDescription: itemDescription,
            category: itemCategory,
            itemImage: itemImage,
            itemPrice: itemPrice - 0.5,
            itemQuantity: itemQuantity,
            attributes: ['Small', 'Almond Milk']
        );
      }
      //Medium almond drink
      return CartItem(
          itemId: id,
          itemName: itemName,
          itemDescription: itemDescription,
          category: itemCategory,
          itemImage: itemImage,
          itemPrice: itemPrice + 0.5,
          itemQuantity: itemQuantity,
          attributes: ['Medium', 'Almond Milk']
      );
    }
    //Oat Milk Surcharge (+ $0.5)
    if (milk == 'oat') {
      //Large Oat drink
      if (sizeVal == 'large') {
        return CartItem(
            itemId: id,
            itemName: itemName,
            itemDescription: itemDescription,
            category: itemCategory,
            itemImage: itemImage,
            itemPrice: itemPrice + 1.50,
            itemQuantity: itemQuantity,
            attributes: ['Large', 'Oat Milk']
        );
      }
      //Small Oat drink
      else if (sizeVal == 'small') {
        return CartItem(
            itemId: id,
            itemName: itemName,
            itemDescription: itemDescription,
            category: itemCategory,
            itemImage: itemImage,
            itemPrice: itemPrice - 0.5,
            itemQuantity: itemQuantity,
            attributes: ['Small', 'Oat Milk']
        );
      }
      //Medium Oat drink
      return CartItem(
          itemId: id,
          itemName: itemName,
          itemDescription: itemDescription,
          category: itemCategory,
          itemImage: itemImage,
          itemPrice: itemPrice + 0.5,
          itemQuantity: itemQuantity,
          attributes: ['Medium', 'Oat Milk']
      );
    }
    if(milk == "full cream") {
      if(sizeVal == 'large') {
        return CartItem(
            itemId: id,
            itemName: itemName,
            itemDescription: itemDescription,
            category: itemCategory,
            itemImage: itemImage,
            itemPrice: itemPrice + 1.00,
            itemQuantity: itemQuantity,
            attributes: ['Large', 'Full Cream Milk']
        );
      }
      else if(sizeVal == 'small') {
        return CartItem(
            itemId: id,
            itemName: itemName,
            itemDescription: itemDescription,
            category: itemCategory,
            itemImage: itemImage,
            itemPrice: itemPrice - 1.00,
            itemQuantity: itemQuantity,
            attributes: ['Small', 'Full Cream Milk']
        );
      }
      //Medium full cream drink
      return CartItem(
          itemId: id,
          itemName: itemName,
          itemDescription: itemDescription,
          category: itemCategory,
          itemImage: itemImage,
          itemPrice: itemPrice,
          itemQuantity: itemQuantity,
          attributes: ['Medium', 'Full Cream Milk']
      );
    }
  }
}

class Counter extends StatefulWidget {
  const Counter({super.key});
  @override
  State<Counter> createState() => _CounterState();
}

//Quantity Counter
class _CounterState extends State<Counter> {
  @override
  Widget build(BuildContext context) {
    void remove() {
      setState(() {
        if (itemQuantity != 1) {
          itemQuantity--;
        } else {
          itemQuantity = 1;
        }
      });
    }

    void add() {
      setState(() {
        itemQuantity += 1;
      });
    }

    final double width = MediaQuery.of(context).size.width;
    return SizedBox(
      height: width / 5,
      child: Row(
        children: [
          SizedBox(
              width: width / 2.7,
              child: Padding(
                padding: EdgeInsets.only(
                    left: width / 15, top: width / 25, bottom: width / 25),
                child: Text(
                  "Quantity",
                  style: TextStyle(
                      fontSize: width / 15, fontWeight: FontWeight.bold),
                ),
              )),
          SizedBox(width: width / 4),
          InkWell(
            onTap: () => remove(),
            child: Container(
                width: width / 10,
                height: width / 10,
                decoration: const BoxDecoration(
                    color: Color(0xFFFFD591),
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Align(
                    alignment: Alignment.center,
                    child: Icon(
                      Icons.remove,
                      size: width / 15,
                    ))),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(itemQuantity.toString(),
                style: TextStyle(fontSize: width / 15)),
          ),
          InkWell(
            onTap: () => add(),
            child: Container(
                width: width / 10,
                height: width / 10,
                decoration: const BoxDecoration(
                    color: Color(0xFFFFD591),
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Align(
                    alignment: Alignment.center,
                    child: Icon(
                      Icons.add,
                      size: width / 15,
                    ))),
          )
        ],
      ),
    );
  }
}