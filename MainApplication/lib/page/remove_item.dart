import 'package:BeanBud/classes/cart.dart';
import 'package:BeanBud/classes/cart_item.dart';
import 'package:flutter/material.dart';

const List<Tab> productTabs = <Tab>[
  Tab(
      child: SizedBox(
        width: 400,
        child: Center(
          child: Text(
            'Remove Item',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
      )),
];

int itemQuantity = 1;

//NEED TO CHANGE TO STATEFUL WIDGET TO CHANGE COLOR OF BUTTON
class RemoveItem extends StatefulWidget {
  CartItem item;

  RemoveItem({Key? key,
    required this.item})
      : super(key: key);
  @override
  State<RemoveItem> createState() => RemoveItemState(item: item);
}
class RemoveItemState extends State<RemoveItem> {
  RemoveItemState({required this.item});
  CartItem item;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery
        .of(context)
        .size
        .width;
    return Scaffold(
      //Appbar implementation
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        titleSpacing: 15.0,
        backgroundColor: Colors.white,
        toolbarHeight: width / 6.5,
        centerTitle: false,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40.0)),
        ),
        //BeanBud title
        title: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'BeanBud',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: width / 16,
                fontFamily: 'Segoe-UI'),
          ),
          Text(
            ' Coffee for you',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w900,
                fontSize: width / 40,
                fontFamily: 'Segoe-UI'),
          ),
        ]),
        leading: Padding(
          padding: EdgeInsets.only(left: width / 40),
          child: const Image(
            //AppBar BeanBud Logo
            image: AssetImage('assets/images/icon.png'),
          ),
        ),
      ),
      body: DefaultTabController(
        length: 1,
        child: Builder(builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(
              flexibleSpace: Align(
                alignment: Alignment.bottomCenter,
                child: Material(
                  color: const Color(0xFFFFD591),
                  child: TabBar(
                    isScrollable: true,
                    unselectedLabelColor:
                    const Color(0xFF000000).withOpacity(0.4),
                    tabs: productTabs,
                  ),
                ),
              ),
              backgroundColor: const Color(0xFFFFD591),
            ),
            //Tab Bar
            body: TabBarView(
              children: productTabs.map((Tab tab) {
                return Column(children: [
                  //Item Box
                  Container(
                      width: width,
                      height: width / 1.7,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            spreadRadius: 5.0,
                            blurRadius: 7,
                            offset: Offset(
                                0, 0), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //Item Image
                          ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Image.memory(item.itemImage,
                                fit: BoxFit.fill,
                                height: width / 3,
                                width: width / 3,
                                alignment: Alignment.center),
                          ),
                          //Item Name
                          Text(
                            item.itemName,
                            style: TextStyle(
                                fontSize: width / 15,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      )),
                  //Item Options
                  Expanded(
                      child: ListView(
                          children: [
                            //Adds the item to the cart button
                            Padding(
                              padding: EdgeInsets.only(
                                  bottom: 50.0,
                                  top: 20.0,
                                  left: width / 4,
                                  right: width / 4),
                              child: SizedBox(
                                width: width / 2.5,
                                height: width / 8,
                                child: ElevatedButton(
                                  onPressed: () async {
                                    Cart.instance.removeFromCart(item);
                                    Navigator.pop(context);
                                  },
                                  style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius
                                                .circular(18.0),
                                          ))),
                                  child: Center(
                                    child: Text(
                                      'Remove from Cart',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'AdobeCleanUXBold',
                                          fontSize: width / 20),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ])
                  )
                ]
                );
              }).toList(),
            ),
          );
        }),
      ),
    );
  }
}