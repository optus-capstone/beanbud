import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';

import '../classes/global_variables.dart';

//Update profile allows the currently logged in user to alter their name, password and profile picture

class UpdateProfile extends StatefulWidget {
  @override
  UpdateProfileState createState() => UpdateProfileState();
}

class UpdateProfileState extends State<UpdateProfile> {
  bool isPasswordTextField = true;
  bool isObscuredPassword = true;
  IconData icon = Icons.visibility_off;

  TextEditingController newName = TextEditingController();
  TextEditingController oldPass = TextEditingController();
  TextEditingController newPass = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    //Scaffold used to hold all child widget of the screen
    return Scaffold(
      //Appbar implementation
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        toolbarHeight: width / 5.5,
        backgroundColor: Colors.white,
        //Bar
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30.0)),
        ),
        shadowColor: Colors.grey.withOpacity(0.5),
        title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          //Icon
          Image.asset(
            "assets/images/icon.png",
            fit: BoxFit.contain,
            height: width / 9,
            width: width / 9,
          ),
          //Sub-heading
          Padding(
            padding: EdgeInsets.only(left: width / 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Bean Bud",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: width / 13,
                  ),
                ),
                Text(
                  "Coffee For You",
                  style: TextStyle(
                    fontSize: width / 35,
                  ),
                )
              ],
            ),
          ),
        ]),
        actions: [
          //todo change profile picture functionality (currently displaying logo by default)
          IconButton(
            icon: const Icon(Icons.settings,
            color: Colors.white,
            ),
            onPressed: () {},
          )
        ],
      ),
      body: Container(
        padding: const EdgeInsets.only(left: 15, top: 20, right: 15),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
              Center(
                child: Stack(
                  children: [
                    Container(
                      width: 130,
                      height: 130,
                      decoration: BoxDecoration(
                        border: Border.all(width: 4, color: Colors.white),
                        boxShadow: const [
                          BoxShadow(
                              spreadRadius: 2,
                              blurRadius: 10,
                              color: Colors.blue
                          ),
                        ],
                          shape: BoxShape.circle,
                          image: const DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage('assets/images/logo.png')
                          )
                      ),
                    ),
                    Positioned(
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              width: 4,
                              color: Colors.white
                            ),
                            color: Colors.blue
                          ),
                          child: const Icon(
                            Icons.edit,
                            color: Colors.white,
                          ),
                        )
                    )
                  ]
                ),
              ),
              //Text fields to change user attributes
              const SizedBox(height: 30),
              buildTextField("Your Name", name, false, newName),
              const Text("Update Password"),
              const SizedBox(height: 30),
              buildTextField("Old Password", '' , true, oldPass),
              buildTextField("New Password", '', true, newPass),
              const SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //Cancel button, navigate back
                  OutlinedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      style: OutlinedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(horizontal: 50),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))
                    ),
                      child: const Text("Cancel",
                        style: TextStyle(
                        fontSize: 15,
                        letterSpacing: 2,
                        color: Colors.black
                    )
                    )
                  ),
                  //Confirm button, change features if applicable
                  ElevatedButton(
                      onPressed: () {
                        try {
                          if(newName.text.isNotEmpty) {
                            Amplify.Auth.updateUserAttribute(
                                userAttributeKey: CognitoUserAttributeKey.name,
                                value: newName.value.text);
                            successDialog('name');
                          }
                          if(oldPass.text.isNotEmpty || newPass.text.isNotEmpty) {
                            Amplify.Auth.updatePassword(
                                oldPassword: oldPass.value.text,
                                newPassword: newPass.value.text);
                            successDialog('password');
                          }
                          fetchCurrentUserAttributes();
                        } on AmplifyException catch (e) {
                          failedDialog(e.toString());
                        }
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.blue,
                          padding: const EdgeInsets.symmetric(horizontal: 50),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))
                      ),
                      child: const Text("Save", style: TextStyle(
                      fontSize: 15,
                      letterSpacing: 2,
                      color: Colors.white,
                    )
                    )
                  )
                ],
              )
            ],
          ),
        )
      ),
    );
  }

  //Build the textfield
  Widget buildTextField(String labelText, String placeholder, bool isPasswordTextField, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 30),
      child: TextField(
        obscureText: isPasswordTextField ? isObscuredPassword : false,
        controller: controller,
        decoration: InputDecoration(
          suffixIcon: isPasswordTextField ?
              IconButton(
                icon: Icon(icon, color: Colors.grey),
                onPressed: () {
                  setState(() {
                    isObscuredPassword = !isObscuredPassword;
                    if(isObscuredPassword) {
                      icon = Icons.visibility_off;
                    }
                    else {
                      icon = Icons.visibility;
                    }
                  });
                }
              ): null,
              contentPadding: const EdgeInsets.only(bottom: 5),
        labelText: labelText,
        floatingLabelBehavior: FloatingLabelBehavior.always,
        hintText: placeholder,
          hintStyle: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
          color: Colors.grey,
          ),
        ),
      ),
    );
  }

  //Show dialog on success
  Future successDialog(String content) => showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text("Success!"),
      content: Text("Successfully saved $content change!"),
    ),
  );

  //Show dialog on failure
  Future failedDialog(String e) => showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text("Failed!"),
      content: Text(e),
    ),
  );
}