import 'dart:collection';
import 'dart:typed_data';

import 'package:BeanBud/classes/orders.dart';
import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/classes/menu_hot_drinks.dart';
import 'package:BeanBud/classes/menu_snacks.dart';
import 'package:flutter/material.dart';

import '../classes/menu.dart';
import '../classes/menu_cold_drinks.dart';
import '../classes/menu_desserts.dart';
import '../models/item_model.dart';
import 'product_page.dart';

//Menu page to display the entire cafe menu sorted by the category of the items on the menu

//Menu Tabs
const List<Tab> menuTabs = <Tab>[
  Tab(text: 'Your Favourites'),
  Tab(text: 'Hot Drinks'),
  Tab(text: 'Cold Drinks'),
  Tab(text: 'Snacks'),
  Tab(text: 'Dessert'),
];

class MenuPage extends StatefulWidget {
  const MenuPage({Key? key}) : super(key: key);

  @override
  State<MenuPage> createState() => _MenuState();
}

class _MenuState extends State<MenuPage> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: menuTabs.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    const List<Tab> menuTabs = <Tab>[
      Tab(text: "Popular"),
      Tab(text: "Hot Drinks"),
      Tab(text: "Cold Drinks"),
      Tab(text: "Snacks"),
      Tab(text: "Desserts"),
    ];
    Set<ItemModel> menuList = Set();
    Set<ItemModel> popularList = Set();
    Queue<ItemModel> hotList = Queue();
    Queue<ItemModel> coldList = Queue();
    Queue<ItemModel> snackList = Queue();
    Queue<ItemModel> dessertList = Queue();

    for (int ii = 0; ii < Menu.instance.getLength(); ii++) {
      switch (Menu.instance.getItemCategoryAtIndex(ii)) {
        case "Hot Drink":
          {
            hotList.add(Menu.instance.findItemAtIndex(ii));
          }
          break;
        case "Cold Drink":
          {
            coldList.add(Menu.instance.findItemAtIndex(ii));
          }
          break;
        case "Snack":
          {
            snackList.add(Menu.instance.findItemAtIndex(ii));
          }
          break;
        case "Dessert":
          {
            dessertList.add(Menu.instance.findItemAtIndex(ii));
          }
          break;
        default:
          {
            popularList.add(Menu.instance.findItemAtIndex(ii));
          }
          break;
      }
    }

    for (int i = 0; i < Orders.instance.getOrderedItems().length; i++) {
      popularList.add(
          Menu.instance.findItemAtId(Orders.instance.getOrderedItems()[i]));
    }

    menuList = popularList;
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Align(
          alignment: Alignment.bottomCenter,
          child: Material(
            color: const Color(0xFFFFD591),
            child: TabBar(
              controller: _tabController,
              isScrollable: true,
              unselectedLabelColor: const Color(0xFF000000).withOpacity(0.4),
              tabs: menuTabs,
            ),
          ),
        ),
        backgroundColor: const Color(0xFFFFD591),
      ),
      body: TabBarView(controller: _tabController, children: <Widget>[
        ListView.builder(
          itemCount: popularList.length,
          itemBuilder: (context, index) {
            final item = popularList.elementAt(index);
            return Item(
              id: item.id,
              image: Uint8List.fromList(item.itemImage.toBytes()),
              description: item.itemDescription,
              name: item.itemName,
              price: item.itemPrice,
              category: item.itemCategory,
            );
          },
        ),
        ListView.builder(
          itemCount: MenuHotDrinks.instance.getLength(),
          itemBuilder: (context, index) {
            final item = MenuHotDrinks.instance.findItemAtIndex(index);
            return Item(
              id: item.id,
              image: Uint8List.fromList(item.itemImage.toBytes()),
              description: item.itemDescription,
              name: item.itemName,
              price: item.itemPrice,
              category: item.itemCategory,
            );
          },
        ),
        ListView.builder(
          itemCount: MenuColdDrinks.instance.getLength(),
          itemBuilder: (context, index) {
            final item = MenuColdDrinks.instance.findItemAtIndex(index);
            return Item(
              id: item.id,
              image: Uint8List.fromList(item.itemImage.toBytes()),
              description: item.itemDescription,
              name: item.itemName,
              price: item.itemPrice,
              category: item.itemCategory,
            );
          },
        ),
        ListView.builder(
          itemCount: MenuSnacks.instance.getLength(),
          itemBuilder: (context, index) {
            final item = MenuSnacks.instance.findItemAtIndex(index);
            return Item(
              id: item.id,
              image: Uint8List.fromList(item.itemImage.toBytes()),
              description: item.itemDescription,
              name: item.itemName,
              price: item.itemPrice,
              category: item.itemCategory,
            );
          },
        ),
        ListView.builder(
          itemCount: MenuDesserts.instance.getLength(),
          itemBuilder: (context, index) {
            final item = MenuDesserts.instance.findItemAtIndex(index);
            return Item(
              id: item.id,
              image: Uint8List.fromList(item.itemImage.toBytes()),
              description: item.itemDescription,
              name: item.itemName,
              price: item.itemPrice,
              category: item.itemCategory,
            );
          },
        ),
      ]),
    );
  }
}

class Item extends StatelessWidget {
  const Item({
    super.key,
    required this.id,
    required this.image,
    required this.name,
    required this.price,
    required this.description,
    required this.category,
  });

  final int id;
  final Uint8List image;
  final String name;
  final double price;
  final String description;
  final String category;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductPage(
                  id: id,
                  itemImage: image,
                  itemName: name,
                  itemDescription: description,
                  itemPrice: price,
                  itemCategory: category,
                )),
      ),
      child: Padding(
          padding: EdgeInsets.only(left: width / 15, top: width / 30),
          child: Row(
            children: [
              Padding(
                  padding: const EdgeInsets.all(5),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.memory(image,
                          fit: BoxFit.fill,
                          height: width / 3.5,
                          width: width / 3.5,
                          alignment: Alignment.center))),
              Padding(
                padding: EdgeInsets.only(left: width / 15),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: width / 2,
                        child: Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: Text(
                              name,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: width / 20,
                                  fontWeight: FontWeight.bold),
                            )),
                      ),
                      SizedBox(
                        width: width / 2,
                        child: Padding(
                            padding: const EdgeInsets.only(left: 1),
                            child: Text(
                              description,
                              textAlign: TextAlign.left,
                              style: TextStyle(fontSize: width / 35),
                            )),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(top: 15),
                          child: Text(
                            price.toString(),
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: width / 20),
                          ))
                    ]),
              )
            ],
          )),
    );
  }
}
