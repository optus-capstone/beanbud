import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/models/order_model.dart';
import 'package:BeanBud/mysql.dart';
import 'package:BeanBud/page/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:BeanBud/classes/cart_item.dart';
import 'package:BeanBud/classes/cart.dart';
import 'package:BeanBud/customWidgets/cart_product.dart';
import 'package:BeanBud/customWidgets/cart_setting.dart';

//Author: Darren Ou Yang
//Last Updated" 23/09/2022
//Cart Page

//Header for the cart page
const List<Tab> accountTabs = <Tab>[
  Tab(
      child: SizedBox(
        width: 400,
        child: Center(
          child: Text(
            'Your Cart',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
      )),
];

class CartPage extends StatefulWidget {
  const CartPage({Key? key}) : super(key: key);

  State<CartPage> createState() => CartPageState();
}

class CartPageState extends State<CartPage> {

  @override
  Widget build(BuildContext context) {
    //Width to create a responsive app
    final double width = MediaQuery.of(context).size.width;
    //Converts the CartItems into CartProduct widget to display
    List<CartProduct> cartProductList = List.empty(growable: true);
    List<CartItem> cartItemList = [];
    for (int ii = 0; ii < Cart.instance.getLength(); ii++) {
      CartItem cartItem = Cart.instance.findItemAtIndex(ii);
      cartProductList.add(CartProduct(cartItem: cartItem));
      cartItemList.add(cartItem);
    }

    //Single tab as a header
    return DefaultTabController(
      length: 1,
      child: Builder(builder: (BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            flexibleSpace: Align(
              alignment: Alignment.bottomCenter,
              child: Material(
                color: const Color(0xFFFFD591),
                child: TabBar(
                  isScrollable: true,
                  unselectedLabelColor:
                      const Color(0xFF000000).withOpacity(0.4),
                  tabs: accountTabs,
                ),
              ),
            ),
            backgroundColor: const Color(0xFFFFD591),
          ),
          //Tab Bar
          body: TabBarView(
            children: accountTabs.map((Tab tab) {
              return ListView(
                  children: [
                    Column(children: [
                      //Cart items displayed as widgets(CartProduct)
                      Column(
                        children: cartProductList,
                      ),
                      //Cart settings
                      InkWell(
                        onTap: () {
                          if(Cart.instance.orderType == 'Takeaway') {
                            setState(() {
                              Cart.instance.orderType = "Dine-In";
                            });
                          }
                          else if(Cart.instance.orderType == 'Dine-In') {
                            setState(() {
                              Cart.instance.orderType = "Takeaway";
                            });
                          }
                        },
                        child: Container(
                            decoration: const BoxDecoration(
                              border: Border(bottom: BorderSide(color: Colors.black54, width: 1)),
                            ),
                            height: width / 5.5,
                            child: Row(
                              children: [
                                SizedBox(
                                  width: width / 1.5,
                                  child: Padding(
                                    padding: const EdgeInsets.all(20),
                                    child: Text(Cart.instance.orderType,
                                        style: TextStyle(
                                            fontSize: width / 17, fontWeight: FontWeight.bold)),
                                  ),
                                ),
                                SizedBox(
                                  width: width / 4,
                                ),
                                const Icon(Icons.arrow_forward_ios_rounded)
                              ],
                            )),
                      ),
                      CartSetting(title: "Payment Method"),
                      //Total price of the cart (Needs to update the total price)
                      Container(
                          height: width / 5.5,
                          decoration: const BoxDecoration(
                            border: Border(
                                bottom: BorderSide(color: Colors.black54, width: 1)),
                          ),
                          child: Row(
                            children: [
                              SizedBox(
                                width: width / 1.5,
                                child: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Text("Total",
                                      style: TextStyle(
                                          fontSize: width / 17,
                                          fontWeight: FontWeight.bold)),
                                ),
                              ),
                              Align(
                                  alignment: Alignment.centerRight,
                                  child: Text("\$${Cart.instance.getCartTotal().toStringAsFixed(2)}",
                                      style: TextStyle(
                                          fontSize: width / 17,
                                          fontWeight: FontWeight.bold))
                              ),
                            ],
                          )),
                    ]),
              ]);
            }).toList(),
          ),
          bottomNavigationBar: Padding(
            padding: const EdgeInsets.only(left: 40, right: 40.0, bottom: 25.0),
            child: InkWell(
              onTap: () {
                Mysql().addOrder(
                OrderModel(
                orderType: Cart.instance.orderType, orderDate: DateTime.now(), status: "Pending", items: cartItemList));
                coffeeLoyalty();
                Cart.instance.emptyCart();
                Cart.instance.coffeeCountCart = 0;
                getCoffeeCount();
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => MainScreen()));
                //todo communicate with the database manager web application and add order (if confirmed) to orders table of database
              },
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.cyan[400]),
                width: width / 3.5,
                height: width / 8,
                child: Center(
                  child: Text(
                    'Place Order',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'AdobeCleanUXBold',
                        fontSize: width / 30),
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
