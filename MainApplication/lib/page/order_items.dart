import 'package:BeanBud/classes/cart_item.dart';
import 'package:BeanBud/customWidgets/order_product.dart';
import 'package:flutter/material.dart';

class OrderItems extends StatelessWidget {
  const OrderItems({super.key, required this.items});

  final List<CartItem> items;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;

    //Converts the CartItems into CartProduct widget to display
    List<OrderProduct> cartProductList = List.empty(growable: true);
    for (CartItem item in items) {
      cartProductList.add(OrderProduct(cartItem: item));
    }
    //Scaffold used to hold all child widget of the screen
    return Scaffold(
      //Appbar implementation
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          toolbarHeight: width / 5.5,
          backgroundColor: Colors.white,
          //Bar
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30.0)),
          ),
          shadowColor: Colors.grey.withOpacity(0.5),
          title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            //Icon
            Image.asset(
              "assets/images/icon.png",
              fit: BoxFit.contain,
              height: width / 9,
              width: width / 9,
            ),
            //Sub-heading
            Padding(
              padding: EdgeInsets.only(left: width / 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Bean Bud",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: width / 13,
                    ),
                  ),
                  Text(
                    "Coffee For You",
                    style: TextStyle(
                      fontSize: width / 35,
                    ),
                  )
                ],
              ),
            ),
          ]),
        ),
        body: ListView(
            children: [
              Column(
                  children: [
                    Column(
                      children: cartProductList,
                    ),
                  ]
              )
            ])
    );
  }
}