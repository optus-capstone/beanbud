import 'dart:typed_data';

import 'package:BeanBud/classes/orders.dart';
import 'package:BeanBud/classes/recommended_items.dart';
import 'package:BeanBud/classes/regular_order_instance.dart';
import 'package:BeanBud/models/order_model.dart';
import 'package:BeanBud/mysql.dart';
import 'package:BeanBud/page/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/classes/menu.dart';
import 'package:BeanBud/models/item_model.dart';
import 'package:BeanBud/page/menu_page.dart';
import 'package:BeanBud/weather/weather_controller.dart';
import 'package:BeanBud/weather/weather_status.dart';

import '../classes/cart.dart';
import '../classes/cart_item.dart';
import '../models/weather_model.dart';

//Homepage is the users personalised page to display their regular order, greet them personally using
//their name and time of day as well as offering items recommended to them by various features

class HomePage extends StatefulWidget {
  HomePage({super.key});

  State<HomePage> createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    final controller = Get.put(WeatherController());
    final weatherStatus = Get.put(WeatherStatus());

    List<ItemModel> hotList = [];
    List<ItemModel> coldList = [];
    List<ItemModel> foodList = [];
    //New items a user hasn't ordered previously
    List<ItemModel> newList = [];
    //ML Model list of recommended items
    List<ItemModel> mlList = [];

    for (int ii = 0; ii < Menu.instance.getLength(); ii++) {
      switch (Menu.instance.getItemCategoryAtIndex(ii)) {
        case "Hot Drink":
          {
            hotList.add(Menu.instance.findItemAtIndex(ii));
          }
          break;
        case "Cold Drink":
          {
            coldList.add(Menu.instance.findItemAtIndex(ii));
          }
          break;
        case "Snack":
          {
            foodList.add(Menu.instance.findItemAtIndex(ii));
          }
          break;
        case "Dessert":
          {
            foodList.add(Menu.instance.findItemAtIndex(ii));
          }
          break;
      }
    }

    return FutureBuilder<Weather>(
        future: controller.getWeatherData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          } else if (snapshot.hasData) {
            //Items a user hasn't ordered in the past
            for (int id in Orders.instance.newItems) {
              newList.add(Menu.instance.findItemAtId(id));
            }
            //Get recommended items from ML model
            mlList = Recommended.instance.getItems();
            //Get the customers regular order
            var data = snapshot.data;
            var weatherIcon = weatherStatus.getWeatherIcon(data!.cod);
            //In the case that the weather is cold, display hot drinks to the user
            if (data.main.temp < 17.0) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  //Show weather data at the top of the screen
                  _weather(width, height, data, weatherIcon),
                  Expanded(
                    child: ListView(children: [
                      _welcomeMessage(width, height),
                      _customerRegular(width, height, context),
                      //Alternate left and right
                      _listViewLeft(
                          width, height, 'Or Try Something NEW?', newList),
                      _listViewRight(
                          width, height, 'Recommended for You!', mlList),
                      _listViewLeft(
                          width, height, 'A Drink for a Cold Day!', hotList),
                    ]),
                  ),
                ],
              );
            }
            //In the case that the weather is hot, display cold drinks to the user
            if (data.main.temp > 25.0) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  //Say hello to the logged in user
                  //Text to offer the user their regular order
                  //Allow scrolling to view all different categories of cafe item offers
                  _weather(width, height, data, weatherIcon),
                  Expanded(
                    child: ListView(children: [
                      _welcomeMessage(width, height),
                      _customerRegular(width, height, context),
                      //Alternate left and right
                      _listViewLeft(
                          width, height, 'Or Try Something NEW?', newList),
                      _listViewRight(
                          width, height, 'Recommended for You!', mlList),
                      _listViewLeft(
                          width, height, 'A Drink for a Hot Day!', coldList),
                    ]),
                  ),
                ],
              );
            }
            //In the case that the weather is neither hot or cold display normal home screen
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                //Say hello to the logged in user
                //Text to offer the user their regular order
                //Allow scrolling to view all different categories of cafe item offers
                _weather(width, height, data, weatherIcon),
                Expanded(
                  child: ListView(children: [
                    _welcomeMessage(width, height),
                    _customerRegular(width, height, context),
                    //Alternate left and right
                    _listViewLeft(width, height, 'Try Something NEW?', newList),
                    _listViewRight(
                        width, height, 'Recommended for You!', mlList),
                  ]),
                ),
              ],
            );
          }
          return const Center(
            child: SpinKitDoubleBounce(
              color: Colors.blue,
              size: 50,
            ),
          );
        });
  }

  _weather(double width, double height, var data, String weatherIcon) {
    //Weather information below the appbar
    return Container(
      height: width / 5.5 + width / 5,
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 3.0, color: Colors.black),
        ),
      ),
      child: Align(
        alignment: Alignment.bottomLeft,
        child: Row(children: [
          SizedBox(
            width: 8,
          ),
          //Icon associated with current weather
          Text(
            weatherIcon,
          ),
          const SizedBox(
            width: 10,
            height: 50,
          ),
          //Current temperature and weather type (Cloudy, sunny etc)
          Text(
            '${data.main.temp.toInt().toString()}° C  ${data.weather[0].description}',
            textAlign: TextAlign.right,
            style: TextStyle(fontSize: width / 30, fontFamily: 'Segoe-UI'),
          ),
        ]),
      ),
    );
  }

  //Personalised greeting to user
  _welcomeMessage(double width, double height) {
    return Stack(children: [
      CustomPaint(
        painter: CirclePainter(width),
      ),
      Padding(
        padding: const EdgeInsets.only(bottom: 10.0, right: 10.0, left: 10.0),
        child: Container(
            transform: Matrix4.translationValues(0.0, -(width / 9), 0.0),
            height: height / 10.0,
            width: width,
            child: Column(
              children: [
                _greeting(width),
                Text(
                  'Would you like your regular today?',
                  textAlign: TextAlign.center,
                  style:
                      TextStyle(fontSize: height / 45, fontFamily: 'Segoe-UI'),
                ),
              ],
            )),
      ),
    ]);
  }

  //Show the customers regular order
  _customerRegular(double width, double height, context) {
    //Return empty widget if the customer does not have a regular order
    if (Regular.regularOrder.status == "null") {
      return Container();
    }
    return Stack(children: [
      Align(
        alignment: Alignment.centerRight,
        child: Padding(
          padding: EdgeInsets.only(top: width / 20),
          child: Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  bottomLeft: Radius.circular(15.0)),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  spreadRadius: 5.0,
                  blurRadius: 7,
                  offset: Offset(0, 0), // changes position of shadow
                ),
              ],
            ),
            width: width / 1.2,
            height: width / 1.3,
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(right: width / 25, top: width / 2),
                child: InkWell(
                  onTap: () {
                    //Add items to cart
                    Cart.instance.emptyCart();
                    for (CartItem item in Regular.instance.getItems()) {
                      Cart.instance.addToCart(item);
                    }
                    Mysql().addOrder(OrderModel(
                        orderType: Cart.instance.orderType,
                        orderDate: DateTime.now(),
                        status: "Pending",
                        items: Regular.instance.getItems()));
                    coffeeLoyalty();
                    Cart.instance.emptyCart();
                    Cart.instance.coffeeCountCart = 0;
                    getCoffeeCount();
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => MainScreen()));
                    //todo communicate with the database manager web application and add order (if confirmed) to orders table of database
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.cyan[400]),
                    width: width / 3.5,
                    height: width / 8,
                    child: Center(
                      child: Text(
                        'Place Order',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'AdobeCleanUXBold',
                            fontSize: width / 30),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(15.0),
                      bottomLeft: Radius.circular(15.0)),
                  color: Colors.cyan[400]),
              width: width / 1.1,
              height: width / 8,
              child: Center(
                child: Text(
                  'Your Regular',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'AdobeCleanUXBold',
                      fontWeight: FontWeight.bold,
                      fontSize: width / 18),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: SizedBox(
              height: width / 2.5,
              width: width / 1.25,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: Regular.instance.getItems().length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: SizedBox(
                          child: Card(
                            child: Center(
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: Image.memory(
                                  Uint8List.fromList(Regular.instance
                                      .getAtIndex(index)
                                      .itemImage
                                      .toBytes()),
                                  height: width / 4,
                                  width: width / 4,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Text(
                        Regular.instance.getAtIndex(index).itemName,
                        style: TextStyle(
                            fontSize: width / 30, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 3,),
                      Text(
                        "Quantity - ${Regular.instance.getAtIndex(index).itemQuantity.toString()}",
                        style: TextStyle(
                            fontSize: width / 45, fontWeight: FontWeight.bold),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: width / 5, top: 30, bottom: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Store',
                  style: TextStyle(fontSize: width / 35),
                ),
                SizedBox(width: width / 5),
                //todo Placeholder name for store (change from cafe to actual cafe)
                Text(
                  'Cafe',
                  style: TextStyle(fontSize: width / 35),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: width / 5, bottom: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Order',
                  style: TextStyle(fontSize: width / 35),
                ),
                SizedBox(width: width / 5),
                Text(
                  Regular.regularOrder.orderType,
                  style: TextStyle(fontSize: width / 35),
                )
              ],
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(left: width / 5, bottom: 8, top: width / 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Total',
                  style: TextStyle(
                      fontSize: width / 20, fontWeight: FontWeight.bold),
                ),
                SizedBox(width: width / 6),
                Text(
                  Regular.instance.getRegularTotal().toStringAsFixed(2),
                  style: TextStyle(
                      fontSize: width / 20, fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
        ],
      ),
    ]);
  }

  //Show category on the right side of the screen
  _listViewRight(
      double width, double height, String title, List<ItemModel> items) {
    return Stack(
      children: [
        Align(
          alignment: Alignment.centerRight,
          child: Padding(
            padding: EdgeInsets.only(top: width / 10),
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15.0),
                    bottomLeft: Radius.circular(15.0)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    spreadRadius: 5.0,
                    blurRadius: 7,
                    offset: Offset(0, 0), // changes position of shadow
                  ),
                ],
              ),
              width: width / 1.2,
              height: width / 1.4,
            ),
          ),
        ),
        Column(
          children: [
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(15.0),
                        bottomLeft: Radius.circular(15.0)),
                    color: Colors.cyan[400]),
                width: width / 1.1,
                height: width / 8,
                child: Center(
                  child: Text(
                    title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'AdobeCleanUXBold',
                        fontWeight: FontWeight.bold,
                        fontSize: width / 18),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(top: width / 30),
                child: SizedBox(
                  height: width / 1.5,
                  width: width / 1.25,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: items.length,
                    itemBuilder: (context, index) {
                      final item = items[index];
                      Item displayItem = Item(
                        id: item.id,
                        image: Uint8List.fromList(item.itemImage.toBytes()),
                        description: item.itemDescription,
                        name: item.itemName,
                        price: item.itemPrice,
                        category: item.itemCategory,
                      );
                      return Column(
                        children: [
                          SizedBox(
                            height: width / 3.5,
                            width: width / 3.5,
                            child: Card(
                              child: Center(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10.0),
                                  child: Image.memory(
                                      Uint8List.fromList(displayItem.image),
                                      height: width / 4,
                                      width: width / 4,
                                      fit: BoxFit.fill,
                                      alignment: Alignment.center),
                                ),
                              ),
                            ),
                          ),
                          Text(
                            displayItem.name,
                            style: TextStyle(
                                fontSize: width / 30,
                                fontFamily: 'AdobeCleanUXBold',
                                fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Text(
                            '+ Large',
                            style: TextStyle(fontSize: width / 40),
                          ),
                          Text(
                            '+ Full Cream Milk',
                            style: TextStyle(fontSize: width / 40),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              displayItem.price.toString(),
                              style: TextStyle(
                                  fontSize: width / 25,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Segoe-UI'),
                            ),
                          ),
                          //Make the place order container clickable
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: InkWell(
                              onTap: () {
                                Cart.instance.addToCart(CartItem(
                                    itemId: item.id,
                                    itemDescription: item.itemDescription,
                                    category: item.itemCategory,
                                    itemImage: item.itemImage,
                                    itemQuantity: 1,
                                    itemName: item.itemName,
                                    itemPrice: item.itemPrice + 1.00,
                                    attributes: ['Large', 'Full Cream Milk']));
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.cyan[400]),
                                width: width / 5,
                                height: width / 13,
                                child: Center(
                                  child: Text(
                                    'Add',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'Segoe-UI',
                                        fontWeight: FontWeight.bold,
                                        fontSize: width / 30),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  //Show categories on the left side of the screen
  _listViewLeft(double width, double height, String title, List items) {
    return Padding(
      padding: EdgeInsets.only(top: 20, bottom: width / 20),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(top: width / 10),
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(15.0),
                      bottomRight: Radius.circular(15.0)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      spreadRadius: 5.0,
                      blurRadius: 7,
                      offset: Offset(0, 0), // changes position of shadow
                    ),
                  ],
                ),
                width: width / 1.2,
                height: width / 1.3,
              ),
            ),
          ),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: width / 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(15.0),
                            bottomRight: Radius.circular(15.0)),
                        color: Colors.cyan[400]),
                    width: width / 1.1,
                    height: width / 8,
                    child: Center(
                      child: Text(
                        title,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'AdobeCleanUXBold',
                            fontWeight: FontWeight.bold,
                            fontSize: width / 18),
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(top: width / 30),
                  child: SizedBox(
                    height: width / 1.5,
                    width: width / 1.25,
                    child: ListView.builder(
                      reverse: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: items.length,
                      itemBuilder: (context, index) {
                        final item = items[index];
                        Item displayItem = Item(
                          id: item.id,
                          image: Uint8List.fromList(item.itemImage.toBytes()),
                          description: item.itemDescription,
                          name: item.itemName,
                          price: item.itemPrice,
                          category: item.itemCategory,
                        );
                        return Column(
                          children: [
                            SizedBox(
                              height: width / 3.5,
                              width: width / 3.5,
                              child: GestureDetector(
                                //SWITCH SCREEN
                                /*onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const ProductPage()),
                                ),*/
                                child: Card(
                                  child: Center(
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10.0),
                                      child: Image.memory(
                                          Uint8List.fromList(displayItem.image),
                                          height: width / 4,
                                          width: width / 4,
                                          fit: BoxFit.fill,
                                          alignment: Alignment.center),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Text(
                              displayItem.name,
                              style: TextStyle(
                                  fontSize: width / 30,
                                  fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Text(
                              '+ Large',
                              style: TextStyle(fontSize: width / 40),
                            ),
                            Text(
                              '+ Full Cream Milk',
                              style: TextStyle(fontSize: width / 40),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                displayItem.price.toString(),
                                style: TextStyle(
                                    fontSize: width / 25,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Segoe-UI'),
                              ),
                            ),
                            //Make the place order container clickable
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: InkWell(
                                onTap: () {
                                  Cart.instance.addToCart(CartItem(
                                      itemId: item.id,
                                      itemDescription: displayItem.description,
                                      category: displayItem.category,
                                      itemImage: displayItem.image,
                                      itemQuantity: 1,
                                      itemName: displayItem.name,
                                      itemPrice: displayItem.price,
                                      attributes: ['Large, Full Cream Milk']));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.cyan[400]),
                                  width: width / 5,
                                  height: width / 13,
                                  child: Center(
                                    child: Text(
                                      'Add',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'Segoe-UI',
                                          fontWeight: FontWeight.bold,
                                          fontSize: width / 30),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  _greeting(double width) {
    if (_getHour() < 12) {
      return Text('Good Morning $name!',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: width / 12.9,
              fontWeight: FontWeight.bold,
              fontFamily: 'Segoe-UI'));
    }
    if (_getHour() < 17) {
      return Text('Good Afternoon $name!',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: width / 12.9,
              fontWeight: FontWeight.bold,
              fontFamily: 'Segoe-UI'));
    }
    if (_getHour() < 17) {
      return Text('Good Afternoon $name!',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: width / 12.9,
              fontWeight: FontWeight.bold,
              fontFamily: 'Segoe-UI'));
    }
    return Text('Good Evening $name!',
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: width / 12.9,
            fontWeight: FontWeight.bold,
            fontFamily: 'Segoe-UI'));
  }
}

_getHour() {
  return DateTime.now().hour;
}

_getColour() {
  if (_getHour() < 17) {
    return Color(0xFFFFD591);
  } else {
    return Colors.deepPurpleAccent;
  }
}

//Paint a circle for screen aesthetic
class CirclePainter extends CustomPainter {
  double screenWidth = 0.0;

  CirclePainter(double width) {
    screenWidth = width;
  }

  @override
  void paint(Canvas canvas, Size size) {
    //shadow
    double radius = (screenWidth + 30) / 2.0;
    Offset center = Offset(screenWidth / 2.0, -100);
    // draw circle
    Paint thumbPaint = Paint()
      ..color = _getColour()
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}
