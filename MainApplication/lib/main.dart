//Author: Darren Ou Yang
//ID: 19471527
//Last Updated: 12/08/2022

import 'dart:async';

import 'package:BeanBud/logIn/entry.dart';
import 'package:BeanBud/logIn/splash_screen.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/logIn/configure_amplify.dart';
import 'package:BeanBud/logIn/confirm_reset.dart';
import 'package:BeanBud/mysql.dart';
import 'package:BeanBud/SignUp/facial_recognition_intro_page.dart';
import 'package:BeanBud/pushNotification/notification_service.dart';
import 'package:BeanBud/services.dart';

import 'kiosk/start_kiosk.dart';
import 'logIn/confirm.dart';

enum MenuItem {
  kioskMobileMode,
}

//Main contains the method that runs the application

Future<void> main() async {
  //Ensure initialization of features
  WidgetsFlutterBinding.ensureInitialized();
  //Configure Amplify for authentication
  await configureAmplify();

  //Required for facial setup and push notification features
  setupServices();

  final NotificationService _notifService = locator<NotificationService>();

  //Setup notifications
  _notifService.initialize();

  //Load cafe menu items locally
  Mysql().getMenuItems();

  //Run the Application
  runApp(const Beanbud());
}

//Main Application
class Beanbud extends StatelessWidget {
  const Beanbud({super.key});

  @override
  Widget build(BuildContext context) {
    Amplify.Auth.signOut();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: const Color(0x0000c6dc),
        primarySwatch: Colors.cyan,
        tabBarTheme: const TabBarTheme(
            indicator: UnderlineTabIndicator(
                borderSide: BorderSide(color: Colors.grey))),
      ),
      onGenerateRoute: (settings) {
        //On Signup take to confirm email page
        if (settings.name == '/confirm') {
          return PageRouteBuilder(
            pageBuilder: (_, __, ___) =>
                ConfirmScreen(data: settings.arguments as SignupData),
            transitionsBuilder: (_, __, ___, child) => child,
          );
        }
        //On password reset take to password reset page
        if (settings.name == '/confirm-reset') {
          return PageRouteBuilder(
            pageBuilder: (_, __, ___) =>
                ConfirmResetScreen(data: settings.arguments as LoginData),
            transitionsBuilder: (_, __, ___, child) => child,
          );
        }
        //On sign-in get taken to the main screen of the application
        if (settings.name == '/dashboard') {
          fetchCurrentUserAttributes();
          return PageRouteBuilder(
            pageBuilder: (_, __, ___) => SplashScreen(),
            transitionsBuilder: (_, __, ___, child) => child,
          );
        }
        //On After email verification finish setup with facial recognition and item ratings
        if (settings.name == '/finish-setup') {
          fetchCurrentUserAttributes();
          return PageRouteBuilder(
            pageBuilder: (_, __, ___) => const FacialRecognitionPage(),
            transitionsBuilder: (_, __, ___, child) => child,
          );
        }
        return MaterialPageRoute(builder: (_) => const EntryScreen());
      },
    );
  }
}

//Signup, password reset and Sign-in screen
class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var _data;
  bool _isSignedIn = false;

  //Login functionality
  Future<String?> _onLogin(LoginData data) async {
    try {
      final res = await Amplify.Auth.signIn(
        username: data.name.trim(),
        password: data.password.trim(),
      );

      _isSignedIn = res.isSignedIn;
      return '';
    } on AuthException catch (e) {
      if (e.message.contains('already a user which is signed in')) {
        await Amplify.Auth.signOut();
        return 'Problem logging in. Please try again.';
      }

      return '${e.message} - ${e.recoverySuggestion}';
    }
  }

  //Signup functionality
  Future<String?> _onSignup(SignupData data) async {
    //invoke signup method
    var data1;
    data1 = data;
    try {
      final userAttributes = <CognitoUserAttributeKey, String>{
        CognitoUserAttributeKey.email: data1.name,
        CognitoUserAttributeKey.name:
            data.additionalSignupData!.values.toList().first
      };

      await Amplify.Auth.signUp(
        username: data1.name.trim(),
        password: data1.password.trim(),
        options: CognitoSignUpOptions(userAttributes: userAttributes),
      );
      _data = data;
      return null;
    } on AuthException catch (e) {
      return '${e.message} - ${e.recoverySuggestion}';
    }
  }

  //Recover password functionality
  Future<String?> _onRecoverPassword(BuildContext context, String email) async {
    try {
      final res = await Amplify.Auth.resetPassword(username: email);

      if (res.nextStep.updateStep == 'CONFIRM_RESET_PASSWORD_WITH_CODE') {
        Navigator.of(context).pushReplacementNamed(
          '/confirm-reset',
          arguments: LoginData(name: email, password: ''),
        );
      }
      return null; //sent code
    } on AuthException catch (e) {
      return '${e.message} - ${e.recoverySuggestion}';
    }
  }

  //Build method
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: width / 3,
          backgroundColor: Colors.white,
          //Bar
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40.0)),
          ),
          shadowColor: Colors.grey.withOpacity(0.5),
          title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            //Icon
            Padding(
              padding: const EdgeInsets.only(top: 45),
              child: Image.asset(
                "assets/images/icon.png",
                fit: BoxFit.contain,
                height: width / 5.5,
                width: width / 5.5,
              ),
            ),
            //Sub-heading
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: width / 10, left: 10),
                  child: Text(
                    "Bean Bud",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: width / 10,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: width / 7),
                  child: Text(
                    "Coffee For You",
                    style: TextStyle(
                      fontSize: width / 25,
                    ),
                  ),
                )
              ],
            ),
          ]),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 15.0),
              child: PopupMenuButton<MenuItem>(
                  iconSize: width / 15,
                  icon: const Icon(Icons.menu_sharp, color: Colors.black),
                  onSelected: (value) {
                    if (value == MenuItem.kioskMobileMode) {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const StartKiosk()),
                      );
                    }
                  },
                  padding: EdgeInsets.all(width / 40),
                  itemBuilder: (context) => [
                        PopupMenuItem(
                          value: MenuItem.kioskMobileMode,
                          child: Text('Kiosk Mode',
                              style: TextStyle(fontSize: width / 35)),
                        ),
                      ]),
            ),
          ],
        ),
        body: Stack(children: [
          Padding(
            padding: EdgeInsets.only(bottom: height / 10),
            child: Center(
                child: CustomPaint(
              painter: CirclePainter(width),
            )),
          ),
          FlutterLogin(
            onLogin: _onLogin,
            onRecoverPassword: (String email) =>
                _onRecoverPassword(context, email),
            onSignup: _onSignup,
            theme: LoginTheme(
              switchAuthTextColor: Colors.black, //switch button colour
              buttonTheme: const LoginButtonTheme(
                  backgroundColor: Colors
                      .cyan), //primaryColor: Theme.of(context).primaryColor,
            ),
            additionalSignupFields: const [
              UserFormField(keyName: 'Name', displayName: 'Your name')
            ],
            onSubmitAnimationCompleted: () {
              Navigator.of(context).pushReplacementNamed(
                _isSignedIn ? '/dashboard' : '/confirm',
                arguments: _data,
              );
            },
          )
        ]));
  }
}

//Draws Circle for decoration
class CirclePainter extends CustomPainter {
  double screenWidth = 0;
  CirclePainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 2 + 40;
    Offset center = const Offset(0, 0);
    Path oval = Path();
    oval.addOval(Rect.fromCircle(center: center, radius: radius));

    Paint shadowPaint = Paint()
      ..color = Colors.black.withOpacity(0.25)
      ..maskFilter = const MaskFilter.blur(BlurStyle.normal, 50);
    canvas.drawPath(oval, shadowPaint);
    // draw circle
    Paint thumbPaint = Paint()
      ..color = const Color(0xFFFFD591)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

//Draws Lines for decoration
class LinePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = const Color(0xFF000000);
    paint.style = PaintingStyle.fill;
    canvas.drawLine(Offset.zero, Offset.infinite, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    throw UnimplementedError();
  }
}
