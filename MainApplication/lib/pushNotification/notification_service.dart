//Notification service initializes and attaches to a chosen geofence location (Longitude and Latitude)
//By default the geofence location is the Mallokup cafe

import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:BeanBud/classes/global_variables.dart';

class NotificationService {
  late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  Future initialize() async {
    //Local Notifications initialization
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');
    const DarwinInitializationSettings initializationSettingsDarwin =
        DarwinInitializationSettings();
    const LinuxInitializationSettings initializationSettingsLinux =
        LinuxInitializationSettings(defaultActionName: 'Open notification');
    const InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsDarwin,
            macOS: initializationSettingsDarwin,
            linux: initializationSettingsLinux);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
    //Geofencing initialization (Mallokup cafe as default)
    bg.BackgroundGeolocation.addGeofence(bg.Geofence(
      identifier: 'Cafe',
      radius: 150,
      //todo Change geolocation longitude and latitude to another location!
      latitude: cafeLatitude,
      longitude: cafeLongitude,
      notifyOnEntry: true,
      notifyOnExit: false,
      notifyOnDwell: false,
      loiteringDelay: 3000,
    )).then((bool success) {
      print('[addGeofence] success with $cafeLatitude and $cafeLongitude');
    }).catchError((error) {
      print('[addGeofence] FAILURE: $error');
    });

    bg.BackgroundGeolocation.ready(bg.Config(
            desiredAccuracy: bg.Config.DESIRED_ACCURACY_HIGH,
            distanceFilter: 10.0,
            stopOnTerminate: false,
            startOnBoot: true,
            debug: true,
            logLevel: bg.Config.LOG_LEVEL_VERBOSE))
        .then((bg.State state) {
      if (!state.enabled) {
        ////
        // 3.  Start the plugin.
        //
        bg.BackgroundGeolocation.startGeofences();
      }
    });
    bg.BackgroundGeolocation.onGeofence(_onGeofence);
  }

  //On location reached do this
  Future<void> _onGeofence(bg.GeofenceEvent event) async {
    print('onGeofence $event');
    const AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails('id_1', 'Action 1');
    var platformChannelSpecifics =
        const NotificationDetails(android: androidNotificationDetails);
    await flutterLocalNotificationsPlugin
        .show(
            0,
            'Welcome to the cafe!',
            'Would you like to order your regular today?',
            platformChannelSpecifics)
        .then((result) {})
        .catchError((onError) {
      print('[flutterLocalNotificationsPlugin.show] ERROR: $onError');
    });
  }
}
