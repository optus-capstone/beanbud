//Page to ask the user if they would like to enable facial recognition or skip the process

import 'package:flutter/material.dart';
import 'package:BeanBud/SignUp/user_preferences.dart';
import 'package:BeanBud/facialRecognition/camera_page.dart';
import 'package:BeanBud/facialRecognition/camera_service.dart';
import 'package:BeanBud/facialRecognition/face_detector_service.dart';
import 'package:BeanBud/facialRecognition/ml_service.dart';
import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/mysql.dart';
import 'package:BeanBud/services.dart';

class FacialRecognitionPage extends StatefulWidget {
  const FacialRecognitionPage({Key? key}) : super(key: key);

  @override
  State<FacialRecognitionPage> createState() => _FacialRecognitionPageState();
}

class _FacialRecognitionPageState extends State<FacialRecognitionPage> {
  final MLService _mlService = locator<MLService>();
  final FaceDetectorService _mlKitService = locator<FaceDetectorService>();
  final CameraService _cameraService = locator<CameraService>();
  bool loading = false;

  @override
  void initState() {
    super.initState();
    Mysql().addUser();
    _initializeServices();
  }

  _initializeServices() async {
    setState(() => loading = true);
    await _cameraService.initialize();
    await _mlService.initialize();
    _mlKitService.initialize();
    setState(() => loading = false);
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    return Scaffold(
      //Main top headbar
      appBar: AppBar(
        toolbarHeight: width / 3,
        backgroundColor: Colors.white,
        //Bar
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50.0)),
        ),
        shadowColor: Colors.grey.withOpacity(0.5),
        title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          //Icon
          Padding(
            padding: const EdgeInsets.only(top: 45),
            child: Image.asset(
              "assets/images/icon.png",
              fit: BoxFit.contain,
              height: width / 5,
              width: width / 5,
            ),
          ),
          //Sub-heading
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(top: width / 10, left: 10),
                child: Text(
                  "Bean Bud",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: width / 10,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: width / 7),
                child: Text(
                  "Coffee For You",
                  style: TextStyle(
                    fontSize: width / 25,
                  ),
                ),
              )
            ],
          ),
        ]),
      ),
      //Login Widget
      body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return _buildLoginContainer(context, width, height);
      }),
    );
  }
}

Widget _buildLoginContainer(context, width, height) {
  return Stack(
    children: [
      //Background circle
      Padding(
        padding: EdgeInsets.only(bottom: height / 10),
        child: Center(child: CustomPaint(painter: CirclePainter(width))),
      ),
      Padding(
        padding: EdgeInsets.only(bottom: height / 10),
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: width / 4,
              ),
              Text(
                "Hey $name!",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: width / 10, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: width / 8,
              ),
              SizedBox(
                width: width / 1.2,
                child: Text(
                  "Thanks for signing up! We use facial recognition for our in-store kiosks to improve your overall in-store experience!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: width / 20,
                  ),
                ),
              ),
              SizedBox(
                height: width / 20,
              ),
              SizedBox(
                width: width / 1.3,
                child: Text(
                  "Would you like to set-up your facial recognition now? We promise your picture will never be saved anywhere!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: width / 20,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      //Log In Button
      Padding(
        padding: EdgeInsets.only(top: height / 4),
        child: Center(
          child: SizedBox(
            width: width / 2,
            height: width / 10,
            //Proceed to capture picture upon accepting
            child: ElevatedButton(
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => const TakePictureScreen())
                    );
              },
              child: Text(
                'Sure!',
                style: TextStyle(
                    fontSize: width / 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
        ),
      ),
      //Skips Facial setup button
      Padding(
        padding: EdgeInsets.only(top: height / 2.5),
        child: Center(
          child: SizedBox(
            child: InkWell(
              //If facial capture is skipped, move to ratings page
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => UserPreferencesPage()),
                );
              },
              child: Text(
                'Skip',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: width / 30),
              ),
            ),
          ),
        ),
      ),
      //Bottom bar
      Container(
        margin: EdgeInsets.only(top: height / 1.4),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius:
                const BorderRadius.only(topLeft: Radius.circular(50.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 10,
                blurRadius: 7,
                offset: const Offset(0, 3),
              ),
            ]),
      ),
    ],
  );
}

//Draws Circle
class CirclePainter extends CustomPainter {
  double screenWidth = 0;
  CirclePainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 2 + screenWidth / 5;
    Offset center = const Offset(0, 0);
    Path oval = Path();
    oval.addOval(Rect.fromCircle(center: center, radius: radius));

    Paint shadowPaint = Paint()
      ..color = Colors.black.withOpacity(0.25)
      ..maskFilter = const MaskFilter.blur(BlurStyle.normal, 50);
    canvas.drawPath(oval, shadowPaint);
    // draw circle
    Paint thumbPaint = Paint()
      ..color = const Color(0xFFFFD591)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
