//Ratings page where the user is asked to give their preferences of food and beverage

import 'package:flutter/material.dart';
import 'package:BeanBud/SignUp/additional_preferences.dart';

import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/classes/menu.dart';
import 'package:BeanBud/mysql.dart';

class UserPreferencesPage extends StatelessWidget {
  double hotDrinkRating = 0.5;
  double coldDrinkRating = 0.5;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    int menuLength = Menu.instance.getLength();

    return Scaffold(
      //Main top headbar
      appBar: AppBar(
        toolbarHeight: width / 3,
        backgroundColor: Colors.white,
        //Bar
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50.0)),
        ),
        shadowColor: Colors.grey.withOpacity(0.5),
        title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          //Icon
          Padding(
            padding: const EdgeInsets.only(top: 45),
            child: Image.asset(
              "assets/images/icon.png",
              fit: BoxFit.contain,
              height: width / 5,
              width: width / 5,
            ),
          ),
          //Sub-heading
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(top: width / 10, left: 10),
                child: Text(
                  "Bean Bud",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: width / 10,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: width / 7),
                child: Text(
                  "Coffee For You",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: width / 25,
                  ),
                ),
              )
            ],
          ),
        ]),
      ),
      body: Stack(
        children: [
          //Background circle
          Padding(
            padding: EdgeInsets.only(bottom: height / 10),
            child: Center(child: CustomPaint(painter: CirclePainter(width))),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: height / 10),
            child: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: width / 4,
                  ),
                  Text(
                    "Hey $name! In order for us to recommend some great food and drinks to you we need to learn a bit more about what you like!",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: width / 22, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: width / 8,
                  ),
                  SizedBox(
                    width: width / 1.3,
                    child: Text(
                      "Do you like hot drinks?",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: width / 25,
                      ),
                    ),
                  ),
                  SizedBox(
                    child: RatingBar.builder(
                      initialRating: 0.5,
                      minRating: 0.5,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => const Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        hotDrinkRating = rating;
                      },
                    ),
                  ),
                  SizedBox(
                    height: width / 20,
                  ),
                  SizedBox(
                    height: width / 8,
                  ),
                  SizedBox(
                    width: width / 1.3,
                    child: Text(
                      "Do you like cold drinks?",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: width / 25,
                      ),
                    ),
                  ),
                  SizedBox(
                    child: RatingBar.builder(
                      initialRating: 0.5,
                      minRating: 0.5,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => const Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                         coldDrinkRating = rating;
                      },
                    ),
                  ),

                ],
              ),
            ),
          ),
          //Log In Button
          Padding(
            padding: EdgeInsets.only(top: height / 4),
            child: Center(
              child: SizedBox(
                width: width / 2,
                height: width / 10,
                child: ElevatedButton(
                  onPressed: () {
                    for(var i = 0; i < menuLength; i++) {
                      if(Menu.instance.findItemAtIndex(i).itemCategory == 'Hot Drink') {
                        Mysql().addRating(hotDrinkRating, Menu.instance.findItemAtIndex(i).id);
                      }
                      if(Menu.instance.findItemAtIndex(i).itemCategory == 'Cold Drink') {
                        Mysql().addRating(coldDrinkRating, Menu.instance.findItemAtIndex(i).id);
                      }
                    }
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => AdditionalPreferencesPage())
                    );
                  },
                  child: Text(
                    'Next',
                    style: TextStyle(
                        fontSize: width / 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
              ),
            ),
          ),
          //Bottom bar
          Container(
            margin: EdgeInsets.only(top: height / 1.4),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius:
                const BorderRadius.only(topLeft: Radius.circular(50.0)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 10,
                    blurRadius: 7,
                    offset: const Offset(0, 3),
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}

//Draws Circle
class CirclePainter extends CustomPainter {
  double screenWidth = 0;
  CirclePainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 2 + screenWidth / 5;
    Offset center = const Offset(0, 0);
    Path oval = Path();
    oval.addOval(Rect.fromCircle(center: center, radius: radius));

    Paint shadowPaint = Paint()
      ..color = Colors.black.withOpacity(0.25)
      ..maskFilter = const MaskFilter.blur(BlurStyle.normal, 50);
    canvas.drawPath(oval, shadowPaint);
    // draw circle
    Paint thumbPaint = Paint()
      ..color = const Color(0xFFFFD591)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}