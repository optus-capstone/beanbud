import 'package:BeanBud/page/confirm_regular_order.dart';
import 'package:flutter/material.dart';
import 'package:BeanBud/page/past_orders.dart';
import 'package:BeanBud/page/update_profile.dart';

import '../facialRecognition/camera_page_standalone.dart';

//Author: Darren Ou Yang
//Last Updated" 23/09/2022
//Widget Structure of an Account setting
//Need to be changed to import a onclick to switch to a new page

class AccountSetting extends StatefulWidget {
  const AccountSetting({
    super.key,
    required this.title,
  });
  final String title;

  @override
  State<AccountSetting> createState() => new AccountSettingState(title: title);
}
class AccountSettingState extends State<AccountSetting> {
  AccountSettingState({
    required this.title,
  });
  final String title;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        if(title == 'Update Profile') {
          final value = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UpdateProfile())
          );
          setState(() {
          });
        }
        if(title == 'Your Regular Order') {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ConfirmRegularOrder())
          );
        }
        if(title == 'Your Facial Recognition') {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const TakePictureScreen())
          );
        }
        if(title == 'Default Payment Methods') {
          //todo Default payments left un-implemented for handover as payment is mocked

        }
        if(title == 'Past Orders') {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PastOrders())
          );
        }
      },
      child: Container(
        decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black54, width: 1)),
        ),
        height: 50,
        child: Padding(
          padding: const EdgeInsets.only(left: 15),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              title,
              style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w900,
                  fontFamily: 'Segoe-UI'),
            ),
          ),
        ),
      ),
    );
  }
}
