import 'package:flutter/material.dart';

//Author: Darren Ou Yang
//Last Updated" 23/09/2022
//Cart Option that alters options within the cart (e.g Payment Methods, Takeaway/Pickup)
//todo: Needs to import an onclick screen

class CartSetting extends StatelessWidget {
  CartSetting({
    super.key,
    required this.title,
  });

  String title;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: () {
      //todo Default payments left un-implemented for handover as payment is mocked
      if(title == 'Payment Method') {}
      },
      child: Container(
          decoration: const BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.black54, width: 1)),
          ),
          height: width / 5.5,
          child: Row(
            children: [
              SizedBox(
                width: width / 1.5,
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Text(title,
                      style: TextStyle(
                          fontSize: width / 17, fontWeight: FontWeight.bold)),
                ),
              ),
              SizedBox(
                width: width / 4,
              ),
              const Icon(Icons.arrow_forward_ios_rounded)
            ],
          )),
    );
  }
}
