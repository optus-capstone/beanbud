import 'package:BeanBud/page/remove_item.dart';
import 'package:flutter/material.dart';
import 'package:BeanBud/classes/cart_item.dart';

//Author: Darren Ou Yang
//Last Updated" 23/09/2022
//Widget Structure of a Item that is in a cart

class CartProduct extends StatefulWidget {
  const CartProduct({super.key, required this.cartItem});

  final CartItem cartItem;

  State<CartProduct> createState() => CartProductState(cartItem: cartItem);
}

class CartProductState extends State<CartProduct> {
  CartProductState({required this.cartItem});

  CartItem cartItem;
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;

    return InkWell(
      onTap: () async {
        final value = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => RemoveItem(
              item: cartItem,
            )));
        setState(() {
        });
      },
      child: Container(
          decoration: const BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.black54, width: 1)),
          ),
          height: width / 3,
          child: Row(
            children: [
              Padding(
                padding: EdgeInsets.all(width / 20),
                child: SizedBox(
                  height: width / 5,
                  width: width / 5,
                  child: Card(
                    child: Center(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: Image.memory(cartItem.itemImage,
                            fit: BoxFit.fill,
                            height: width / 4,
                            width: width / 4,
                            alignment: Alignment.center),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: width / 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      cartItem.itemName,
                      style: TextStyle(
                          fontSize: width / 20, fontWeight: FontWeight.bold),
                    ),
                    for(String att in cartItem.attributes)
                      Text(att, style: TextStyle(fontSize: width / 35),
                      ),
                    Text(
                      "- Quantity: ${cartItem.itemQuantity}",
                      style: TextStyle(fontSize: width / 35),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: width / 5,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    "+ Edit",
                    style: TextStyle(fontSize: width / 25),
                  ),
                  Text(
                    (cartItem.itemPrice).toStringAsFixed(2),
                    style: TextStyle(fontSize: width / 25),
                  )
                ],
              )
            ],
          )),
    );
  }
}
