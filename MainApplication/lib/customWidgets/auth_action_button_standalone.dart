//The facialRecognition directory has been implemented with reference to code from the below reference:

//Carlomagno,M (2020) FaceRecognitionAuth [Source Code]. https://github.com/MCarlomagno/FaceRecognitionAuth

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:BeanBud/customWidgets/app_button.dart';
import 'package:BeanBud/facialRecognition/ml_service.dart';
import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/services.dart';
import 'package:BeanBud/facialRecognition/user_model.dart';
import 'package:BeanBud/mysql.dart';
import 'package:BeanBud/page/home_page.dart';

class AuthActionButton extends StatefulWidget {
  AuthActionButton(
      {Key? key,
        required this.onPressed,
        required this.isLogin,
        required this.reload});
  final Function onPressed;
  final bool isLogin;
  final Function reload;
  @override
  _AuthActionButtonState createState() => _AuthActionButtonState();
}

class _AuthActionButtonState extends State<AuthActionButton> {
  final MLService _mlService = locator<MLService>();

  UserModel? predictedUser;

  Future _signUp(context) async {
    List predictedData = _mlService.predictedData;
    UserModel userToSave = UserModel(
      id: id,
      firstName: name,
      faceData: predictedData,
    );
    _insertFaceData(userToSave);
    this._mlService.setPredictedData([]);
    Navigator.pop(context);
  }

  Future _signIn(context) async {
    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => HomePage()));
  }

  Future<UserModel?> _predictUser() async {
    UserModel? userAndPass = await _mlService.predict();
    return userAndPass;
  }

  Future onTap() async {
    try {
      bool faceDetected = await widget.onPressed();
      if (faceDetected) {
        if (widget.isLogin) {
          var user = await _predictUser();
          if (user != null) {
            this.predictedUser = user;
          }
        }
        PersistentBottomSheetController bottomSheetController =
        Scaffold.of(context)
            .showBottomSheet((context) => signSheet(context));
        bottomSheetController.closed.whenComplete(() => widget.reload());
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.blue[200],
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.blue.withOpacity(0.1),
              blurRadius: 1,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 16),
        width: MediaQuery.of(context).size.width * 0.8,
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text(
              'CAPTURE',
              style: TextStyle(color: Colors.white),
            ),
            SizedBox(
              width: 10,
            ),
            Icon(Icons.camera_alt, color: Colors.white)
          ],
        ),
      ),
    );
  }

  signSheet(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          widget.isLogin && predictedUser != null
              ? Container(
            child: Text(
              'Welcome back, ' + predictedUser!.firstName + '.',
              style: const TextStyle(fontSize: 20),
            ),
          )
              : widget.isLogin
              ? Container(
              child: const Text(
                'User not found 😞',
                style: TextStyle(fontSize: 20),
              ))
              : Container(),
          Container(
            child: Column(
              children: [
                const SizedBox(height: 10),
                const Divider(),
                const SizedBox(height: 10),
                widget.isLogin && predictedUser != null
                    ? AppButton(
                  text: 'LOGIN',
                  onPressed: () async {
                    _signIn(context);
                  },
                  icon: const Icon(
                    Icons.login,
                    color: Colors.white,
                  ),
                )
                    : !widget.isLogin
                    ? AppButton(
                  text: 'CONFIRM',
                  onPressed: () async {
                    await _signUp(context);
                  },
                  icon: const Icon(
                    Icons.person_add,
                    color: Colors.white,
                  ),
                )
                    : Container(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _insertFaceData(UserModel user) async {
    var db = Mysql();

    String jsonFaceData = jsonEncode(user.faceData);

    var conn = await db.getConnection();

    var results = await conn.query(
        'update Customer set face_data=? where customer_id=?',
        [jsonFaceData, user.id]);

    conn.close();
  }
}