//The facialRecognition directory has been implemented with reference to code from the below reference:

//Carlomagno,M (2020) FaceRecognitionAuth [Source Code]. https://github.com/MCarlomagno/FaceRecognitionAuth

import 'package:flutter/cupertino.dart';
import 'package:BeanBud/facialRecognition/camera_service.dart';
import 'package:BeanBud/facialRecognition/face_detector_service.dart';
import 'package:BeanBud/services.dart';
import 'package:flutter/material.dart';

import '../facialRecognition/face_painter.dart';

class CameraDetectionPreview extends StatelessWidget {
  CameraDetectionPreview({Key? key}) : super(key: key);

  final CameraService _cameraService = locator<CameraService>();
  final FaceDetectorService _faceDetectorService =
  locator<FaceDetectorService>();

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Transform.scale(
      scale: 1.0,
      child: AspectRatio(
        aspectRatio: MediaQuery.of(context).size.aspectRatio,
        child: OverflowBox(
          alignment: Alignment.center,
          child: FittedBox(
            fit: BoxFit.fitHeight,
            child: Container(
              width: width,
              height:
              width * _cameraService.cameraController!.value.aspectRatio,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  //CameraPreview(_cameraService.cameraController!),
                  _blankWidget(width, height),
                  if (_faceDetectorService.faceDetected)
                    CustomPaint(
                      painter: FacePainter(
                        face: _faceDetectorService.faces[0],
                        imageSize: _cameraService.getImageSize(),
                      ),
                    )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _blankWidget(width, height) {
    return Stack(
        children: [
          //Background circle
          Padding(
            padding: EdgeInsets.only(bottom: height / 10),
            child: Center(child: CustomPaint(painter: CirclePainter(width))),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: height / 10),
            child: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: width / 2,
                  ),
                  Text(
                    "Sign in using your Face-Data!",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: width / 10, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: width / 8,
                  ),
                  SizedBox(
                    width: width / 1.2,
                    child: Text(
                      "Your face is never stored, it exists as a list of numbers!",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: width / 20,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: width / 20,
                  ),
                  SizedBox(
                    width: width / 1.3,
                    child: Text(
                      'Look into the camera and press the "Capture" button to login!',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: width / 20,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ]
    );
  }
}

//Draws Circle for decoration
class CirclePainter extends CustomPainter {
  double screenWidth = 0;
  CirclePainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 2 + 40;
    Offset center = const Offset(0, 0);
    Path oval = Path();
    oval.addOval(Rect.fromCircle(center: center, radius: radius));

    Paint shadowPaint = Paint()
      ..color = Colors.black.withOpacity(0.25)
      ..maskFilter = const MaskFilter.blur(BlurStyle.normal, 50);
    canvas.drawPath(oval, shadowPaint);
    // draw circle
    Paint thumbPaint = Paint()
      ..color = const Color(0xFFFFD591)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

//Draws Lines for decoration
class LinePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = const Color(0xFF000000);
    paint.style = PaintingStyle.fill;
    canvas.drawLine(Offset.zero, Offset.infinite, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    throw UnimplementedError();
  }
}