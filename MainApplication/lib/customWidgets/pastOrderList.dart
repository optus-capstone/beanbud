

import 'package:BeanBud/classes/cart_item.dart';
import 'package:BeanBud/customWidgets/cart_product.dart';
import 'package:BeanBud/models/order_model.dart';
import 'package:BeanBud/page/order_items.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PastOrder extends StatelessWidget {
  const PastOrder({super.key, required this.order});

  final OrderModel order;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final date = DateFormat('dd-MM-yyyy').format(order.orderDate);
    final time = DateFormat('kk:mm').format(order.orderDate);
    double total = 0;

    for(CartItem item in order.items) {
      total += item.itemPrice;
    }

    //Converts the CartItems into CartProduct widget to display
    List<CartProduct> cartProductList = List.empty(growable: true);
    for (CartItem item in order.items) {
      cartProductList.add(CartProduct(cartItem: item));
    }

    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => OrderItems(items: order.items))
        );
      },
      child: Container(
          decoration: const BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.black54, width: 1)),
          ),
          height: width / 3,
          child: Row(
            children: [
              Padding(
                padding: EdgeInsets.all(width / 20),
                child: SizedBox(
                  height: width / 5,
                  width: width / 5,
                  child: Card(
                    child: Center(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: Image(image: AssetImage("assets/images/logo.png"),
                            fit: BoxFit.fill,
                            height: width / 4,
                            width: width / 4,
                            alignment: Alignment.center),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: width / 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      date,
                      style: TextStyle(
                          fontSize: width / 20, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      time,
                      style: TextStyle(fontSize: width / 35),
                    ),
                    Text(
                      order.orderType,
                      style: TextStyle(fontSize: width / 35),
                    ),
                    Text(
                      order.status,
                      style: TextStyle(fontSize: width / 35),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: width / 5,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    "\$${total.toStringAsFixed(2)}",
                    style: TextStyle(fontSize: width / 25),
                  )
                ],
              )
            ],
          )),
    );
  }
}