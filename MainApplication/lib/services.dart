import 'package:get_it/get_it.dart';
import 'package:BeanBud/facialRecognition/camera_service.dart';
import 'package:BeanBud/facialRecognition/face_detector_service.dart';
import 'package:BeanBud/facialRecognition/ml_service.dart';
import 'package:BeanBud/pushNotification/notification_service.dart';

//Services class for easy access to facial recognition and push notification services

final locator = GetIt.instance;

//Setup the services
void setupServices() {
  locator.registerLazySingleton<CameraService>(() => CameraService());
  locator.registerLazySingleton<FaceDetectorService>(() => FaceDetectorService());
  locator.registerLazySingleton<MLService>(() => MLService());
  locator.registerLazySingleton<NotificationService>(() => NotificationService());
}