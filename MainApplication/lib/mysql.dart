import 'dart:convert';

import 'package:BeanBud/classes/cart_item.dart';
import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/classes/orders.dart';
import 'package:BeanBud/classes/regular_order_instance.dart';
import 'package:BeanBud/classes/menu_desserts.dart';
import 'package:BeanBud/classes/menu_hot_drinks.dart';
import 'package:BeanBud/classes/menu_snacks.dart';
import 'package:BeanBud/models/item_model.dart';
import 'package:BeanBud/models/order_model.dart';
import 'package:intl/intl.dart';
import 'package:mysql1/mysql1.dart';

import 'classes/menu.dart';
import 'classes/menu_cold_drinks.dart';

//MySQL class for access to simple AWS RDS

//Database access parameters
class Mysql {
  static String host =
          'beanbud-database.cmnuelfqlcfy.us-west-1.rds.amazonaws.com',
      user = 'admin',
      password = 'D8zGIn9x2B3yGEYPaIgq',
      db = 'BeanBud';
  static int port = 3306;

  Mysql();

  //Return connection to the database
  Future<MySqlConnection> getConnection() async {
    var settings = ConnectionSettings(
        host: host, port: port, user: user, password: password, db: db);

    return await MySqlConnection.connect(settings);
  }

  //Return a list of menu items from the database
  void getMenuItems() async {
    var db = Mysql();

    String sql =
        'select * from Item;';

    await db.getConnection().then((conn) async {
      await conn.query(sql).then((results) {
        for (var res in results) {
          final ItemModel myitem = ItemModel(
            id: res['item_id'],
            itemName: res['item_name'].toString(),
            itemCategory: res['category'].toString(),
            itemDescription: res['item_description'].toString(),
            itemPrice: res['item_price'],
            itemImage: res['item_image'],
          );

          switch (myitem.itemCategory) {
            case "Hot Drink":
              {
                MenuHotDrinks.instance.addToMenu(myitem);
              }
              break;
            case "Cold Drink":
              {
                MenuColdDrinks.instance.addToMenu(myitem);
              }
              break;
            case "Snack":
              {
                MenuSnacks.instance.addToMenu(myitem);
              }
              break;
            case "Dessert":
              {
                MenuDesserts.instance.addToMenu(myitem);
              }
              break;
            default:
              {}
              break;
          }
          Menu.instance.addToMenu(myitem);
        }
      }).onError((error, stackTrace) {
        print(error);
        return null;
      });
      conn.close();
      print("Length: " + Menu.instance.getLength().toString());
    });
  }

  //Add new user to database
  Future<void> addUser() async {
    var db = Mysql();

    var conn = await db.getConnection();

    await conn.query(
        'INSERT INTO Customer (customer_id, first_name, coffee_count) VALUES (?, ?, 0)',
        [id, name]);
  }

  //Delete a user from database
  Future<void> deleteUser() async {
    var db = Mysql();

    var conn = await db.getConnection();
    await conn.query("DELETE FROM Orders WHERE customer_id = (?)", [id]);
    await conn.query("DELETE FROM Ratings WHERE customer_id = (?)", [id]);
    await conn.query('DELETE FROM Customer WHERE customer_id = (?)', [id]);
  }

  //Add ratings to database
  Future<void> addRating(double rating, int itemId) async {
    var db = Mysql();

    var conn = await db.getConnection();
    await conn.query(
        'INSERT INTO Ratings (customer_id, item_id, rating) VALUES (?, ?, ?)',
        [id, itemId, rating]);
  }

  //Get coffee count from user
  Future<int> getCoffeeCount() async {
    var db = Mysql();

    int coffeeCount = 0;

    var conn = await db.getConnection();
    var results = await conn.query(
        'SELECT coffee_count FROM Customer WHERE customer_id =(?)', [id]);
    for (var res in results) {
      coffeeCount = res['coffee_count'];
    }

    return coffeeCount;
  }

  //Add and order once accepted to database
  Future<void> addOrder(OrderModel order) async {
    var db = Mysql();

    final date = DateFormat('yyyy-MM-dd HH:mm:ss').format(order.orderDate);
    List<int> itemId = [];
    List<int> itemQuant = [];
    List<String> itemAtt = [];
    List<double> itemPrice = [];

    for (CartItem item in order.items) {
      itemQuant.add(item.itemQuantity);
      itemAtt.add(item.attributes.toString());
      itemId.add(item.itemId);
      itemPrice.add(item.itemPrice);
    }

    var conn = await db.getConnection();
    await conn.query(
        'INSERT INTO Orders (customer_id, order_date, item_id, item_quantity, item_attributes, status, order_type, item_price) VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
        [
          id,
          date,
          jsonEncode(itemId),
          jsonEncode(itemQuant),
          jsonEncode(itemAtt),
          "Pending",
          order.orderType,
          jsonEncode(itemPrice)
        ]);
    //Add order to orders list
    getOrders();
  }

  //Get user orders
  getOrders() async {
    Orders.instance.clearOrders();
    var db = Mysql();

    var conn = await db.getConnection();
    var results = await conn.query(
        'SELECT order_date, item_id, item_quantity, item_attributes, order_type, status, item_price FROM Orders WHERE customer_id =(?)',
        [id]);
    for (var res in results) {
      List itemId;
      List itemQuant;
      List itemPrice;
      List itemAtt = [];
      List<CartItem> items = [];
      itemId = jsonDecode(res["item_id"]) as List;
      itemQuant = jsonDecode(res["item_quantity"]) as List;
      itemAtt = (jsonDecode(res["item_attributes"]) as List).toList();
      itemPrice = jsonDecode(res['item_price']) as List;
      for (int i = 0; i < itemId.length; i++) {
        CartItem item = CartItem(
            itemId: itemId[i],
            itemQuantity: itemQuant[i],
            itemPrice: itemPrice[i],
            category: Menu.instance.findItemAtId(itemId[i]).itemCategory,
            itemDescription:
                Menu.instance.findItemAtId(itemId[i]).itemDescription,
            itemName: Menu.instance.findItemAtId(itemId[i]).itemName,
            itemImage: Menu.instance.findItemAtId(itemId[i]).itemImage,
            attributes: []);
        items.add(item);
      }
      final OrderModel order = OrderModel(
          orderDate: res['order_date'],
          orderType: res['order_type'],
          status: res['status'],
          items: items);
      Orders.instance.addToOrders(order);
    }
  }

  setCoffeeCount(int count) async {
    var db = Mysql();
    var conn = await db.getConnection();
    var results = await conn.query(
        'UPDATE Customer SET coffee_count=(?) WHERE customer_id =(?)',
        [count, id]);
  }

  //Add and order once accepted to database
  Future<void> addRegular(OrderModel order) async {
    var db = Mysql();

    List<int> itemId = [];
    List<int> itemQuant = [];
    List<String> itemAtt = [];
    List<double> itemPrice = [];

    for (CartItem item in order.items) {
      itemQuant.add(item.itemQuantity);
      itemAtt.add(item.attributes.toString());
      itemId.add(item.itemId);
      itemPrice.add(item.itemPrice);
    }

    var conn = await db.getConnection();

    //Delete previous regular order if it exists
    await conn.query('DELETE FROM Regulars WHERE customer_id =(?)', [id]);

    await conn.query(
        'INSERT INTO Regulars (customer_id, item_id, item_quantity, item_attributes, order_type, item_price) VALUES (?, ?, ?, ?, ?, ?)',
        [
          id,
          jsonEncode(itemId),
          jsonEncode(itemQuant),
          jsonEncode(itemAtt),
          order.orderType,
          jsonEncode(itemPrice)
        ]);
  }

  //Get user orders
  Future<void> getRegularOrder() async {
    var db = Mysql();
    List<CartItem> items = [];

    var conn = await db.getConnection();
    //Delete previous regular orders before adding a new one
    var results = await conn.query(
        'SELECT item_id, item_quantity, item_attributes, order_type, item_price FROM Regulars WHERE customer_id =(?)',
        [id]);
    for (var res in results) {
      List itemId;
      List itemAtt = [];
      List itemQuant;
      List itemPrice;
      itemId = jsonDecode(res["item_id"]) as List;
      itemQuant = jsonDecode(res["item_quantity"]) as List;
      itemAtt = (jsonDecode(res["item_attributes"]) as List).toList();
      itemPrice = jsonDecode(res['item_price']) as List;
      for (int i = 0; i < itemId.length; i++) {
        CartItem item = CartItem(
            itemId: itemId[i],
            itemQuantity: itemQuant[i],
            itemPrice: itemPrice[i],
            category: Menu.instance.findItemAtId(itemId[i]).itemCategory,
            itemDescription:
                Menu.instance.findItemAtId(itemId[i]).itemDescription,
            itemName: Menu.instance.findItemAtId(itemId[i]).itemName,
            itemImage: Menu.instance.findItemAtId(itemId[i]).itemImage,
            attributes: []);
        items.add(item);
      }
      Regular.regularOrder = OrderModel(
          orderDate: DateTime.now(),
          orderType: res['order_type'],
          status: '',
          items: items);
    }
  }
}
