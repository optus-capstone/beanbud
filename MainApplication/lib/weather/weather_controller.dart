//The weather directory has been implemented using code from the below reference:

//Lekwa,E (2021) WeatherApp [Source Code]. https://github.com/Lekwacious/WeatherApp

//Weather controller to return weather data

import 'package:get/get.dart';
import 'package:BeanBud/models/weather_model.dart';
import 'package:BeanBud/weather/weather_service.dart';

class WeatherController extends GetxController {
  final weatherService = Get.put(WeatherService());

  Future<Weather> getWeatherData() async {
    var res;
    try {
      res = await weatherService.getWeather();
      if (res.statusCode != 200 || res.statusCode != 201) {
      } else {}
    } catch (e) {
    }
    return Weather.fromJson(res.data);
  }
}