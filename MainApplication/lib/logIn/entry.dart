import 'package:flutter/material.dart';
import 'package:BeanBud/main.dart';

//Entry Screen default navigation from main.dart
class EntryScreen extends StatefulWidget {
  const EntryScreen({Key? key}) : super(key: key);

  @override
  State<EntryScreen> createState() => _EntryScreenState();
}

class _EntryScreenState extends State<EntryScreen> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Login(),
      ),
    );
  }
}
