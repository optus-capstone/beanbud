import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_analytics_pinpoint/amplify_analytics_pinpoint.dart';
import 'package:amplify_datastore/amplify_datastore.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:BeanBud/models/ModelProvider.dart';

import '../amplifyconfiguration.dart';

//Configure amplify services (Required for AWS Amplify Authentication)

Future<void> configureAmplify() async {
  final auth = AmplifyAuthCognito();
  final analytics = AmplifyAnalyticsPinpoint();
  final dataStore = AmplifyDataStore(modelProvider: ModelProvider.instance);
  final api = AmplifyAPI();

  try {
    Amplify.addPlugins([auth, analytics, api, dataStore]);

    await Amplify.configure(amplifyconfig);
  } on AmplifyAlreadyConfiguredException catch (e) {
    print(e);
  }
}
