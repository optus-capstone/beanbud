//User model class

class UserModel {
  UserModel({
    required this.id,
    required this.firstName,
    this.faceData
});

  String id;
  String firstName;
  List? faceData;
}
