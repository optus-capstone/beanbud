//The facialRecognition directory has been implemented with reference to code from the below reference:

//Carlomagno,M (2020) FaceRecognitionAuth [Source Code]. https://github.com/MCarlomagno/FaceRecognitionAuth

import 'dart:async';
import 'dart:io';

import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:BeanBud/facialRecognition/camera_service.dart';
import 'package:BeanBud/facialRecognition/face_detector_service.dart';
import 'package:BeanBud/facialRecognition/face_painter.dart';
import 'package:BeanBud/facialRecognition/ml_service.dart';
import 'package:BeanBud/services.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import '../customWidgets/auth_action_button_standalone.dart';

//Camera page is the page that uses facial recognition technology using the camera and google.mlKit to take a photo of
//the user

// A screen that allows users to take a picture using a given camera.
class TakePictureScreen extends StatefulWidget {
  const TakePictureScreen({Key? key}) : super(key: key);

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  Face? faceDetected;
  String? imagePath;
  Size? imageSize;

  bool _initializing = false;
  bool _detectingFaces = false;
  bool _saving = false;
  bool pictureTaken = false;
  bool _bottomSheetVisible = false;

  final FaceDetectorService _faceDetectorService = locator<FaceDetectorService>();
  final CameraService _cameraService = locator<CameraService>();
  final MLService _mlService = locator<MLService>();

  @override
  void initState() {
    super.initState();
    _start();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _cameraService.dispose();
    super.dispose();
  }

  _start() async {
    setState(() => _initializing = true);
    await _cameraService.initialize();
    await _mlService.initialize();
    _faceDetectorService.initialize();
    setState(() => _initializing = false);
    _frameFaces();
  }

  //On camera picture taken
  Future<bool> onShot() async {
    if (faceDetected == null) {
      showDialog(
        context: context,
        builder: (context) {
          return const AlertDialog(
            content: Text('No face detected!'),
          );
        },
      );
      return false;
    } else {
      //dispose();
      _saving = true;
      await Future.delayed(const Duration(milliseconds: 500));
      await Future.delayed(const Duration(milliseconds: 200));
      XFile? file = await _cameraService.takePicture();
      imagePath = file?.path;

      setState(() {
        _bottomSheetVisible = true;
        pictureTaken = true;
      });

      return true;
    }
  }

  _frameFaces() {
    imageSize = _cameraService.getImageSize();

    _cameraService.cameraController?.startImageStream((image) async {
      if (_cameraService.cameraController != null) {
        if (_detectingFaces) return;

        _detectingFaces = true;

        try {
          await _faceDetectorService.detectFacesFromImage(image);

          if (_faceDetectorService.faces.isNotEmpty) {
            setState(() {
              faceDetected = _faceDetectorService.faces[0];
            });
            if (_saving) {
              _mlService.setCurrentPrediction(image, faceDetected);
              setState(() {
                _saving = false;
              });
            }
          } else {
            setState(() {
              faceDetected = null;
            });
          }

          _detectingFaces = false;
        } catch (e) {
          print(e);
          _detectingFaces = false;
        }
      }
    });
  }

  _reload() {
    setState(() {
      _bottomSheetVisible = true;
      pictureTaken = false;
    });
    _start();
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery
        .of(context)
        .size
        .width;
    final double height = MediaQuery
        .of(context)
        .size
        .height;
    const double mirror = math.pi;

    late Widget body;
    if (_initializing) {
      body = const Center(
        child: CircularProgressIndicator(),
      );
    }

    if (!_initializing && pictureTaken) {
      body = SizedBox(
        width: width,
        height: height,
        child: Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(mirror),
            child: FittedBox(
              fit: BoxFit.cover,
              child: Image.file(File(imagePath!)),
            )),
      );
    }

    if (!_initializing && !pictureTaken) {
      body = Transform.scale(
        scale: 1.0,
        child: AspectRatio(
          aspectRatio: MediaQuery
              .of(context)
              .size
              .aspectRatio,
          child: OverflowBox(
            alignment: Alignment.center,
            child: FittedBox(
              fit: BoxFit.fitHeight,
              child: SizedBox(
                width: width,
                height:
                width * _cameraService.cameraController!.value.aspectRatio,
                child: Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    CameraPreview(_cameraService.cameraController!),
                    CustomPaint(
                      painter: FacePainter(
                          face: faceDetected, imageSize: imageSize!),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    }


    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: false,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(40.0)),
          ),
          //BeanBud title
          title: Column(
              crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text('BeanBud', style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: width / 16,
                fontFamily: 'Segoe-UI'),
            ),
            Text(' Coffee for you',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w900,
                  fontSize: width / 40,
                  fontFamily: 'Segoe-UI'),
            ),
          ]),
          leading: Padding(
            padding: EdgeInsets.only(left: width / 40),
            child: const Image(
              //AppBar BeanBud Logo
              image: AssetImage('assets/images/icon.png'),
            ),
          ),
          toolbarHeight: width / 6.5,
        ),
        body: body,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: !_bottomSheetVisible
            ? AuthActionButton(
          // Provide an onPressed callback
          onPressed: onShot,
          isLogin: false,
          reload: _reload,
        ) : Container()
    );
  }
}

//Draws Circle
class CirclePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = 230;
    Offset center = const Offset(200.0, 200.0);
    Path oval = Path();
    oval.addOval(Rect.fromCircle(center: center, radius: radius));

    Paint shadowPaint = Paint()
      ..color = Colors.black.withOpacity(0.25)
      ..maskFilter = const MaskFilter.blur(BlurStyle.normal, 50);
    canvas.drawPath(oval, shadowPaint);
    // draw circle
    Paint thumbPaint = Paint()
      ..color = const Color(0xFFFFD591)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}