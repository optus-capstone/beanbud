import 'package:BeanBud/classes/cart_item.dart';
import 'package:BeanBud/models/order_model.dart';

class Regular {
  static final Regular _singleton = Regular._privateContructor();
  static OrderModel regularOrder = OrderModel(
      items: [],
      orderDate: DateTime.now(),
      orderType: '',
      status: 'null'
  );

  Regular._privateContructor();

  static Regular get instance => _singleton;

  List<CartItem> getItems() {
    return regularOrder.items;
  }

  CartItem getAtIndex(int index) {
    return regularOrder.items[index];
  }

  double getRegularTotal() {
    double total = 0.0;
    for(CartItem item in regularOrder.items) {
      total += item.itemPrice;
    }
    return total;
  }

  void reset() {
    regularOrder = OrderModel(
        items: [],
        orderDate: DateTime.now(),
        orderType: '',
        status: 'null'
    );
  }
}