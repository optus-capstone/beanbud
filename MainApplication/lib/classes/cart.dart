//A Singleton consisting of a cart
//Author: Darren Ou Yang
//Last Updated" 23/09/2022

import 'dart:collection';

import 'package:BeanBud/classes/cart_item.dart';
import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/classes/menu.dart';

class Cart {
  static final Cart _singleton = Cart._privateContructor();
  static Queue<CartItem> cartList = Queue();
  int coffeeCountCart = 0;
  String orderType = 'Takeaway';

  Cart._privateContructor();

  static Cart get instance => _singleton;

  addToCart(CartItem item) {
    item.itemPrice = item.itemQuantity * item.itemPrice;
    if (item.category == "Hot Drink" || item.category == "Cold Drink") {
      coffeeCountCart += item.itemQuantity;
      if (coffeeCountCart + coffeeCount >= 9) {
        if (item.itemQuantity > 1) {
          item.itemPrice -= Menu.instance.findItemAtId(item.itemId).itemPrice;
          item.attributes.add("Free Beverage!");
        } else {
          item.itemPrice = 0.0;
          item.attributes.add("Free Beverage!");
        }
      }
    }
    cartList.add(item);
  }

  removeFromCart(CartItem item) {
    if (item.category == "Hot Drink" || item.category == "Cold Drink") {
      coffeeCountCart -= item.itemQuantity;
    }
    cartList.remove(item);
  }

  getLength() {
    return cartList.length;
  }

  findItemAtIndex(int index) {
    return cartList.elementAt(index);
  }

  getCartTotal() {
    double total = 0;
    for (var items in cartList) {
      total += (items.itemPrice);
    }
    return total;
  }

  emptyCart() {
    cartList.clear();
  }
}
