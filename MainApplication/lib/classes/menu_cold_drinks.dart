import 'dart:collection';
import 'package:BeanBud/models/item_model.dart';

class MenuColdDrinks {
  static final MenuColdDrinks _singleton = MenuColdDrinks._privateContructor();
  static Queue<ItemModel> coldDrinkList = Queue();

  MenuColdDrinks._privateContructor();

  static MenuColdDrinks get instance => _singleton;

  void addToMenu(ItemModel item) {
    coldDrinkList.add(item);
  }

  void removeFromMenu(ItemModel item) {
    coldDrinkList.remove(item);
  }

  int getLength() {
    return coldDrinkList.length;
  }

  ItemModel findItemAtIndex(int index) {
    return coldDrinkList.elementAt(index);
  }
}
