import 'dart:collection';
import 'package:BeanBud/models/item_model.dart';

class MenuHotDrinks {
  static final MenuHotDrinks _singleton = MenuHotDrinks._privateContructor();
  static Queue<ItemModel> hotDrinkList = Queue();

  MenuHotDrinks._privateContructor();

  static MenuHotDrinks get instance => _singleton;

  void addToMenu(ItemModel item) {
    hotDrinkList.add(item);
  }

  void removeFromMenu(ItemModel item) {
    hotDrinkList.remove(item);
  }

  int getLength() {
    return hotDrinkList.length;
  }

  ItemModel findItemAtIndex(int index) {
    return hotDrinkList.elementAt(index);
  }
}
