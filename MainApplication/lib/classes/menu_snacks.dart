import 'dart:collection';
import 'package:BeanBud/models/item_model.dart';

class MenuSnacks {
  static final MenuSnacks _singleton = MenuSnacks._privateContructor();
  static Queue<ItemModel> SnacksList = Queue();

  MenuSnacks._privateContructor();

  static MenuSnacks get instance => _singleton;

  void addToMenu(ItemModel item) {
    SnacksList.add(item);
  }

  void removeFromMenu(ItemModel item) {
    SnacksList.remove(item);
  }

  int getLength() {
    return SnacksList.length;
  }

  ItemModel findItemAtIndex(int index) {
    return SnacksList.elementAt(index);
  }
}
