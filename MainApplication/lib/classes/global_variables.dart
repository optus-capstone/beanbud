import 'package:BeanBud/classes/cart.dart';
import 'package:BeanBud/classes/recommender.dart';
import 'package:BeanBud/mysql.dart';
import 'package:amplify_flutter/amplify_flutter.dart';

//Global variables used throughout the application

double cafeLatitude = -32.00627;
double cafeLongitude = 115.89247;

String name = '';
String id = '';
int coffeeCount = 0;


Future<void> fetchCurrentUserAttributes() async {
  try {
    final result = await Amplify.Auth.fetchUserAttributes();
    for (var element in result) {
      if (element.userAttributeKey == CognitoUserAttributeKey.name) {
        name = element.value;
      }
      if (element.userAttributeKey == CognitoUserAttributeKey.sub) {
        id = element.value;
      }
    }
    getCoffeeCount();
    //Run get orders once menu has been populated
    Mysql().getOrders();
    Mysql().getRegularOrder();
    Recommender().getRecommendations();
  } on AuthException catch (e) {
    print(e.message);
  }
}

Future<void> getCoffeeCount() async {
  coffeeCount = await Mysql().getCoffeeCount();
}

coffeeLoyalty() {
  int count = coffeeCount + Cart.instance.coffeeCountCart;
  if(count > 8) {
    Mysql().setCoffeeCount(count % 8);
  }
  else {
    Mysql().setCoffeeCount(count);
  }
}