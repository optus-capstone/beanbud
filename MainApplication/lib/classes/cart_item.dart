//Class to handle items added to the cart
//Needs to updated to compose of Item Model
//Author: Darren Ou Yang
//Last Updated" 23/09/2022
import 'package:BeanBud/classes/itemOption.dart';
import 'package:BeanBud/classes/kiosk_cart_item.dart';

class CartItem {
  CartItem({
    required this.itemId,
    required this.itemName,
    required this.itemDescription,
    required this.category,
    required this.itemImage,
    required this.itemPrice,
    required this.itemQuantity,
    required this.attributes,
  });

  int itemId;
  String itemName;
  String itemDescription;
  String category;
  var itemImage;
  double itemPrice;
  int itemQuantity;
  List attributes;

  KioskCartItem toKioskCartItem() {
    List<ItemOption> itemOptionAttributes = List.empty(growable: true);
    for (int ii = 0; ii < attributes.length; ii++) {
      ItemOption itemOption;
      if (ii == 0) {
        itemOption = ItemOption("Size", attributes.elementAt(ii));
      } else {
        itemOption = ItemOption("Milk", attributes.elementAt(ii));
      }
      itemOptionAttributes.add(itemOption);
    }

    return KioskCartItem(
        itemId: itemId,
        itemName: itemName,
        itemDescription: itemDescription,
        category: category,
        itemImage: itemImage,
        itemPrice: itemPrice,
        itemQuantity: itemQuantity,
        attributes: itemOptionAttributes);
  }
}
