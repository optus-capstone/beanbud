import 'package:BeanBud/classes/cart_item.dart';
import 'package:BeanBud/classes/itemOption.dart';

class KioskCartItem {
  KioskCartItem({
    required this.itemId,
    required this.itemName,
    required this.itemDescription,
    required this.category,
    required this.itemImage,
    required this.itemPrice,
    required this.itemQuantity,
    required this.attributes,
  });

  int itemId;
  String itemName;
  String itemDescription;
  String category;
  var itemImage;
  double itemPrice;
  int itemQuantity;
  List<ItemOption> attributes;

  CartItem toCartItem() {
    List cartItemAttributes = List.empty(growable: true);
    if (attributes.isNotEmpty) {
      cartItemAttributes.add(attributes.elementAt(0).option);
      cartItemAttributes.add(attributes.elementAt(1).option);
    }

    CartItem cartItem = CartItem(
        itemId: itemId,
        itemName: itemName,
        itemDescription: itemDescription,
        category: category,
        itemImage: itemImage,
        itemPrice: itemPrice,
        itemQuantity: itemQuantity,
        attributes: cartItemAttributes);

    return cartItem;
  }
}
