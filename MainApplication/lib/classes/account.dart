//Class to handle accounts
//Unused, needs to be updated
//Author: Darren Ou Yang
//Last Updated" 23/09/2022
class Account {
  Account({
    required this.accountName,
    required this.itemDescription,
    required this.itemImage,
    required this.itemPrice,
    required this.itemQuantity
  });

  String accountName;
  String itemDescription;
  var itemImage;
  double itemPrice;
  int itemQuantity;
}
