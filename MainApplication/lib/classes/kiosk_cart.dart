//A Singleton consisting of a cart
//Author: Darren Ou Yang
//Last Updated" 23/09/2022

import 'dart:collection';

import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/classes/itemOption.dart';
import 'package:BeanBud/classes/menu.dart';

import 'kiosk_cart_item.dart';

class KioskCart {
  static final KioskCart _singleton = KioskCart._privateContructor();
  static Queue<KioskCartItem> KioskCartList = Queue();
  int coffeeCountKioskCart = 0;
  String orderType = 'Takeaway';

  KioskCart._privateContructor();

  static KioskCart get instance => _singleton;

  addToKioskCart(KioskCartItem item) {
    if (item.category == "Hot Drink" || item.category == "Cold Drink") {
      coffeeCountKioskCart += item.itemQuantity;
      if (coffeeCountKioskCart + coffeeCount == 9) {
        if (item.itemQuantity > 1) {
          item.itemPrice -= Menu.instance.findItemAtId(item.itemId).itemPrice;
          item.attributes.add(ItemOption("Free", "Beverage!"));
        } else {
          item.itemPrice = 0.0;
          item.attributes.add(ItemOption("Free", "Beverage!"));
        }
      }
    }
    KioskCartList.add(item);
  }

  removeFromKioskCart(KioskCartItem item) {
    if (item.category == "Hot Drink" || item.category == "Cold Drink") {
      coffeeCountKioskCart -= item.itemQuantity;
    }
    KioskCartList.remove(item);
  }

  getLength() {
    return KioskCartList.length;
  }

  findItemAtIndex(int index) {
    return KioskCartList.elementAt(index);
  }

  getKioskCartTotal() {
    double total = 0;
    for (var items in KioskCartList) {
      total += (items.itemPrice);
    }
    return total;
  }

  emptyKioskCart() {
    KioskCartList.clear();
  }
}
