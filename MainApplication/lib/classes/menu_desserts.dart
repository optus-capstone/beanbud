import 'dart:collection';
import 'package:BeanBud/models/item_model.dart';

class MenuDesserts {
  static final MenuDesserts _singleton = MenuDesserts._privateContructor();
  static Queue<ItemModel> dessertList = Queue();

  MenuDesserts._privateContructor();

  static MenuDesserts get instance => _singleton;

  void addToMenu(ItemModel item) {
    dessertList.add(item);
  }

  void removeFromMenu(ItemModel item) {
    dessertList.remove(item);
  }

  int getLength() {
    return dessertList.length;
  }

  ItemModel findItemAtIndex(int index) {
    return dessertList.elementAt(index);
  }
}
