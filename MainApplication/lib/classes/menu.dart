import 'dart:collection';
import 'package:BeanBud/models/item_model.dart';

class Menu {
  static final Menu _singleton = Menu._privateContructor();
  static Queue<ItemModel> menuList = Queue();

  Menu._privateContructor();

  static Menu get instance => _singleton;

  void addToMenu(ItemModel item) {
    menuList.add(item);
  }

  void removeFromMenu(ItemModel item) {
    menuList.remove(item);
  }

  int getLength() {
    return menuList.length;
  }

  ItemModel findItemAtIndex(int index) {
    return menuList.elementAt(index);
  }

  ItemModel findItemAtId(int id) {
    for(ItemModel item in menuList) {
      if(id == item.id) {
        return item;
      }
    }
    return menuList.first;
  }

  ItemModel findItemAtName(String name) {
    for(ItemModel item in menuList) {
      if(name == item.itemName) {
        return item;
      }
    }
    return menuList.first;
  }

  String getItemCategoryAtIndex(int index) {
    return menuList.elementAt(index).itemCategory;
  }

  Queue<int> getAllItemId() {
    Queue<int> list = Queue();
    for(ItemModel items in menuList) {
      list.add(items.id);
    }
    return list;
  }
}
