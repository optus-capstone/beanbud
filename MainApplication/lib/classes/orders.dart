import 'dart:collection';
import 'package:BeanBud/classes/cart_item.dart';
import 'package:BeanBud/classes/menu.dart';
import 'package:BeanBud/models/order_model.dart';

class Orders {
  static final Orders _singleton = Orders._privateContructor();
  static Set<OrderModel> orderList = Set();

  Queue<int> newItems = Menu.instance.getAllItemId();

  Orders._privateContructor();

  static Orders get instance => _singleton;

  void addToOrders(OrderModel item) {
    orderList.add(item);
    for(CartItem item in item.items) {
      newItems.remove(item.itemId);
    }
  }

  void removeFromOrders(OrderModel item) {
    orderList.remove(item);
  }

  int getLength() {
    return orderList.length;
  }

  OrderModel findItemAtIndex(int index) {
    return orderList.elementAt(index);
  }

  void clearOrders() {
    orderList = Set();
  }

  List<int> getOrderedItems() {
    List<int> items = [];
    for(OrderModel order in orderList) {
      for(CartItem item in order.items) {
        items.add(item.itemId);
      }
    }
    return items;
  }
}