import 'package:BeanBud/models/item_model.dart';

class Recommended {
  static final Recommended _singleton = Recommended._privateContructor();
  static List<ItemModel> recommendList = [];

  Recommended._privateContructor();

  static Recommended get instance => _singleton;

  List<ItemModel> getItems() {
    return recommendList;
  }

  void addItem(ItemModel itemModel) {
    recommendList.add(itemModel);
  }
}