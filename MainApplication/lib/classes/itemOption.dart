class ItemOption {
  String expandableOption = "";
  String option = "";

  ItemOption(String expandableOption, String option) {
    this.expandableOption = expandableOption;
    this.option = option;
  }
}
