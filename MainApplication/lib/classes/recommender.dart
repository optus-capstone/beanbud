import 'package:BeanBud/classes/global_variables.dart';
import 'package:BeanBud/classes/menu.dart';
import 'package:BeanBud/classes/recommended_items.dart';
import 'package:http/http.dart';

import 'dart:convert';

class Recommender {
  Future<void> getRecommendations() async {
    String user_id = id;
    var data = await _getData('http://13.236.181.169:5000/$user_id');

    var decodedData = jsonDecode(data);
    List strings = [];

    strings = ((decodedData['query'].split("b")) as List);
    strings = ((decodedData['query'].split("'")) as List);
    Recommended.instance.addItem(Menu.instance.findItemAtName(strings[1]));
    Recommended.instance.addItem(Menu.instance.findItemAtName(strings[3]));
    Recommended.instance.addItem(Menu.instance.findItemAtName(strings[5]));
  }

  Future _getData(String url) async {
  Response response = await get(Uri.parse(url));
  return response.body;
  }
}