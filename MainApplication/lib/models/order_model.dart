//Order Model Class

import 'package:BeanBud/classes/cart_item.dart';

class OrderModel {
  OrderModel({
    required this.orderType,
    required this.orderDate,
    required this.status,
    required this.items,
});

  String orderType;
  DateTime orderDate;
  String status;
  List<CartItem> items;
}