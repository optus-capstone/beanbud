//Item model class

class ItemModel {
  ItemModel({
    required this.id,
    required this.itemName,
    required this.itemDescription,
    required this.itemImage,
    required this.itemPrice,
    required this.itemCategory,
  });

  int id;
  String itemName;
  double itemPrice;
  String itemCategory;
  String itemDescription;
  var itemImage;
}
