import 'dart:collection';
import 'dart:math';

import 'package:BeanBud/classes/cart_item.dart';
import 'package:flutter/material.dart';

import '../classes/global_variables.dart';
import '../classes/kiosk_cart.dart';
import '../classes/kiosk_cart_item.dart';
import '../models/order_model.dart';
import '../mysql.dart';
import 'cart_item_kiosk_widget.dart';
import 'confirm_kiosk.dart';

//Author: Darren Ou Yang
//Last Updated" 23/09/2022
//Cart Page

//Header for the cart page

class CartKiosk extends StatelessWidget {
  const CartKiosk({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //Width to create a responsive app
    final double width = MediaQuery.of(context).size.width;
    //Converts the CartItems into CartProduct widget to display
    List<CartItemKioskWidget> cartProductList = List.empty(growable: true);
    double cartTotalPrice = 0;
    for (int ii = 0; ii < KioskCart.instance.getLength(); ii++) {
      KioskCartItem cartItem = KioskCart.instance.findItemAtIndex(ii);
      cartProductList.add(CartItemKioskWidget(cartItem: cartItem));
      print("CartItem Price: " + cartItem.itemPrice.toString());
      cartTotalPrice = cartTotalPrice + cartItem.itemPrice;
    }

    //Single tab as a header
    return DefaultTabController(
      length: 1,
      child: Builder(builder: (BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            toolbarHeight: width / 6.5,
            backgroundColor: Colors.white,
            //Bar
            shape: const RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomLeft: Radius.circular(40.0)),
            ),
            shadowColor: Colors.grey.withOpacity(0.5),
            title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              //Icon
              Image.asset(
                "assets/images/icon.png",
                fit: BoxFit.contain,
                height: width / 9,
                width: width / 9,
              ),
              //Sub-heading
              Padding(
                padding: EdgeInsets.only(left: width / 25),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Bean Bud",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: width / 15,
                      ),
                    ),
                    Text(
                      "Coffee For You",
                      style: TextStyle(
                        fontSize: width / 35,
                      ),
                    )
                  ],
                ),
              ),
            ]),
          ),
          //Tab Bar
          body: Column(children: [
            //Cart items displayed as widgets(CartProduct)
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(left: width / 3),
                child: CustomPaint(
                  painter: CirclePainter(width),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: width / 20),
              child: Text(
                "Your Cart",
                style: TextStyle(
                    fontSize: width / 12, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: width / 20,
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                height: width * 0.9,
                width: width / 1.2,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      spreadRadius: 5.0,
                      blurRadius: 7,
                      offset: Offset(0, 0), // changes position of shadow
                    ),
                  ],
                ),
                child: ListView(
                  children: cartProductList,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                height: width / 4.5,
                width: width / 1.2,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      spreadRadius: 5.0,
                      blurRadius: 7,
                      offset: Offset(0, 0), // changes position of shadow
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: width / 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            "Takeaway",
                            style: TextStyle(fontSize: width / 35),
                          ),
                          Text(
                            "Total " + cartTotalPrice.toStringAsFixed(2),
                            style: TextStyle(
                                fontSize: width / 25,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(width / 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          SizedBox(
                            width: width / 4,
                            height: width / 10,
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ))),
                              child: Center(
                                child: Text(
                                  'Back',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'AdobeCleanUXBold',
                                      fontSize: width / 20),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: width / 2.5,
                            height: width / 10,
                            child: ElevatedButton(
                              onPressed: () {
                                if (KioskCart.instance.getLength() != 0) {
                                  List<CartItem> cartList =
                                      List.empty(growable: true);
                                  for (int ii = 0;
                                      ii < KioskCart.instance.getLength();
                                      ii++) {
                                    KioskCartItem kioskCartItem =
                                        KioskCart.instance.findItemAtIndex(ii);
                                    cartList.add(kioskCartItem.toCartItem());
                                  }
                                  Mysql().addOrder(OrderModel(
                                      orderType: KioskCart.instance.orderType,
                                      orderDate: DateTime.now(),
                                      status: "Pending",
                                      items: cartList));
                                  coffeeLoyalty();
                                  KioskCart.instance..emptyKioskCart();
                                  KioskCart.instance.coffeeCountKioskCart = 0;
                                  getCoffeeCount();
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            new ConfirmKiosk()),
                                  );
                                } else {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        _buildPopupDialog(
                                            context, "Your Cart is Empty!"),
                                  );
                                }
                              },
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ))),
                              child: Center(
                                child: Text(
                                  'Confirm',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'AdobeCleanUXBold',
                                      fontSize: width / 20),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ]),
        );
      }),
    );
  }
}

class CirclePainter extends CustomPainter {
  double screenWidth = 0;
  CirclePainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 2 + 40;
    Offset center = const Offset(0, 0);

    // draw circle
    Paint thumbPaint = Paint()
      ..color = const Color(0xFFFFD591)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}

Widget _buildPopupDialog(BuildContext context, String text) {
  return new AlertDialog(
    title: const Text('Error'),
    content: new Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(text),
      ],
    ),
    actions: <Widget>[
      new ElevatedButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: const Text('Close'),
      ),
    ],
  );
}
