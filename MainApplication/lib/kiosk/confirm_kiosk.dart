import 'dart:async';
import 'dart:collection';
import 'dart:math';

import 'package:BeanBud/classes/kiosk_cart.dart';
import 'package:BeanBud/kiosk/cart_kiosk.dart';
import 'package:BeanBud/kiosk/start_kiosk.dart';
import 'package:flutter/material.dart';
import 'package:BeanBud/classes/cart_item.dart';
import 'package:BeanBud/classes/cart.dart';
import 'package:BeanBud/customWidgets/cart_product.dart';
import 'package:BeanBud/customWidgets/cart_setting.dart';

import '../classes/kiosk_cart_item.dart';
import 'cart_item_kiosk_widget.dart';

//Author: Darren Ou Yang
//Last Updated" 23/09/2022
//Cart Page

//Header for the cart page
var random = Random();

class ConfirmKiosk extends StatefulWidget {
  const ConfirmKiosk({super.key});
  @override
  State<ConfirmKiosk> createState() => _ConfirmKioskState();
}

//Quantity Counter
class _ConfirmKioskState extends State<ConfirmKiosk> {
  @override
  Widget build(BuildContext context) {
    //Width to create a responsive app
    final double width = MediaQuery.of(context).size.width;
    //Converts the CartItems into CartProduct widget to display
    List<CartItemKioskWidget> cartProductList = List.empty(growable: true);
    for (int ii = 0; ii < Cart.instance.getLength(); ii++) {
      KioskCartItem cartItem = Cart.instance.findItemAtIndex(ii);
      cartProductList.add(CartItemKioskWidget(cartItem: cartItem));
    }

    /*
    Timer? countdownTimer;
    Duration myDuration = Duration(seconds: 10);
    void stopTimer() {
      setState(() => countdownTimer!.cancel());
    }

    void resetTimer() {
      stopTimer();
      setState(() => myDuration = Duration(days: 5));
    }

    void setCountDown() {
      final reduceSecondsBy = 1;
      setState(() {
        int seconds = myDuration.inSeconds - reduceSecondsBy;
        if (seconds < 0) {
          stopTimer();
          resetTimer();
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => StartKiosk()));
        } else {
          myDuration = Duration(seconds: seconds);
        }
      });
    }

    void startTimer() {
      countdownTimer =
          Timer.periodic(Duration(seconds: 1), (_) => setCountDown());
    }
    

    startTimer();
    */
    KioskCart.instance.emptyKioskCart();

    //Single tab as a header
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: width / 6.5,
        backgroundColor: Colors.white,
        //Bar
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40.0)),
        ),
        shadowColor: Colors.grey.withOpacity(0.5),
        title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          //Icon
          Image.asset(
            "assets/images/icon.png",
            fit: BoxFit.contain,
            height: width / 9,
            width: width / 9,
          ),
          //Sub-heading
          Padding(
            padding: EdgeInsets.only(left: width / 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Bean Bud",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: width / 15,
                  ),
                ),
                Text(
                  "Coffee For You",
                  style: TextStyle(
                    fontSize: width / 35,
                  ),
                )
              ],
            ),
          ),
        ]),
      ),
      //Tab Bar
      body: InkWell(
        onTap: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => const StartKiosk()),
          );
        },
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: CustomPaint(
                painter: CirclePainter(width),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.only(top: width / 4),
                child: Column(children: [
                  //Cart items displayed as widgets(CartProduct)
                  Padding(
                    padding: EdgeInsets.only(top: width / 20),
                    child: SizedBox(
                      width: width / 1.4,
                      child: Text(
                        "Pay at Counter Thanks!",
                        style: TextStyle(
                            fontSize: width / 12, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: width / 10,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: width / 20),
                    child: SizedBox(
                      width: width / 1.4,
                      child: Text(
                        "Your Order Number: " + random.nextInt(100).toString(),
                        style: TextStyle(
                            fontSize: width / 7, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CirclePainter extends CustomPainter {
  double screenWidth = 0;
  CirclePainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 2 + 40;
    Offset center = const Offset(0, 0);

    // draw circle
    Paint thumbPaint = Paint()
      ..color = const Color(0xFFFFD591)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}
