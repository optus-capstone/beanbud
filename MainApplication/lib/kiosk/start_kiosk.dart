import 'package:BeanBud/main.dart';
import 'package:flutter/material.dart';
import 'package:BeanBud/kiosk/home_kiosk.dart';

import '../facialRecognition/camera_service.dart';
import '../facialRecognition/face_detector_service.dart';
import '../facialRecognition/ml_service.dart';
import '../mysql.dart';
import '../services.dart';
import 'facial_sign_in.dart';
import 'main_kiosk.dart';

//Author: Darren Ou Yang
//Last Updated" 23/09/2022
//Kiosk Start Screen
//todo: Camera Functionality

enum MenuItem {
  kioskMobileMode,
}

class StartKiosk extends StatefulWidget {
  const StartKiosk({super.key});
  @override
  State<StartKiosk> createState() => _StartKioskState();
}

class _StartKioskState extends State<StartKiosk> {
  final MLService _mlService = locator<MLService>();
  final FaceDetectorService _mlKitService = locator<FaceDetectorService>();
  final CameraService _cameraService = locator<CameraService>();
  bool loading = false;

  @override
  void initState() {
    super.initState();
    _initializeServices();
  }

  _initializeServices() async {
    setState(() => loading = true);
    await _cameraService.initialize();
    await _mlService.initialize();
    _mlKitService.initialize();
    setState(() => loading = false);
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;
    //Scaffold used to hold all child widget of the screen
    return Scaffold(
        //Appbar implementation
        appBar: AppBar(
          toolbarHeight: width / 6.5,
          backgroundColor: Colors.white,
          //Bar
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40.0)),
          ),
          shadowColor: Colors.grey.withOpacity(0.5),
          title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            //Icon
            Image.asset(
              "assets/images/icon.png",
              fit: BoxFit.contain,
              height: width / 9,
              width: width / 9,
            ),
            //Sub-heading
            Padding(
              padding: EdgeInsets.only(left: width / 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Bean Bud",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: width / 15,
                    ),
                  ),
                  Text(
                    "Coffee For You",
                    style: TextStyle(
                      fontSize: width / 35,
                    ),
                  )
                ],
              ),
            ),
          ]),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 15.0),
              child: PopupMenuButton<MenuItem>(
                  iconSize: width / 15,
                  icon: const Icon(Icons.menu_sharp, color: Colors.black),
                  onSelected: (value) {
                    if (value == MenuItem.kioskMobileMode) {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => const Login()),
                      );
                    }
                  },
                  padding: EdgeInsets.all(width / 40),
                  itemBuilder: (context) => [
                        PopupMenuItem(
                          value: MenuItem.kioskMobileMode,
                          child: Text('Mobile Mode',
                              style: TextStyle(fontSize: width / 15)),
                        ),
                      ]),
            ),
          ],
        ),

        //When the screen is tapped, navigate to the home_kiosk
        body: InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const MainKiosk()),
            );
          },
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.only(top: height / 20),
                  child: CustomPaint(
                    painter: CirclePainter(width * 1.2),
                  ),
                ),
              ),
              Column(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: SizedBox(
                      height: width,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: width / 20),
                            child: SizedBox(
                              width: width / 1.3,
                              child: Text(
                                "Stand here to sign in!",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: width / 15,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(height: width / 5),
                          //Sign in button
                          SizedBox(
                            width: width / 2,
                            height: width / 5,
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const SignIn()),
                                );
                              },
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ))),
                              child: Center(
                                child: Text(
                                  'Tap here to Sign In',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'AdobeCleanUXBold',
                                      fontSize: width / 40),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: CustomPaint(
                  painter: CirclePainter(width),
                ),
              ),
              //Prompt to touch the screen to begin order
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.only(bottom: width / 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: width / 2),
                        child: Image(
                            height: width / 4,
                            width: width / 4,
                            image: const AssetImage(
                                'assets/images/touchIcon.png')),
                      ),
                      SizedBox(
                        width: width / 1.3,
                        child: Text(
                          "Or Touch Anywhere to Start!",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: width / 15,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}

//Creates a circle
class CirclePainter extends CustomPainter {
  double screenWidth = 0;
  CirclePainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 2 + 40;
    Offset center = const Offset(0, 0);

    // draw circle
    Paint thumbPaint = Paint()
      ..color = const Color(0xFFFFD591)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}

//Tempoarary circle creationg in stand for a camera
class CameraCirclePainter extends CustomPainter {
  double screenWidth = 0;
  CameraCirclePainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 2.3;
    Offset center = const Offset(0, 0);

    // draw circle
    Paint thumbPaint = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}
