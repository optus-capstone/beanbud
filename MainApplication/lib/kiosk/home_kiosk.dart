import 'dart:math';

import 'package:BeanBud/classes/cart_item.dart';
import 'package:BeanBud/classes/menu.dart';
import 'package:BeanBud/kiosk/product_kiosk.dart';
import 'package:flutter/material.dart';
import 'package:BeanBud/kiosk/regular_item.dart';

import '../classes/global_variables.dart';
import '../classes/kiosk_cart.dart';
import '../classes/kiosk_cart_item.dart';
import '../classes/regular_order_instance.dart';
import '../models/order_model.dart';
import '../mysql.dart';
import 'confirm_kiosk.dart';

var random = Random();
bool _loggedIn = false;

class HomeKiosk extends StatefulWidget {
  const HomeKiosk({
    Key? key,
  }) : super(key: key);

  @override
  State<HomeKiosk> createState() => _HomeKioskState();
}

class _HomeKioskState extends State<HomeKiosk> {
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    List<CartItem> items = List.empty(growable: true);
    double total = 0.0;
    String widgetTitle = "";
    if (Menu.instance.getLength() != 0) {
      var n1 = random.nextInt(Menu.instance.getLength());
      var n2 = random.nextInt(Menu.instance.getLength() - 1);
      if (n2 >= n1) n2 += 1;

      if (name != '' && Regular.instance.getItems().length != 0) {
        for (int ii = 0; ii < Regular.instance.getItems().length; ii++) {
          items.add(Regular.instance.getAtIndex(ii));
          total = total + Regular.instance.getAtIndex(ii).itemPrice;
          widgetTitle = "Your Regular";
        }
      } else {
        items.add(CartItem(
            itemQuantity: 1,
            itemImage: Menu.instance.findItemAtIndex(n1).itemImage,
            itemDescription: Menu.instance.findItemAtIndex(n1).itemDescription,
            itemPrice: Menu.instance.findItemAtIndex(n1).itemPrice,
            itemId: Menu.instance.findItemAtIndex(n1).id,
            itemName: Menu.instance.findItemAtIndex(n1).itemName,
            category: Menu.instance.findItemAtIndex(n1).itemCategory,
            attributes: []));
        items.add(CartItem(
            itemQuantity: 1,
            itemImage: Menu.instance.findItemAtIndex(n2).itemImage,
            itemDescription: Menu.instance.findItemAtIndex(n2).itemDescription,
            itemPrice: Menu.instance.findItemAtIndex(n2).itemPrice,
            itemId: Menu.instance.findItemAtIndex(n2).id,
            itemName: Menu.instance.findItemAtIndex(n2).itemName,
            category: Menu.instance.findItemAtIndex(n2).itemCategory,
            attributes: []));
        total = items[0].itemPrice + items[1].itemPrice;
        widgetTitle = "Recommended Items";
      }
    }
    return Padding(
      padding: EdgeInsets.only(top: width / 4),
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: Container(
                width: width / 1.3,
                height: width / 10,
                decoration: BoxDecoration(
                  color: Colors.cyan,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(15.0),
                      bottomLeft: Radius.circular(15.0)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 3,
                      offset: const Offset(0, 3),
                    )
                  ],
                ),
                child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      widgetTitle,
                      style: TextStyle(
                          fontSize: width / 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ))),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              height: width / 1.7,
              width: width / 1.4,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                    bottom: BorderSide(
                        color: Colors.grey.withOpacity(0.5), width: 3)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 3,
                    blurRadius: 3,
                  )
                ],
              ),
              child: Scrollbar(
                thickness: width / 50,
                thumbVisibility: true,
                child: ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (context, index) {
                    return RegularItem(item: items.elementAt(index));
                  },
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
                height: width / 7,
                width: width / 1.4,
                decoration: BoxDecoration(
                  color: const Color(0xFFDBFAFF),
                  borderRadius:
                      const BorderRadius.only(bottomLeft: Radius.circular(0.0)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 3,
                      offset: const Offset(0, 4),
                    )
                  ],
                ),
                child: Row(
                  children: [
                    Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                              width: width / 5.5,
                              child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "Order",
                                    style: TextStyle(fontSize: width / 35),
                                  ))),
                          SizedBox(
                            height: width / 60,
                          ),
                          SizedBox(
                              width: width / 5.5,
                              child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "Total",
                                    style: TextStyle(
                                        fontSize: width / 20,
                                        fontWeight: FontWeight.bold),
                                  ))),
                        ]),
                    SizedBox(
                      width: width / 10,
                    ),
                    Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                              width: width / 5.5,
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Takeaway",
                                    style: TextStyle(fontSize: width / 35),
                                  ))),
                          SizedBox(
                            height: width / 60,
                          ),
                          SizedBox(
                              width: width / 5.5,
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    total.toStringAsFixed(2),
                                    style: TextStyle(
                                        fontSize: width / 20,
                                        fontWeight: FontWeight.bold),
                                  ))),
                        ]),
                    SizedBox(
                      width: width / 5,
                      height: width / 10,
                      child: ElevatedButton(
                        onPressed: () {
                          if (name != '' &&
                              Regular.instance.getItems().length != 0) {
                            for (int ii = 0;
                                ii < Regular.instance.getItems().length;
                                ii++) {
                              KioskCart.instance.addToKioskCart(Regular.instance
                                  .getAtIndex(ii)
                                  .toKioskCartItem());
                            }
                          } else {
                            for (int ii = 0; ii < items.length; ii++) {
                              KioskCart.instance.addToKioskCart(
                                  items.elementAt(ii).toKioskCartItem());
                            }
                          }

                          if (KioskCart.instance.getLength() != 0) {
                            List<CartItem> cartList =
                                List.empty(growable: true);
                            for (int ii = 0;
                                ii < KioskCart.instance.getLength();
                                ii++) {
                              KioskCartItem kioskCartItem =
                                  KioskCart.instance.findItemAtIndex(ii);
                              cartList.add(kioskCartItem.toCartItem());
                            }
                            Mysql().addOrder(OrderModel(
                                orderType: KioskCart.instance.orderType,
                                orderDate: DateTime.now(),
                                status: "Pending",
                                items: cartList));
                            coffeeLoyalty();
                            KioskCart.instance..emptyKioskCart();
                            KioskCart.instance.coffeeCountKioskCart = 0;
                            getCoffeeCount();
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => new ConfirmKiosk()),
                            );
                          }
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => new ConfirmKiosk()),
                          );
                        },
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(7),
                        ))),
                        child: Center(
                          child: Text(
                            'Place Order',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'AdobeCleanUXBold',
                                fontSize: width / 40),
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          )
        ],
      ),
    );
  }
}
