//Author: Darren Ou Yang
//Last Updated" 23/09/2022
//Home Screen of the Kiosk
//Only the structure has been written
//todo: The entire home screen

import 'package:BeanBud/kiosk/cart_kiosk.dart';
import 'package:BeanBud/kiosk/home_kiosk.dart';
import 'package:BeanBud/kiosk/menu_kiosk.dart';
import 'package:BeanBud/page/main_screen.dart';
import 'package:flutter/material.dart';

import '../classes/global_variables.dart';

enum MenuItem { kioskMobileMode, logout, deleteAccount }

class MainKiosk extends StatefulWidget {
  const MainKiosk({
    Key? key,
  }) : super(key: key);

  @override
  State<MainKiosk> createState() => _MainKioskState();
}

int _selectedIndex = 0;

final kioskScreens = [
  HomeKiosk(),
  MenuKiosk(itemCategory: "Hot Drinks"),
  MenuKiosk(itemCategory: "Cold Drinks"),
  MenuKiosk(itemCategory: "Snacks"),
  MenuKiosk(itemCategory: "Desserts"),
];

class _MainKioskState extends State<MainKiosk> {
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    //Scaffold used to hold all child widget of the screen

    /* Future Work: Used for user inactivity
    Timer? sessionTimer;
    Duration myDuration = Duration(seconds: 20);
    void stopTimer() {
      setState(() => sessionTimer!.cancel());
      print("Stop");
    }

    void resetTimer() {
      stopTimer();
      setState(() => myDuration = Duration(seconds: 20));
    }

    void setCountDown() {
      final reduceSecondsBy = 1;
      setState(() {
        int seconds = myDuration.inSeconds - reduceSecondsBy;
        if (seconds < 0) {
          stopTimer();
          resetTimer();
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => StartKiosk()));
        } else {
          myDuration = Duration(seconds: seconds);
        }
      });
    }

    void startTimer() {
      sessionTimer =
          Timer.periodic(Duration(seconds: 1), (_) => setCountDown());
    }
    */

    return Scaffold(
      //Appbar implementation
      appBar: AppBar(
        toolbarHeight: width / 6.5,
        backgroundColor: Colors.white,
        //Bar
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40.0)),
        ),
        shadowColor: Colors.grey.withOpacity(0.5),
        title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          //Icon
          Image.asset(
            "assets/images/icon.png",
            fit: BoxFit.contain,
            height: width / 9,
            width: width / 9,
          ),
          //Sub-heading
          Padding(
            padding: EdgeInsets.only(left: width / 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Bean Bud",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: width / 15,
                  ),
                ),
                Text(
                  "Coffee For You",
                  style: TextStyle(
                    fontSize: width / 35,
                  ),
                )
              ],
            ),
          ),
        ]),
      ),

      body: Stack(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: EdgeInsets.only(left: width / 3),
              child: CustomPaint(
                painter: CirclePainter(width),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: CustomPaint(
              painter: CirclePainter(width / 1.4),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: width / 15, left: width / 12),
            child: (_greeting(width)),
          ),
          InkWell(
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => CartKiosk()),
              )
            },
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: width / 6.5, right: width / 6.5),
                    child: CustomPaint(
                      painter: CartPainter(width / 1.5),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(bottom: width / 15, right: width / 15),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: SizedBox(
                        width: width / 6,
                        height: width / 6,
                        child: const Image(
                            image: AssetImage('assets/images/cartIcon.png'))),
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              height: width,
              width: width / 7,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(50.0),
                    bottomRight: Radius.circular(50.0)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 10,
                    blurRadius: 7,
                    offset: const Offset(0, 3),
                  )
                ],
              ),
              child: NavigationRail(
                  onDestinationSelected: (int index) {
                    setState(() {
                      _selectedIndex = index;
                    });
                  },
                  labelType: NavigationRailLabelType.all,
                  selectedIndex: _selectedIndex,
                  destinations: [
                    NavigationRailDestination(
                      icon: Icon(
                        Icons.home,
                        size: width / 15,
                      ),
                      label: Text(
                        'Home',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: width / 30),
                      ),
                    ),
                    NavigationRailDestination(
                      icon: Icon(Icons.coffee, size: width / 15),
                      label: Text('Hot Drinks',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: width / 30)),
                    ),
                    NavigationRailDestination(
                      icon: Icon(Icons.snowing, size: width / 15),
                      label: Text('Cold Drinks',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: width / 30)),
                    ),
                    NavigationRailDestination(
                      icon: Icon(Icons.restaurant, size: width / 15),
                      label: Text('Snacks',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: width / 30)),
                    ),
                    NavigationRailDestination(
                      icon: Icon(Icons.cake, size: width / 15),
                      label: Text('Desserts',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: width / 30)),
                    )
                  ]),
            ),
          ),
          kioskScreens[_selectedIndex]
        ],
      ),
    );
  }
}

_greeting(double width) {
  if (_getHour() < 12) {
    if (name == '') {
      return Text('Good Morning!',
          textAlign: TextAlign.center,
          style:
              TextStyle(fontSize: width / 13.5, fontWeight: FontWeight.bold));
    } else {
      return Text('Good Morning ' + name + '!',
          textAlign: TextAlign.center,
          style:
              TextStyle(fontSize: width / 13.5, fontWeight: FontWeight.bold));
    }
  }
  if (_getHour() < 17) {
    if (name == '') {
      return Text('Good Afternoon!',
          textAlign: TextAlign.center,
          style:
              TextStyle(fontSize: width / 13.5, fontWeight: FontWeight.bold));
    } else {
      return Text('Good Afternoon ' + name + '!',
          textAlign: TextAlign.center,
          style:
              TextStyle(fontSize: width / 13.5, fontWeight: FontWeight.bold));
    }
  }
  if (name == '') {
    return Text('Good Evening!',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: width / 13.5, fontWeight: FontWeight.bold));
  } else {
    return Text('Good Evening ' + name + '!',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: width / 13.5, fontWeight: FontWeight.bold));
  }
}

_getHour() {
  return DateTime.now().hour;
}

_getColour() {
  if (_getHour() < 17) {
    return Color(0xFFFFD591);
  } else {
    return Colors.deepPurpleAccent;
  }
}

//Create a circle
class CirclePainter extends CustomPainter {
  double screenWidth = 0;
  CirclePainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 2 + 40;
    Offset center = const Offset(0, 0);

    // draw circle
    Paint thumbPaint = Paint()
      ..color = _getColour()
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}

class CartPainter extends CustomPainter {
  double screenWidth = 0;
  CartPainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 6;
    Offset center = const Offset(0, 0);

    // draw circle
    Paint thumbPaint = Paint()
      ..color = _getColour()
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}
