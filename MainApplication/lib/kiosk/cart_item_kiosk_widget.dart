import 'package:BeanBud/classes/kiosk_cart.dart';
import 'package:flutter/material.dart';
import 'package:BeanBud/classes/cart_item.dart';

import '../classes/kiosk_cart_item.dart';

//Author: Darren Ou Yang
//Last Updated" 23/09/2022
//Widget Structure of a Item that is in a cart

class CartItemKioskWidget extends StatelessWidget {
  const CartItemKioskWidget({super.key, required this.cartItem});

  final KioskCartItem cartItem;
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    List<Widget> attributeWidgets = List.empty(growable: true);
    attributeWidgets.add(
      Text(
        cartItem.itemName,
        style: TextStyle(fontSize: width / 20, fontWeight: FontWeight.bold),
      ),
    );
    attributeWidgets.add(Text(
      "- Quantity: ${cartItem.itemQuantity}",
      style: TextStyle(fontSize: width / 45),
    ));
    for (int ii = 0; ii < cartItem.attributes.length; ii++) {
      attributeWidgets.add(
        Text(
          "- " +
              cartItem.attributes.elementAt(ii).expandableOption +
              ": " +
              cartItem.attributes.elementAt(ii).option,
          style: TextStyle(fontSize: width / 45),
        ),
      );
    }
    return InkWell(
      onTap: () {},
      child: Container(
          height: width / 3,
          child: Row(
            children: [
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      top: width / 35,
                      left: width / 35,
                      right: width / 35,
                      bottom: width / 60,
                    ),
                    child: SizedBox(
                      height: width / 5,
                      width: width / 5,
                      child: Card(
                        child: Center(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Image.memory(cartItem.itemImage,
                                fit: BoxFit.fill,
                                height: width / 4,
                                width: width / 4,
                                alignment: Alignment.center),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: width / 6,
                    height: width / 20,
                    child: ElevatedButton(
                      onPressed: () =>
                          {KioskCart.instance.removeFromKioskCart(cartItem)},
                      style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ))),
                      child: Center(
                        child: Text(
                          'Remove',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'AdobeCleanUXBold',
                              fontSize: width / 40),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: width / 40,
              ),
              SizedBox(
                width: width / 2.5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: attributeWidgets,
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: width / 30),
                    child: Text(
                      cartItem.itemPrice.toStringAsFixed(2),
                      style: TextStyle(fontSize: width / 25),
                    ),
                  )
                ],
              )
            ],
          )),
    );
  }
}
