import 'package:BeanBud/models/item_model.dart';
import 'package:BeanBud/page/menu_page.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'dart:typed_data';

import '../classes/cart.dart';
import '../classes/cart_item.dart';
import '../classes/itemOption.dart';
import '../classes/kiosk_cart.dart';
import '../classes/kiosk_cart_item.dart';
import '../models/item_model.dart';

int itemQuantity = 1;
int numExapndedableOptions = 0;
double priceChanges = 0.0;
List<ItemOption> itemAttributes = List.empty(growable: true);

class ProductKiosk extends StatelessWidget {
  ItemModel item;

  ProductKiosk({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return Scaffold(
      //Appbar implementation
      appBar: AppBar(
        toolbarHeight: width / 6.5,
        backgroundColor: Colors.white,
        //Bar
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40.0)),
        ),
        shadowColor: Colors.grey.withOpacity(0.5),
        title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          //Icon
          Image.asset(
            "assets/images/icon.png",
            fit: BoxFit.contain,
            height: width / 9,
            width: width / 9,
          ),
          //Sub-heading
          Padding(
            padding: EdgeInsets.only(left: width / 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Bean Bud",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: width / 15,
                  ),
                ),
                Text(
                  "Coffee For You",
                  style: TextStyle(
                    fontSize: width / 35,
                  ),
                )
              ],
            ),
          ),
        ]),
      ),

      body: Stack(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: EdgeInsets.only(left: width / 3, top: width / 10),
              child: CustomPaint(
                painter: CirclePainter(width),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: width / 15),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: width / 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: width / 15),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: Image.memory(
                              Uint8List.fromList(item.itemImage.toBytes()),
                              fit: BoxFit.fill,
                              height: width / 3.5,
                              width: width / 3.5,
                              alignment: Alignment.center),
                        ),
                      ),
                      //Item Name
                      Padding(
                        padding: EdgeInsets.only(left: width / 30),
                        child: SizedBox(
                          width: width / 2,
                          child: Text(
                            item.itemName,
                            style: TextStyle(
                                fontSize: width / 15,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Align(
                    alignment: Alignment.bottomRight,
                    child: Product(itemModel: item)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Product extends StatefulWidget {
  final ItemModel itemModel;
  const Product({super.key, required this.itemModel});
  @override
  State<Product> createState() => _ProductState(itemModel);
}

class _ProductState extends State<Product> {
  var itemModel;
  _ProductState(ItemModel itemModel) {
    this.itemModel = itemModel;
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    List<Widget> itemWidgetOptions = List.empty(growable: true);
    switch (itemModel.itemCategory) {
      case "Hot Drink":
        itemWidgetOptions.add(Counter());
        itemWidgetOptions.add(
          _expandable_Option(
              "Size",
              [
                _option("Size", width, "Small (220ml)", 0.0),
                _option("Size", width, "Medium (340ml)", 0.6),
                _option("Size", width, "Large (450ml)", 1.1)
              ],
              width),
        );
        itemWidgetOptions.add(_expandable_Option(
            "Milk",
            [
              _option("Milk", width, "Whole Milk", 0.0),
              _option("Milk", width, "Almond Milk", 0.6),
              _option("Milk", width, "Soy Milk", 0.6),
              _option("Milk", width, "Oat Milk", 0.6),
            ],
            width));
        numExapndedableOptions = 2;

        break;
      case "Cold Drink":
        itemWidgetOptions.add(Counter());
        itemWidgetOptions.add(
          _expandable_Option(
              "Size",
              [
                _option("Size", width, "Small (220ml)", 0.0),
                _option("Size", width, "Medium (340ml)", 0.6),
                _option("Size", width, "Large (450ml)", 1.1)
              ],
              width),
        );
        itemWidgetOptions.add(_expandable_Option(
            "Milk",
            [
              _option("Milk", width, "Whole Milk", 0.0),
              _option("Milk", width, "Almond Milk", 0.6),
              _option("Milk", width, "Soy Milk", 0.6),
              _option("Milk", width, "Oat Milk", 0.6),
            ],
            width));
        numExapndedableOptions = 2;
        break;
      case "Snack":
        itemWidgetOptions.add(Counter());
        numExapndedableOptions = 0;
        break;
      case "Dessert":
        itemWidgetOptions.add(Counter());
        numExapndedableOptions = 0;
        break;

      default:
    }

    return Column(children: [
      //Item Options
      Container(
        height: width / 1.1,
        width: width / 1.2,
        decoration: const BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              spreadRadius: 5.0,
              blurRadius: 7,
              offset: Offset(0, 0), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          children: [
            Expanded(
                child: ListView(
              children: itemWidgetOptions,
            )),
            Padding(
              padding: EdgeInsets.all(width / 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                    width: width / 4,
                    height: width / 10,
                    child: ElevatedButton(
                      onPressed: () {
                        itemQuantity = 0;
                        Navigator.pop(context);
                      },
                      style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ))),
                      child: Center(
                        child: Text(
                          'Cancel',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'AdobeCleanUXBold',
                              fontSize: width / 30),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: width / 2.5,
                    height: width / 10,
                    child: ElevatedButton(
                      onPressed: () {
                        if (itemQuantity != 0) {
                          if (itemAttributes.length == numExapndedableOptions) {
                            List<ItemOption> tempItemAttributes =
                                List.empty(growable: true);
                            for (int ii = 0; ii < itemAttributes.length; ii++) {
                              tempItemAttributes
                                  .add(itemAttributes.elementAt(ii));
                            }

                            double totalPrice =
                                (itemModel.itemPrice + priceChanges) *
                                    itemQuantity;

                            KioskCartItem item = KioskCartItem(
                              itemName: itemModel.itemName,
                              itemDescription: itemModel.itemDescription,
                              itemImage: Uint8List.fromList(
                                  itemModel.itemImage.toBytes()),
                              itemPrice: totalPrice,
                              itemQuantity: itemQuantity,
                              attributes: tempItemAttributes,
                              itemId: itemModel.id,
                              category: itemModel.itemCategory,
                            );

                            KioskCart.instance.addToKioskCart(item);
                            itemQuantity = 1;
                            numExapndedableOptions = 0;
                            priceChanges = 0.0;
                            itemAttributes.clear();
                            //CHANGE TO IF ALL SELECTIONS SELECTED
                            Navigator.pop(context);
                          } else {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  _buildPopupDialog(context,
                                      "Please select 1 of each option"),
                            );
                          }
                        } else {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) =>
                                _buildPopupDialog(context,
                                    "Please have quantity of 1 or more"),
                          );
                        }
                      },
                      style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ))),
                      child: Center(
                        child: Text(
                          'Add to Cart: ' +
                              ((itemModel.itemPrice + priceChanges) *
                                      itemQuantity)
                                  .toString(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'AdobeCleanUXBold',
                              fontSize: width / 30),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ]);
  }
}

class Counter extends StatefulWidget {
  const Counter({super.key});
  @override
  State<Counter> createState() => _CounterState();
}

//Quantity Counter
class _CounterState extends State<Counter> {
  @override
  Widget build(BuildContext context) {
    void remove() {
      setState(() {
        if (itemQuantity != 0) {
          itemQuantity--;
        } else {
          itemQuantity = 0;
        }
      });
    }

    void add() {
      setState(() {
        itemQuantity += 1;
      });
    }

    final double width = MediaQuery.of(context).size.width;
    return SizedBox(
      height: width / 5,
      child: Row(
        children: [
          SizedBox(
              width: width / 2.7,
              child: Padding(
                padding: EdgeInsets.only(
                    left: width / 15, top: width / 25, bottom: width / 25),
                child: Text(
                  "Quantity",
                  style: TextStyle(
                      fontSize: width / 15, fontWeight: FontWeight.bold),
                ),
              )),
          SizedBox(width: width / 6),
          InkWell(
            onTap: () => remove(),
            child: Container(
                width: width / 10,
                height: width / 10,
                decoration: const BoxDecoration(
                    color: Color(0xFFFFD591),
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Align(
                    alignment: Alignment.center,
                    child: Icon(
                      Icons.remove,
                      size: width / 15,
                    ))),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(itemQuantity.toString(),
                style: TextStyle(fontSize: width / 15)),
          ),
          InkWell(
            onTap: () => add(),
            child: Container(
                width: width / 10,
                height: width / 10,
                decoration: const BoxDecoration(
                    color: Color(0xFFFFD591),
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Align(
                    alignment: Alignment.center,
                    child: Icon(
                      Icons.add,
                      size: width / 15,
                    ))),
          )
        ],
      ),
    );
  }
}

_option(String expandableOption, double width, String option, double price) {
  Color optionColor = Colors.transparent;
  return InkWell(
    onTap: () {
      ItemOption newOption = ItemOption(expandableOption, option);
      if (itemAttributes.length != 0) {
        bool foundAttribute = false;
        for (int ii = 0; ii < itemAttributes.length; ii++) {
          if (itemAttributes.elementAt(ii).expandableOption ==
              expandableOption) {
            itemAttributes[ii] = newOption;
            foundAttribute = true;
            priceChanges = priceChanges + price;
          }
        }
        if (!foundAttribute) {
          itemAttributes.add(newOption);
          priceChanges = priceChanges + price;
        }
      } else {
        itemAttributes.add(newOption);
        priceChanges = priceChanges + price;
      }
    },
    child: Container(
      width: width,
      height: width / 10,
      decoration: BoxDecoration(color: optionColor),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: EdgeInsets.only(left: width / 15),
          child: Row(
            children: [
              SizedBox(
                width: width / 2.5,
                child: Text(
                  option,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: 'AdobeCleanUXBold',
                      fontSize: width / 30),
                ),
              ),
              SizedBox(
                width: width / 6,
              ),
              Text(
                price.toStringAsPrecision(3),
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'AdobeCleanUXBold',
                    fontSize: width / 30),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

//Expands to list avaliable options to the use
_expandable_Option(String title, List<InkWell> options, double width) {
  return ExpandablePanel(
    header: Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.only(
            left: width / 15, top: width / 25, bottom: width / 25),
        child: Text(
          title,
          style: TextStyle(fontSize: width / 20, fontWeight: FontWeight.bold),
        ),
      ),
    ),
    collapsed: const Text(""),
    expanded: Column(
      children: options,
    ),
    theme: ExpandableThemeData(
      iconSize: width / 10,
      iconColor: Colors.black,
    ),
  );
}

class CirclePainter extends CustomPainter {
  double screenWidth = 0;
  CirclePainter(double width) {
    screenWidth = width;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // draw shadow
    double radius = screenWidth / 2 + 40;
    Offset center = const Offset(0, 0);

    // draw circle
    Paint thumbPaint = Paint()
      ..color = const Color(0xFFFFD591)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, thumbPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}

Widget _buildPopupDialog(BuildContext context, String text) {
  return new AlertDialog(
    title: const Text('Error'),
    content: new Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(text),
      ],
    ),
    actions: <Widget>[
      new ElevatedButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: const Text('Close'),
      ),
    ],
  );
}
