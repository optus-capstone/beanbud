import 'dart:typed_data';

import 'package:BeanBud/classes/menu_desserts.dart';
import 'package:BeanBud/classes/menu_hot_drinks.dart';
import 'package:BeanBud/classes/menu_snacks.dart';
import 'package:BeanBud/kiosk/cart_kiosk.dart';
import 'package:BeanBud/kiosk/product_kiosk.dart';
import 'package:BeanBud/models/item_model.dart';
import 'package:flutter/material.dart';

import '../classes/menu_cold_drinks.dart';

class MenuKiosk extends StatelessWidget {
  String itemCategory;

  MenuKiosk({
    Key? key,
    required this.itemCategory,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    List<Widget> kioskMenuList = List.empty(growable: true);

    switch (itemCategory) {
      case "Popular":
        {
          for (int ii = 0; ii < MenuHotDrinks.instance.getLength(); ii++) {
            ProductWidget productWidget = ProductWidget(
                thisItem: MenuHotDrinks.instance.findItemAtIndex(ii));
            kioskMenuList.add(productWidget);
          }
        }
        break;
      case "Hot Drinks":
        {
          for (int ii = 0; ii < MenuHotDrinks.instance.getLength(); ii++) {
            ProductWidget productWidget = ProductWidget(
                thisItem: MenuHotDrinks.instance.findItemAtIndex(ii));
            kioskMenuList.add(productWidget);
          }
        }
        break;
      case "Cold Drinks":
        {
          for (int ii = 0; ii < MenuColdDrinks.instance.getLength(); ii++) {
            ProductWidget productWidget = ProductWidget(
                thisItem: MenuColdDrinks.instance.findItemAtIndex(ii));
            kioskMenuList.add(productWidget);
          }
        }
        break;
      case "Snacks":
        {
          for (int ii = 0; ii < MenuSnacks.instance.getLength(); ii++) {
            ProductWidget productWidget = ProductWidget(
                thisItem: MenuSnacks.instance.findItemAtIndex(ii));
            kioskMenuList.add(productWidget);
          }
        }
        break;
      case "Desserts":
        {
          for (int ii = 0; ii < MenuDesserts.instance.getLength(); ii++) {
            ProductWidget productWidget = ProductWidget(
                thisItem: MenuDesserts.instance.findItemAtIndex(ii));
            kioskMenuList.add(productWidget);
          }
        }
        break;
      default:
        {}
        break;
    }
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        height: width / 1.25,
        width: width / 1.4,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
              bottom:
                  BorderSide(color: Colors.grey.withOpacity(0.5), width: 3)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 3,
              blurRadius: 3,
            )
          ],
        ),
        child: Padding(
          padding: EdgeInsets.only(top: width / 35),
          child: GridView(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisSpacing: 20,
                crossAxisSpacing: 20,
                childAspectRatio: 0.8),
            children: kioskMenuList,
          ),
        ),
      ),
    );
  }
}

class ProductWidget extends StatelessWidget {
  ProductWidget({super.key, required this.thisItem});

  ItemModel thisItem;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductKiosk(
                  item: thisItem,
                )),
      ),
      child: SizedBox(
        height: width / 3,
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.memory(
                  Uint8List.fromList(thisItem.itemImage.toBytes()),
                  fit: BoxFit.fill,
                  height: width / 6,
                  width: width / 6,
                  alignment: Alignment.center)),
          Padding(
            padding: EdgeInsets.only(top: 15),
            child: Text(
              thisItem.itemName,
              style: TextStyle(fontSize: width / 35),
              textAlign: TextAlign.center,
            ),
          )
        ]),
      ),
    );
  }
}
