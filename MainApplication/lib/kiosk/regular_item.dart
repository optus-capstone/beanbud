import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:BeanBud/classes/cart_item.dart';

//Author: Darren Ou Yang
//Last Updated" 23/09/2022
//Widget Structure of an Account setting
//Need to be changed to import a onclick to switch to a new page

class RegularItem extends StatelessWidget {
  const RegularItem({
    super.key,
    required this.item,
  });

  final CartItem item;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    List<Widget> attributesWidgets = List.empty(growable: true);
    if (item.category == "Hot Drink" || item.category == "Cold Drink") {
      attributesWidgets.add(
        SizedBox(
            width: width / 5,
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "+ Medium",
                  style: TextStyle(fontSize: width / 35),
                ))),
      );
      attributesWidgets.add(
        SizedBox(
            width: width / 5,
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "+ Whole Milk",
                  style: TextStyle(fontSize: width / 35),
                ))),
      );
    }
    attributesWidgets.add(
      SizedBox(
          width: width / 5,
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                item.itemPrice.toStringAsFixed(2),
                style: TextStyle(fontSize: width / 35),
              ))),
    );

    return Padding(
      padding: EdgeInsets.all(width / 40),
      child: Row(children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: SizedBox(
            width: width / 9,
            height: width / 9,
            child: Image.memory(
              Uint8List.fromList(item.itemImage.toBytes()),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: width / 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text(
                  item.itemName,
                  style: TextStyle(
                      fontSize: width / 25, fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                "+ Large",
                style: TextStyle(fontSize: width / 35),
              ),
              Row(
                children: [
                  SizedBox(
                      width: width / 5,
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "+ Whole Milk",
                            style: TextStyle(fontSize: width / 35),
                          ))),
                  SizedBox(
                      width: width / 5,
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            item.itemPrice.toStringAsFixed(2),
                            style: TextStyle(
                                fontSize: width / 25,
                                fontWeight: FontWeight.bold),
                          ))),
                ],
              )
            ],
          ),
        )
      ]),
    );
  }
}
