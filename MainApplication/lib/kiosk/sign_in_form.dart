import 'package:BeanBud/classes/global_variables.dart';
import 'package:flutter/material.dart';
import 'package:BeanBud/customWidgets/app_button.dart';
import 'package:BeanBud/facialRecognition/user_model.dart';

import '../page/main_screen.dart';

class SignInSheet extends StatelessWidget {
  const SignInSheet({Key? key, required this.user}) : super(key: key);
  final UserModel user;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Text(
              'Welcome Back!',
              style: const TextStyle(fontSize: 20),
            ),
          ),
          Column(
            children: [
              const SizedBox(height: 10),
              const SizedBox(height: 10),
              const Divider(),
              const SizedBox(height: 10),
              AppButton(
                text: 'LOGIN',
                onPressed: () async {
                  name = user.firstName;
                  id = user.id;
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => const MainScreen()));
                },
                icon: const Icon(
                  Icons.login,
                  color: Colors.white,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}